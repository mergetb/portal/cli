package main

import (
	"bufio"
	"context"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/cli/clients"
	pviews "gitlab.com/mergetb/tech/shared/cli/portal"
)

func communications() {

	var withStatus bool

	email := &cobra.Command{
		Use:   "email",
		Short: "Email users, projects, organizations",
	}

	var contentType = "text/plain"
	email.Flags().StringVarP(&contentType, "contentType", "", "text/plain", "Repository branch to push the model to.")

	users := &cobra.Command{
		Use:   "users <username 1> <username 2> ... <username N> <subject> <content>",
		Short: "Email a list of users. Content is path to file or \"-\" for stdin.",
		Args:  cobra.MinimumNArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			l := len(args)
			emailUsers(args[:l-2], args[l-2], args[l-1], contentType, withStatus)
		},
	}
	users.Flags().BoolVarP(&withStatus, "with-status", "S", false, "Show task status of sending email")
	email.AddCommand(users)

	project := &cobra.Command{
		Use:   "project <project name> <subject> <content>",
		Short: "Email all users in a project. Content is path to file or \"-\" for stdin.",
		Args:  cobra.ExactArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			emailProj(args[0], args[1], args[2], contentType, withStatus)
		},
	}
	project.Flags().BoolVarP(&withStatus, "with-status", "S", false, "Show task status of sending email")
	email.AddCommand(project)

	org := &cobra.Command{
		Use:   "organization <organization name> <subject> <content>",
		Short: "Email all users in an organization. Content is path to file or \"-\" for stdin.",
		Args:  cobra.ExactArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			emailOrg(args[0], args[1], args[2], contentType, withStatus)
		},
	}
	org.Flags().BoolVarP(&withStatus, "with-status", "S", false, "Show task status of sending email")
	email.AddCommand(org)

	del := &cobra.Command{
		Use:   "delete <id>",
		Short: "Delete an existing email request. The ID is found in the request status. Rerun the send command with -S to see it",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			deleteEmail(args[0])
		},
	}
	email.AddCommand(del)

	root.AddCommand(email) // top-level
}

func emailUsers(users []string, subject, content, contentType string, withStatus bool) {
	log.Debugf("Emailing users %+v - \"%s\"", users, subject)

	// Store as we can read from stdin and checkEMailUsers needs the content.
	contentbuf := readContent(content)

	clients.Communications(
		endpoint(),
		token(),
		func(cli portal.CommunicationsClient) error {
			_, err := cli.EMailUsers(
				context.TODO(),
				&portal.EMailUsersRequest{
					Usernames:   users,
					Subject:     subject,
					Contents:    contentbuf,
					ContentType: contentType,
					StatusMS:    timeoutAsBool(withStatus),
				},
			)
			if err != nil {
				handleError(err)
			}
			return nil
		},
	)

	checkEMailUsers(users, subject, contentbuf, contentType)
}

func emailProj(project, subject, content, contentType string, withStatus bool) {
	log.Debugf("Emailing project  %s - \"%s\"", project, subject)

	contentbuf := readContent(content)

	clients.Communications(
		endpoint(),
		token(),
		func(cli portal.CommunicationsClient) error {
			resp, err := cli.EMailProjectMembers(
				context.TODO(),
				&portal.EMailProjectMembersRequest{
					Project:     project,
					Subject:     subject,
					Contents:    contentbuf,
					ContentType: contentType,
					StatusMS:    timeoutAsBool(withStatus),
				},
			)
			if err != nil {
				handleError(err)
			}
			if withStatus {
				pviews.WriteTaskForest(resp.Status, getDepth(), true, tw)
				osExit1IfUnsuccessful(resp.Status)

				return nil
			}
			return nil
		},
	)

	checkEMailProject(project, subject, contentbuf, contentType)
}

func emailOrg(org, subject, content, contentType string, withStatus bool) {
	log.Debugf("Emailing org  %s - \"%s\"", org, subject)

	contentbuf := readContent(content)

	clients.Communications(
		endpoint(),
		token(),
		func(cli portal.CommunicationsClient) error {
			resp, err := cli.EMailOrganizationMembers(
				context.TODO(),
				&portal.EMailOrganizationMembersRequest{
					Organization: org,
					Subject:      subject,
					Contents:     contentbuf,
					ContentType:  contentType,
					StatusMS:     timeoutAsBool(withStatus),
				},
			)
			if err != nil {
				handleError(err)
			}
			if withStatus {
				pviews.WriteTaskForest(resp.Status, getDepth(), true, tw)
				osExit1IfUnsuccessful(resp.Status)

				return nil
			}
			return nil
		},
	)

	checkEMailOrganization(org, subject, contentbuf, contentType)
}

func deleteEmail(id string) {
	clients.Communications(
		endpoint(),
		token(),
		func(cli portal.CommunicationsClient) error {
			_, err := cli.DeleteEMail(
				context.TODO(),
				&portal.DeleteEMailRequest{
					Id: id,
				},
			)

			if err != nil {
				handleError(err)
			}

			return nil
		},
	)
}

func readContent(c string) string {

	if c != "-" {
		m, err := os.ReadFile(c)
		if err != nil {
			handleError(err)
		}
		return string(m)
	}

	ret := ""
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		ret += scanner.Text()
	}
	if err := scanner.Err(); err != nil {
		handleError(err)
	}

	return ret + "\n"
}
