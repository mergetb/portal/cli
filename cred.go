package main

import (
	"context"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/cli/clients"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func cred() {

	var private bool
	showKey := &cobra.Command{
		Use:   "key",
		Short: "Show the authenticated user's key",
		Args:  cobra.ExactArgs(0),
		Run: func(cmd *cobra.Command, args []string) {
			showKey(private)
		},
	}
	showKey.Flags().BoolVarP(&private, "private", "", false,
		"If given, show the user's private key else the public key",
	)
	show.AddCommand(showKey)

	showCert := &cobra.Command{
		Use:   "sshcert",
		Short: "Show the user's SSH certificate",
		Args:  cobra.ExactArgs(0),
		Run: func(cmd *cobra.Command, args []string) {
			showCert()
		},
	}
	show.AddCommand(showCert)
}

func showKey(private bool) {

	username := getLoggedInUsername()

	keys, err := getKeys(username)
	if err != nil {
		log.Fatal(err)
	}

	if private {
		fmt.Println(keys.Private)
	} else {
		fmt.Println(keys.Public)
	}

}

func showCert() {

	username := getLoggedInUsername()

	c, err := getCert(username)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(c.Cert)

}

func getCert(username string) (*portal.SSHCert, error) {

	var cert = new(portal.SSHCert)
	err := clients.Cred(endpoint(), token(), func(cli portal.CredClient) error {

		// We are going read the cert a few times until we get it as the cert
		// is created on login and may take a few seconds to get generated.

		for i := 5; i != 0; i-- {
			resp, err := cli.GetUserSSHCert(
				context.TODO(),
				&portal.GetUserSSHCertRequest{
					Username: username,
				},
			)
			if err != nil {
				if status.Code(err) == codes.NotFound {
					time.Sleep(1 * time.Second)
					continue
				}
				return err
			}

			cert = resp.Cert
			return nil
		}

		// no cert read at this point. warn the user and continue.
		fmt.Println("Warning: unable to retrive SSH cert.")
		return fmt.Errorf("no cert available")
	})

	if err != nil {
		return nil, err
	}

	return cert, nil
}

func getKeys(username string) (*portal.SSHKeyPair, error) {

	var keys *portal.SSHKeyPair
	err := clients.Cred(endpoint(), token(), func(cli portal.CredClient) error {

		resp, err := cli.GetUserSSHKeys(
			context.TODO(),
			&portal.GetUserSSHKeysRequest{
				Username: username,
			},
		)
		if err != nil {
			return err
		}

		keys = resp.Keys

		return nil
	})

	if err != nil {
		return nil, err
	}

	return keys, nil
}
