package main

import (
	"context"
	"fmt"
	"os"
	"sync"
	"time"

	"github.com/schollz/progressbar/v3"
	log "github.com/sirupsen/logrus"
	"google.golang.org/protobuf/encoding/protojson"

	"gitlab.com/mergetb/portal/cli/clients"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	pviews "gitlab.com/mergetb/tech/shared/cli/portal"
)

const MAX_TIMEOUT = time.Duration(time.Second)
const TIMEOUT_SYNCED_TIMES = 3
const TIMEOUT_BACKOFF_FACTOR = 2
const MAX_BACKOFF_DIVISOR = 4
const MAX_BACKOFF_PERIOD = 30 * time.Second
const DEFAULT_TIMEOUT = 10 * time.Second

type statusChecker func(context.Context, int32) (*portal.TaskTree, error)

func checkWrapper(f statusChecker) {
	total_duration := getTimeout()
	if total_duration <= 0 {
		return
	}

	// the progress bar get its own goroutine,
	// so that updating the elasped time happens
	// independently of whenever we receive a status update

	st_ch := make(chan *portal.TaskTree, 1)
	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		ok := true
		var st *portal.TaskTree
		var pb *progressbar.ProgressBar

		for ok {
			select {
			case st, ok = <-st_ch:
				if !ok {
					break
				}

				if !progressless && pb == nil {
					var err error
					pb, err = pviews.TaskTreeToProgressBar(st)

					if err != nil {
						pb = nil
					}
				}

				pviews.TaskTreeUpdateProgressBar(st, pb)

			case <-time.After(time.Second):
				if pb != nil {
					pb.ChangeMax((pb.GetMax()))
				}
			}
		}

		if pb != nil {
			pb.Clear()
			pb.Close()
		}

		wg.Done()
	}()

	// the idea is that instead of doing a single synced status watch,
	// we do several smaller synced status watches
	// however, if we do too many synced status watches,
	// then we just revert to checking it periodically, with increasing scaling

	var status *portal.TaskTree
	var err error

	unsynced_factor := TIMEOUT_BACKOFF_FACTOR

	start := time.Now()
	end := start.Add(total_duration)

	for count := 0; time.Now().Before(end); count++ {
		remaining_time := end.Sub(time.Now())

		// cap out each synced watch to MAX_TIMEOUT
		timeout := MAX_TIMEOUT
		if remaining_time < timeout {
			timeout = remaining_time
		}

		if count < TIMEOUT_SYNCED_TIMES {
			// we're in synced watch mode: just leave timeout as is and pass it to f

			ctx, cancel := context.WithTimeout(context.Background(), DEFAULT_TIMEOUT+timeout)

			status, err = f(ctx, int32(timeout.Milliseconds()))
			cancel()
		} else {
			// we're in unsynced watch mode: wait the period, then get the status

			// cap the timeout
			if timeout > total_duration/MAX_BACKOFF_DIVISOR {
				timeout = total_duration / MAX_BACKOFF_DIVISOR
			}

			if timeout > MAX_BACKOFF_PERIOD {
				timeout = MAX_BACKOFF_PERIOD
			}

			unsynced_factor *= TIMEOUT_BACKOFF_FACTOR

			time.Sleep(timeout)
			ctx, cancel := context.WithTimeout(context.Background(), DEFAULT_TIMEOUT)

			status, err = f(ctx, -1)
			cancel()
		}

		if err != nil ||
			status.HighestStatus == portal.TaskStatus_Success ||
			status.HighestStatus == portal.TaskStatus_Error {

			break
		}

		st_ch <- status
	}

	close(st_ch)

	wg.Wait()

	if err != nil {
		handleError(err)
	}

	if jsonOut {
		fmt.Printf("%s\n", protojson.Format(status))
		return
	}

	pviews.WriteTaskForest(status, getDepth(), true, tw)

	if status.HighestStatus != portal.TaskStatus_Success {
		if total_duration >= time.Hour {
			log.Errorf("If you waited an hour or more for something to come up, something went wrong, please contact support.")
		}

		os.Exit(1)
	}
}

func checkUser(username string) {
	f := func(ctx context.Context, timeout int32) (ret_status *portal.TaskTree, ret_err error) {

		clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {
			resp, err := cli.GetUser(
				ctx,
				&portal.GetUserRequest{
					Username: username,
					StatusMS: timeout,
				},
			)

			ret_err = err

			if err != nil {
				return err
			}

			ret_status = resp.GetStatus()

			return nil
		})

		return
	}

	checkWrapper(f)
}

func checkOrganization(name string) {
	f := func(ctx context.Context, timeout int32) (ret_status *portal.TaskTree, ret_err error) {

		clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {
			resp, err := cli.GetOrganization(
				ctx,
				&portal.GetOrganizationRequest{
					Name:     name,
					StatusMS: timeout,
				},
			)

			ret_err = err

			if err != nil {
				return err
			}

			ret_status = resp.GetStatus()

			return nil
		})

		return
	}

	checkWrapper(f)
}

func checkProject(project string) {
	f := func(ctx context.Context, timeout int32) (ret_status *portal.TaskTree, ret_err error) {

		clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {
			resp, err := cli.GetProject(
				ctx,
				&portal.GetProjectRequest{
					Name:     project,
					StatusMS: timeout,
				},
			)

			ret_err = err

			if err != nil {
				return err
			}

			ret_status = resp.GetStatus()

			return nil
		})

		return
	}

	checkWrapper(f)

}

func checkExperiment(pid, xid string) {
	f := func(ctx context.Context, timeout int32) (ret_status *portal.TaskTree, ret_err error) {

		clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {
			resp, err := cli.GetExperiment(
				ctx,
				&portal.GetExperimentRequest{
					Project:    pid,
					Experiment: xid,
					StatusMS:   timeout,
				},
			)

			ret_err = err

			if err != nil {
				return err
			}

			ret_status = resp.GetStatus()

			return nil
		})

		return
	}

	checkWrapper(f)

}

func checkRealization(pid, xid, rid string) {
	f := func(ctx context.Context, timeout int32) (ret_status *portal.TaskTree, ret_err error) {

		clients.Realize(endpoint(), token(), func(cli portal.RealizeClient) error {
			resp, err := cli.GetRealization(
				ctx,
				&portal.GetRealizationRequest{
					Project:     pid,
					Experiment:  xid,
					Realization: rid,
					StatusMS:    timeout,
				},
			)

			ret_err = err

			if err != nil {
				return err
			}

			ret_status = resp.GetStatus()

			return nil
		})

		return
	}

	checkWrapper(f)

}

func checkMaterialization(pid, xid, rid string) {
	f := func(ctx context.Context, timeout int32) (ret_status *portal.TaskTree, ret_err error) {

		clients.Materialize(endpoint(), token(), func(cli portal.MaterializeClient) error {
			resp, err := cli.GetMaterializationV2(
				ctx,
				&portal.GetMaterializationRequestV2{
					Project:     pid,
					Experiment:  xid,
					Realization: rid,
					StatusMS:    timeout,
				},
			)

			ret_err = err

			if err != nil {
				return err
			}

			ret_status = resp.GetStatus()

			return nil
		})

		return
	}

	checkWrapper(f)

}

func checkXDC(name, project string) {
	f := func(ctx context.Context, timeout int32) (ret_status *portal.TaskTree, ret_err error) {

		clients.XDC(endpoint(), token(), func(cli portal.XDCClient) error {
			resp, err := cli.GetXDC(
				ctx,
				&portal.GetXDCRequest{
					Xdc:      name,
					Project:  project,
					StatusMS: timeout,
				},
			)

			ret_err = err

			if err != nil {
				return err
			}

			ret_status = resp.GetStatus()

			return nil
		})

		return
	}

	checkWrapper(f)
}

func checkEMailUsers(users []string, subject, content, contentType string) {

	f := func(ctx context.Context, timeout int32) (ret_status *portal.TaskTree, ret_err error) {
		clients.Communications(
			endpoint(),
			token(),
			func(cli portal.CommunicationsClient) error {
				resp, err := cli.EMailUsers(
					context.TODO(),
					&portal.EMailUsersRequest{
						Usernames:   users,
						Subject:     subject,
						Contents:    content,
						ContentType: contentType,
						StatusMS:    timeoutAsBool(true),
					},
				)

				ret_err = err
				if err != nil {
					handleError(err)
				}

				ret_status = resp.GetStatus()

				return nil
			},
		)
		return
	}

	checkWrapper(f)
}

func checkEMailProject(project string, subject, content, contentType string) {

	f := func(ctx context.Context, timeout int32) (ret_status *portal.TaskTree, ret_err error) {
		clients.Communications(
			endpoint(),
			token(),
			func(cli portal.CommunicationsClient) error {
				resp, err := cli.EMailProjectMembers(
					context.TODO(),
					&portal.EMailProjectMembersRequest{
						Project:     project,
						Subject:     subject,
						Contents:    content,
						ContentType: contentType,
						StatusMS:    timeoutAsBool(true),
					},
				)

				ret_err = err
				if err != nil {
					handleError(err)
				}

				ret_status = resp.GetStatus()

				return nil
			},
		)
		return
	}

	checkWrapper(f)
}

func checkEMailOrganization(org string, subject, content, contentType string) {

	f := func(ctx context.Context, timeout int32) (ret_status *portal.TaskTree, ret_err error) {
		clients.Communications(
			endpoint(),
			token(),
			func(cli portal.CommunicationsClient) error {
				resp, err := cli.EMailOrganizationMembers(
					context.TODO(),
					&portal.EMailOrganizationMembersRequest{
						Organization: org,
						Subject:      subject,
						Contents:     content,
						ContentType:  contentType,
						StatusMS:     timeoutAsBool(true),
					},
				)

				ret_err = err
				if err != nil {
					handleError(err)
				}

				ret_status = resp.GetStatus()

				return nil
			},
		)
		return
	}

	checkWrapper(f)
}
func timeoutAsBool(status bool) int32 {
	if !status {
		return 0
	}

	return -1
}
