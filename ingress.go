package main

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/cli/clients"
	shcli "gitlab.com/mergetb/tech/shared/cli"
	xir "gitlab.com/mergetb/xir/v0.3/go"
	"google.golang.org/protobuf/encoding/protojson"
)

func ingress() {

	listIng := &cobra.Command{
		Use:   "ingresses <realization>.<experiment>.<project>",
		Short: "List the ingresses of the given materialization.",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			listIngs(args[0])
		},
	}
	list.AddCommand(listIng)

	newIng := &cobra.Command{
		Use:   "ingress <realization>.<experiment>.<project> <host> <port> <protocol>",
		Short: "Add a new ingress to the given materialization.",
		Args:  cobra.ExactArgs(4),
		Run: func(cmd *cobra.Command, args []string) {
			newIng(args[0], args[1], args[2], args[3])
		},
	}
	newcmd.AddCommand(newIng)

	delIng := &cobra.Command{
		Use:   "ingress <realization>.<experiment>.<project> <host> <port> <protocol>",
		Short: "Delete an ingress from the given materialization.",
		Args:  cobra.ExactArgs(4),
		Run: func(cmd *cobra.Command, args []string) {
			delIng(args[0], args[1], args[2], args[3])
		},
	}
	del.AddCommand(delIng)
}

func listIngs(mzid string) {

	rid, eid, pid := parseRzid(mzid)

	clients.Materialize(endpoint(), token(), func(cli portal.MaterializeClient) error {
		resp, err := cli.GetMaterialization(context.TODO(), &portal.GetMaterializationRequest{
			Project:     pid,
			Experiment:  eid,
			Realization: rid,
		})
		if err != nil {
			handleError(err)
		}

		if jsonOut {
			fmt.Printf("%s\n", protojson.Format(resp.Ingresses))
			return nil
		}

		if resp.Ingresses == nil || len(resp.Ingresses.Ingresses) == 0 {
			return nil
		}

		fmt.Print(shcli.TableColumns(shcli.Blue, "Ingresses"))
		fmt.Fprint(tw, shcli.TableColumns(shcli.Blue, "mzid", "protocol", "host", "port", "address", "ingress"))

		for _, ing := range resp.Ingresses.Ingresses {

			var ingpath string
			switch ing.Protocol {
			case "tcp", "udp":
				ingpath = fmt.Sprintf("%s:%d", ing.Gateway, ing.Gatewayport)
			case "http":
				ingpath = fmt.Sprintf(
					"http://%s:80/%s/%s/%d",
					ing.Gateway,
					ing.Mzid,
					ing.Hostname,
					ing.Hostport,
				)
			case "https":
				ingpath = fmt.Sprintf(
					"https://%s:443/%s/%s/%d",
					ing.Gateway,
					ing.Mzid,
					ing.Hostname,
					ing.Hostport,
				)
			}

			fmt.Fprint(tw, shcli.TableRow(
				shcli.TableItem{Value: ing.Mzid},
				shcli.TableItem{Value: ing.Protocol},
				shcli.TableItem{Value: ing.Hostname},
				shcli.TableItem{Value: ing.Hostport},
				shcli.TableItem{Value: ing.Hostaddr},
				shcli.TableItem{Value: ingpath},
			))
		}

		tw.Flush()

		return nil
	})
}

func newIng(mzid, host, port, proto string) {

	if _, ok := xir.Protocol_value[proto]; !ok {

		keys := []string{}
		for key := range xir.Protocol_value {
			keys = append(keys, key)
		}

		fmt.Printf("Protocol must be one of: %s.\n", strings.Join(keys, ", "))
		os.Exit(1)
	}

	clients.Materialize(endpoint(), token(), func(cli portal.MaterializeClient) error {

		rid, eid, pid := parseRzid(mzid)
		iPort, err := strconv.Atoi(port)
		if err != nil {
			log.Fatal(err)
		}

		_, err = cli.NewIngress(
			context.TODO(),
			&portal.NewIngressRequest{
				Project:     pid,
				Experiment:  eid,
				Realization: rid,
				Protocol:    xir.Protocol(xir.Protocol_value[proto]),
				Host:        host,
				Port:        uint32(iPort),
			})

		if err != nil {
			log.Fatal(err)
		}

		return nil
	},
	)
}

func delIng(mzid, host, port, proto string) {

	if _, ok := xir.Protocol_value[proto]; !ok {

		keys := []string{}
		for key := range xir.Protocol_value {
			keys = append(keys, key)
		}

		fmt.Printf("Protocol must be one of: %s.\n", strings.Join(keys, ", "))
		os.Exit(1)
	}

	clients.Materialize(endpoint(), token(), func(cli portal.MaterializeClient) error {

		rid, eid, pid := parseRzid(mzid)
		iPort, err := strconv.Atoi(port)
		if err != nil {
			log.Fatal(err)
		}

		_, err = cli.DeleteIngress(
			context.TODO(),
			&portal.DeleteIngressRequest{
				Project:     pid,
				Experiment:  eid,
				Realization: rid,
				Protocol:    xir.Protocol(xir.Protocol_value[proto]),
				Host:        host,
				Port:        uint32(iPort),
			})

		if err != nil {
			log.Fatal(err)
		}

		return nil
	},
	)
}
