package main

import (
	"context"
	"fmt"
	"sort"
	"strings"

	"github.com/spf13/cobra"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/cli/clients"
	"google.golang.org/protobuf/encoding/protojson"
)

func vpn() {
	showEnc := &cobra.Command{
		Use:   "vpn <realization>.<experiment>.<project>",
		Short: "Show VPN details for the given materialization",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			showVPN(args[0])
		},
	}
	show.AddCommand(showEnc)
}

func showVPN(encid string) {
	// example JSON:
	// {
	//   "enclave":  {
	//     "enclaveid":  "rlz.exp.glawler",
	//     "gateways":  {
	//       "S22K6PeQDzZfpyALBXElWiTv5R4MMIlbzynzG1k9MDs=":  {
	//         "endpoint":  "ifr:44935",
	//         "key":  "S22K6PeQDzZfpyALBXElWiTv5R4MMIlbzynzG1k9MDs=",
	//         "allowedips":  [
	//           "192.168.254.0/24",
	//           "172.30.0.0/16"
	//         ]
	//       }
	//     },
	//     "clients":  {
	//       "NEYHSTtDw++Tv6GDObwp9uUlGnQA2q1KTqavH/buOVM=":  {
	//         "endpoint":  "xdc.glawler",
	//         "key":  "NEYHSTtDw++Tv6GDObwp9uUlGnQA2q1KTqavH/buOVM=",
	//         "allowedips":  [
	//           "192.168.254.2"
	//         ]
	//       }
	//     },
	//     "Ver":  "3"
	//   }
	// }

	clients.Wireguard(endpoint(), token(), func(cli portal.WireguardClient) error {

		resp, err := cli.GetWgEnclave(
			context.TODO(),
			&portal.GetWgEnclaveRequest{
				Enclaveid: encid,
			},
		)
		if err != nil {
			handleError(err)
		}

		if jsonOut {
			fmt.Printf("%s\n", protojson.Format(resp))
			return nil
		}

		sortedKeys := func(items map[string]*portal.WgIfConfig) []string {
			keys := []string{}
			for key := range items {
				keys = append(keys, key)
			}
			sort.SliceStable(keys, func(i, j int) bool {
				return items[keys[i]].Endpoint < items[keys[j]].Endpoint
			})

			return keys
		}

		enc := resp.Enclave

		fmt.Printf("Enclave ID: %s\n", enc.Enclaveid)

		fmt.Printf("\nGateways:\n")
		fmt.Fprintf(tw, "Endpoint\tPublic Key\tAccess Addr\tAllowed IPs\n")
		fmt.Fprintf(tw, "--------\t----------\t-----------\t-----------\n")

		keys := sortedKeys(enc.Gateways)
		for _, key := range keys {
			fmt.Fprintf(tw,
				"%s\t%s\t%s\t%s\n",
				enc.Gateways[key].Endpoint,
				enc.Gateways[key].Key[0:8],
				enc.Gateways[key].Accessaddr,
				strings.Join(enc.Gateways[key].Allowedips, ", "),
			)
		}
		tw.Flush()

		fmt.Printf("\nClients:\n")
		fmt.Fprintf(tw, "Endpoint\tPublic Key\tAccess Addr\tAllowed IPs\n")
		fmt.Fprintf(tw, "--------\t----------\t-----------\t-----------\n")

		keys = sortedKeys(enc.Clients)
		for _, key := range keys {
			fmt.Fprintf(tw,
				"%s\t%s\t%s\t%s\n",
				enc.Clients[key].Endpoint,
				enc.Clients[key].Key[0:8],
				enc.Clients[key].Accessaddr,
				strings.Join(enc.Clients[key].Allowedips, ", "),
			)
		}
		tw.Flush()

		return nil
	})
}
