package main

import (
	"bufio"
	"context"
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/cli/clients"
	"google.golang.org/protobuf/encoding/protojson"
)

var pool = &cobra.Command{
	Use:   "pools",
	Short: "Pool specific commands",
}

func pools() {

	var (
		desc       string
		projects   string
		facilities string
	)
	newpool := &cobra.Command{
		Use:   "pool <name>",
		Short: "Create a new pool (with optional projects and description added)",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			newPool(args[0], desc, projects, facilities)
		},
	}
	newpool.Flags().StringVarP(&desc, "desc", "d", "", "description")
	newpool.Flags().StringVarP(&projects, "projects", "", "",
		"Comma separated list of projects. (ex: murphy,olive,projone)")
	newpool.Flags().StringVarP(&facilities, "facilities", "", "",
		"Comma separated list of facilities. (added without resources)")
	newcmd.AddCommand(newpool)

	delpool := &cobra.Command{
		Use:   "pool <name>",
		Short: "Delete a pool",
		Args:  cobra.ExactArgs(1),
		Run:   func(cmd *cobra.Command, args []string) { delPool(args[0]) },
	}
	del.AddCommand(delpool)

	var (
		verbose bool
	)
	poollist := &cobra.Command{
		Use:   "pools",
		Short: "Get pools",
		Args:  cobra.NoArgs,
		Run:   func(cmd *cobra.Command, args []string) { listPool(verbose) },
	}
	poollist.Flags().BoolVarP(&verbose, "verbose", "", false, "Fully show each pool")
	list.AddCommand(poollist)

	poolshow := &cobra.Command{
		Use:   "pool <name>",
		Short: "Get pool show",
		Args:  cobra.ExactArgs(1),
		Run:   func(cmd *cobra.Command, args []string) { poolShow(args[0]) },
	}
	show.AddCommand(poolshow)

	add := &cobra.Command{
		Use:   "add",
		Short: "Add things to a pool",
	}
	pool.AddCommand(add)

	addproj := &cobra.Command{
		Use:   "project <pool> <project>",
		Short: "Add a project to a pool",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {

			addProject(args[0], args[1])

		},
	}
	add.AddCommand(addproj)

	addFacility := &cobra.Command{
		Use:   "facility <pool> <facility> <node> ... <node>",
		Short: "Add the given facility to the pool with optional resources. If first <node> is - then standard in is read.",
		Args:  cobra.MinimumNArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			addFacility(args[0], args[1], args[2:])
		},
	}
	add.AddCommand(addFacility)

	addResources := &cobra.Command{
		Use:   "resources <pool> <facility> <node> ... <node>",
		Short: "Add the given facility resouces the pool. If first <node> is - then standard in is read.",
		Args:  cobra.MinimumNArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			updateFacility(args[0], args[1], args[2:], true)
		},
	}
	add.AddCommand(addResources)

	pooldel := &cobra.Command{
		Use:   "delete",
		Short: "Delete things from a pool",
	}
	pool.AddCommand(pooldel)

	delproj := &cobra.Command{
		Use:   "project <pool> <project>",
		Short: "Delete a project from a pool",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {

			deletePoolProject(args[0], args[1])

		},
	}
	pooldel.AddCommand(delproj)

	delfacility := &cobra.Command{
		Use:   "facility <pool> <facility>",
		Short: "Delete a facility from a pool",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {

			deletePoolFacility(args[0], args[1])

		},
	}
	pooldel.AddCommand(delfacility)

	delResources := &cobra.Command{
		Use:   "resources <pool> <facility> <node> ... <node>",
		Short: "Remove the given facility resouces the pool. If first <node> is - then standard in is read.",
		Args:  cobra.MinimumNArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			updateFacility(args[0], args[1], args[2:], false)
		},
	}
	pooldel.AddCommand(delResources)

	root.AddCommand(pool)
}

func newPool(name, desc, projs, facs string) {

	projects := []string{}
	if projs != "" {
		projects = strings.Split(projs, ",")
	}

	facilities := []string{}
	if facs != "" {
		facilities = strings.Split(facs, ",")
	}

	clients.Alloc(
		endpoint(), token(),
		func(cli portal.AllocClient) error {

			_, err := cli.CreatePool(
				context.TODO(),
				&portal.CreatePoolRequest{
					Name:        name,
					Description: desc,
					Projects:    projects,
					Facilities:  facilities,
				},
			)

			if err != nil {
				handleError(err)
			}

			return nil
		},
	)
}

func delPool(name string) {

	clients.Alloc(
		endpoint(), token(),
		func(cli portal.AllocClient) error {

			_, err := cli.DeletePool(
				context.TODO(),
				&portal.DeletePoolRequest{
					Name: name,
				},
			)

			if err != nil {
				handleError(err)
			}

			return nil
		},
	)
}

func listPool(verbose bool) {

	clients.Alloc(
		endpoint(), token(),
		func(cli portal.AllocClient) error {

			resp, err := cli.GetPools(
				context.TODO(),
				&portal.GetPoolsRequest{},
			)

			if err != nil {
				handleError(err)
			}

			if !verbose {
				fmt.Fprintf(tw, "%s\t%s\t%s\t%s\t%s\t%s\t%s\n", "Name", "Creator", "Facilities", "Projects", "Organzations", "Resources", "Description")
				fmt.Fprintf(tw, "%s\t%s\t%s\t%s\t%s\t%s\t%s\n", "----", "-------", "----------", "--------", "------------", "---------", "-----------")

				for _, p := range resp.Pools {
					facilities := []string{}
					recCnt := 0

					for f, res := range p.Facilities {
						facilities = append(facilities, f)
						recCnt += len(res.Resources)
					}

					fmt.Fprintf(
						tw,
						"%s\t%s\t%s\t%s\t%s\t%d\t%s\n",
						p.Name,
						p.Creator,
						strings.Join(facilities, ", "),
						strings.Join(p.Projects, ", "),
						strings.Join(p.Organizations, ", "),
						recCnt,
						p.Description)
				}

				tw.Flush()

			} else {

				for _, p := range resp.Pools {
					printPool(p)
				}
			}

			return nil
		},
	)
}

func printPool(p *portal.Pool) {

	fmt.Printf("Name: %s\n", p.Name)
	fmt.Printf("Description: %s\n", p.Description)
	fmt.Printf("Creator: %s\n", p.Creator)
	fmt.Printf("Projects: %s\n", strings.Join(p.Projects, " "))

	if len(p.Facilities) > 0 {
		fmt.Printf("Resources: \n")
		for facility, resources := range p.Facilities {
			fmt.Printf("\t%s: %s\n", facility, strings.Join(resources.Resources, " "))
		}
	} else {
		fmt.Printf("Resources: None\n")
	}
}

func poolShow(name string) {

	clients.Alloc(
		endpoint(), token(),
		func(cli portal.AllocClient) error {

			resp, err := cli.GetPool(
				context.TODO(),
				&portal.GetPoolRequest{
					Name: name,
				},
			)

			if err != nil {
				handleError(err)
			}

			if jsonOut {
				fmt.Printf("%s\n", protojson.Format(resp))
				return nil
			}

			printPool(resp.Pool)

			return nil
		},
	)
}

func addProject(pool, project string) {

	clients.Alloc(
		endpoint(), token(),
		func(cli portal.AllocClient) error {

			_, err := cli.AddProject(
				context.TODO(),
				&portal.AddProjectToPoolRequest{
					Name:    pool,
					Project: project,
				},
			)

			if err != nil {
				handleError(err)
			}

			return nil
		},
	)
}

func addFacility(pool, facility string, nodes []string) {

	resources := nodes

	if len(nodes) > 0 && nodes[0] == "-" {
		resources = readStdInStrings()
	}

	clients.Alloc(
		endpoint(), token(),
		func(cli portal.AllocClient) error {

			_, err := cli.AddFacility(
				context.TODO(),
				&portal.AddFacilityToPoolRequest{
					Name:      pool,
					Facility:  facility,
					Resources: resources,
				},
			)

			if err != nil {
				handleError(err)
			}

			return nil
		},
	)
}

func updateFacility(pool, facility string, nodes []string, add bool) {
	resources := nodes

	if len(nodes) > 0 && nodes[0] == "-" {
		resources = readStdInStrings()
	}

	ps := &portal.PatchStrategy{}
	if add {
		ps.Strategy = portal.PatchStrategy_expand
	} else {
		ps.Strategy = portal.PatchStrategy_subtract
	}

	clients.Alloc(
		endpoint(), token(),
		func(cli portal.AllocClient) error {

			_, err := cli.UpdatePoolResources(
				context.TODO(),
				&portal.UpdatePoolResourcesRequest{
					Pool:          pool,
					Facility:      facility,
					Resources:     resources,
					Patchstrategy: ps,
				},
			)

			if err != nil {
				handleError(err)
			}

			return nil
		},
	)
}

func deletePoolFacility(pool, facility string) {

	clients.Alloc(
		endpoint(), token(),
		func(cli portal.AllocClient) error {

			_, err := cli.RemoveFacility(
				context.TODO(),
				&portal.RemoveFacilityFromPoolRequest{
					Name:     pool,
					Facility: facility,
				},
			)

			if err != nil {
				handleError(err)
			}

			return nil
		},
	)
}

func deletePoolProject(pool, project string) {

	clients.Alloc(
		endpoint(), token(),
		func(cli portal.AllocClient) error {

			_, err := cli.RemoveProject(
				context.TODO(),
				&portal.RemoveProjectFromPoolRequest{
					Name:    pool,
					Project: project,
				},
			)

			if err != nil {
				handleError(err)
			}

			return nil
		},
	)
}

func readStdInStrings() []string {

	result := []string{}

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		result = append(result, strings.Fields(scanner.Text())...)
	}

	return result
}
