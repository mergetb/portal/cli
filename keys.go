package main

import (
	"context"
	"fmt"
	"golang.org/x/crypto/ssh"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/cli/clients"
)

func keys() {

	listcmd := &cobra.Command{
		Use:   "keys",
		Short: "Get SSH public keys",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			listkeys()
		},
	}
	list.AddCommand(listcmd)

	addcmd := &cobra.Command{
		Use:   "key [filename]",
		Short: "Add a SSH public key",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			addkey(args[0])
		},
	}
	newcmd.AddCommand(addcmd)

	delcmd := &cobra.Command{
		Use:   "key [filename | fingerprint]",
		Short: "Delete a SSH public key",
		Long:  "Delete a SSH public key\n\nRemove a public key by specifying either the filename containing the public key, or by the fingerprint as output by the list keys command.",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			delkey(args[0])
		},
	}
	del.AddCommand(delcmd)

}

func listkeys() {

	var keys []*portal.PublicKey

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		resp, err := cli.GetUserPublicKeys(context.TODO(), &portal.GetUserPublicKeysRequest{
			User: getLoggedInUsername(),
		})
		if err != nil {
			handleError(err)
		}

		keys = resp.Keys

		return nil

	})

	for _, k := range keys {
		p, comment, _, _, err := ssh.ParseAuthorizedKey([]byte(k.Key))
		if err != nil {
			log.Fatalf("parse ssh pubkey: %v", err)
		}
		fmt.Printf("%s %s %s\n", p.Type(), k.Fingerprint, comment)
	}

}

func addkey(filename string) {

	buf, err := os.ReadFile(filename)
	if err != nil {
		log.Fatalf("read pubkey: %v", err)
	}
	key := string(buf)

	// validate key
	if _, _, _, _, err := ssh.ParseAuthorizedKey(buf); err != nil {
		log.Fatalf("could not parse public key: %v", err)
	}

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		_, err := cli.AddUserPublicKey(context.TODO(), &portal.AddUserPublicKeyRequest{
			User: getLoggedInUsername(),
			Key:  key,
		})
		if err != nil {
			handleError(err)
		}

		return nil

	})

}

func delkey(filename string) {
	var fingerprint string

	// determine if key is specified by filename or fingerprint
	if strings.HasPrefix(filename, "SHA256:") {
		fingerprint = filename
	} else {
		buf, err := os.ReadFile(filename)
		if err != nil {
			log.Fatalf("read pubkey: %v", err)
		}
		pubkey, _, _, _, err := ssh.ParseAuthorizedKey(buf)
		if err != nil {
			log.Fatalf("unable to parse public key: %v", err)
		}
		fingerprint = ssh.FingerprintSHA256(pubkey)
	}

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		_, err := cli.DeleteUserPublicKey(context.TODO(), &portal.DeleteUserPublicKeyRequest{
			User:        getLoggedInUsername(),
			Fingerprint: fingerprint,
		})
		if err != nil {
			handleError(err)
		}

		return nil

	})

}
