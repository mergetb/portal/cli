# mrg

**mrg** is a command-line tool used to interact and manage a Merge
testbed and experiments.

See [Command Line Interface Reference](https://next.mergetb.org/docs/experimentation/cli-reference/).

## install

There are two ways to install `mrg`, via:
1. [binaries](#download-and-install-binaries) (*recommended*)
2. [source](#build-and-install-from-source)

### download and install binaries

1. Navigate to the [latest mrg release](https://gitlab.com/mergetb/portal/cli/-/releases/permalink/latest/)
2. Download the appropriate binary for your architecture
3. (*optional*) Copy the binary to `/usr/local/bin` or your preferred
   `PATH`

Example for non-Windows OSes:
```shell
curl -L -o mrg https://gitlab.com/mergetb/portal/cli/-/releases/permalink/latest/downloads/binaries/mrg-$(uname -s | tr A-Z a-z)-$(uname -m)
chmod +x mrg
sudo mv mrg /usr/local/bin/
```

### build and install from source

Prerequisites: Go

Build and install:
```shell
# clone git repository
git clone https://gitlab.com/mergetb/portal/cli/

# build for the current architecture (run `make all` to build for *all*
# architectures)
make build/$(uname -m)/$(uname -s | tr A-Z a-z)/mrg

# assumes linux and x86_64
sudo make install_linux
```

## usage

See [Command Line Interface Reference](https://next.mergetb.org/docs/experimentation/cli-reference/).

## license

TODO
