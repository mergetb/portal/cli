package main

import (
	"context"
	"sort"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/cli/clients"
)

func member() {

	var role string

	// add

	addMemberCmd := &cobra.Command{
		Use:   "member",
		Short: "Add a member to an entity",
	}

	addProjectMember := &cobra.Command{
		Use:   "project <project> <username>",
		Short: "Add a user to a project",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			addProjectMember(args[0], args[1], role)
		},
	}

	addOrganizationCmd := &cobra.Command{
		Use:   "organization",
		Short: "Add a member to an organization",
	}

	addOrganizationProject := &cobra.Command{
		Use:   "project <organization> <project>",
		Short: "Add a project to an organization",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			addOrganizationProject(args[0], args[1], role)
		},
	}

	addOrganizationCmd.AddCommand(addOrganizationProject)
	addMemberCmd.AddCommand(addProjectMember)
	newcmd.AddCommand(addMemberCmd)

	// del

	delMemberCmd := &cobra.Command{
		Use:   "member",
		Short: "Remove a member from an entity",
	}

	delProjectMember := &cobra.Command{
		Use:   "project <project> <username>",
		Short: "Remove a user from a project",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			delProjectMember(args[0], args[1])
		},
	}

	delOrganizationCmd := &cobra.Command{
		Use:   "organization",
		Short: "Remove a member from an organization",
	}

	delOrganizationMember := &cobra.Command{
		Use:   "user <organization> <username>",
		Short: "Remove a user from an organization",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			delOrganizationMember(args[0], args[1])
		},
	}

	delOrganizationProject := &cobra.Command{
		Use:   "project <organization> <project>",
		Short: "Remove a project from an organization",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			delOrganizationProject(args[0], args[1])
		},
	}

	delOrganizationCmd.AddCommand(delOrganizationMember)
	delOrganizationCmd.AddCommand(delOrganizationProject)
	delMemberCmd.AddCommand(delProjectMember)
	delMemberCmd.AddCommand(delOrganizationCmd)
	del.AddCommand(delMemberCmd)

	// update

	updateMemberCmd := &cobra.Command{
		Use:   "member",
		Short: "Update a member in an entity",
	}

	updateProjectMember := &cobra.Command{
		Use:   "project <project> <username>",
		Short: "Update a user in a project",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			updateProjectMember(args[0], args[1], role)
		},
	}

	updateOrganizationCmd := &cobra.Command{
		Use:   "organization",
		Short: "Update a member in an organization",
	}

	updateOrganizationMember := &cobra.Command{
		Use:   "user <organization> <username>",
		Short: "Update a user in an organization",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			updateOrganizationMember(args[0], args[1], role)
		},
	}

	updateOrganizationProject := &cobra.Command{
		Use:   "project <organization> <project>",
		Short: "Update a project in an organization",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			updateOrganizationProject(args[0], args[1], role)
		},
	}

	updateOrganizationCmd.AddCommand(updateOrganizationMember)
	updateOrganizationCmd.AddCommand(updateOrganizationProject)
	updateMemberCmd.AddCommand(updateProjectMember)
	updateMemberCmd.AddCommand(updateOrganizationCmd)
	update.AddCommand(updateMemberCmd)

	//
	// Membership commands. Request and confirm/deny membership.
	//
	membershipCmd := &cobra.Command{
		Use:   "membership",
		Short: "Membership commands",
		Long:  "Commands around membership in entities. Requesting, approving, denying membership in organizations or projects.",
	}
	root.AddCommand(membershipCmd)

	// membership confirm
	confirmCmd := &cobra.Command{
		Use:   "confirm",
		Short: "Confirm a member in an entity",
	}
	membershipCmd.AddCommand(confirmCmd)

	// membership confirm organization
	confirmOrgCmd := &cobra.Command{
		Use:   "organization",
		Short: "Confirm a member in an organization",
	}
	confirmCmd.AddCommand(confirmOrgCmd)

	// membership confirm organization user <org> <username>
	confirmOrgUserCmd := &cobra.Command{
		Use:   "user <organization> <username>",
		Short: "Confirm a user in an organization",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			confirmOrganizationMember(args[0], args[1], portal.MembershipType_UserMember)
		},
	}
	confirmOrgCmd.AddCommand(confirmOrgUserCmd)

	// membership deny
	denyCmd := &cobra.Command{
		Use:   "deny",
		Short: "Deny a member in an entity",
	}
	membershipCmd.AddCommand(denyCmd)

	// membership deny organization
	denyOrgCmd := &cobra.Command{
		Use:   "organization",
		Short: "Deny a member in an organization",
	}
	denyCmd.AddCommand(denyOrgCmd)

	// membership deny organization user <org> <username>
	denyOrgUserCmd := &cobra.Command{
		Use:   "user <organization> <username>",
		Short: "Deny a user membership in an organization",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			denyOrganizationMember(args[0], args[1], portal.MembershipType_UserMember)
		},
	}
	denyOrgCmd.AddCommand(denyOrgUserCmd)

	// membership request
	requestCmd := &cobra.Command{
		Use:   "request",
		Short: "Request membership in an entity",
	}
	membershipCmd.AddCommand(requestCmd)

	// membership request organization
	reqOrgMembership := &cobra.Command{
		Use:   "organization",
		Short: "request membership in an organization",
	}
	requestCmd.AddCommand(reqOrgMembership)

	// membership request organization user <org> <user>
	reqOrgUserMembershipCmd := &cobra.Command{
		Use:   "user [organization] [username]",
		Short: "Request a user membership in an organization",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			requestOrgMembership(args[0], args[1], role, portal.MembershipType_UserMember)
		},
	}
	reqOrgMembership.AddCommand(reqOrgUserMembershipCmd)

	role_cmds := []*cobra.Command{
		addProjectMember,
		addOrganizationProject,
		updateProjectMember,
		updateOrganizationMember,
		updateOrganizationProject,
		reqOrgUserMembershipCmd,
	}
	for _, cmd := range role_cmds {
		cmd.Flags().StringVarP(&role, "role", "r",
			portal.Member_Member.String(),
			"One of "+strings.Join(memberRoles(), ", "),
		)
	}

}

func memberRoles() []string {

	keys := []int{}
	for k := range portal.Member_Role_name {
		keys = append(keys, int(k))
	}
	sort.Ints(keys)

	r := []string{}
	for k := range keys {
		r = append(r, portal.Member_Role_name[int32(k)])
	}

	return r
}

func addProjectMember(pid, uid, role string) {

	if _, ok := portal.Member_Role_value[role]; !ok {
		log.Fatalf("Bad member role. Must be one of %s", strings.Join(memberRoles(), ", "))
	}

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		_, err := cli.AddProjectMember(
			context.TODO(),
			&portal.AddProjectMemberRequest{
				Project:  pid,
				Username: uid,
				Member: &portal.Member{
					Role: portal.Member_Role(portal.Member_Role_value[role]),
				},
			},
		)
		if err != nil {
			handleError(err)
		}

		return nil
	})
}

func addOrganizationProject(oid, pid, role string) {

	if _, ok := portal.Member_Role_value[role]; !ok {
		log.Fatalf("Bad member role. Must be one of %s", strings.Join(memberRoles(), ", "))
	}

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		_, err := cli.AddOrganizationProject(
			context.TODO(),
			&portal.AddOrganizationProjectRequest{
				Organization: oid,
				Project:      pid,
				Member: &portal.Member{
					Role: portal.Member_Role(portal.Member_Role_value[role]),
				},
			},
		)
		if err != nil {
			handleError(err)
		}

		return nil
	})

}

func delProjectMember(pid, uid string) {

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		_, err := cli.DeleteProjectMember(
			context.TODO(),
			&portal.DeleteProjectMemberRequest{
				Project: pid,
				Member:  uid,
			},
		)
		if err != nil {
			handleError(err)
		}
		return nil
	})
}

func delOrganizationMember(oid, uid string) {

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		_, err := cli.DeleteOrganizationMember(
			context.TODO(),
			&portal.DeleteOrganizationMemberRequest{
				Organization: oid,
				Username:     uid,
			},
		)
		if err != nil {
			handleError(err)
		}

		return nil
	})

}

func delOrganizationProject(oid, pid string) {

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		_, err := cli.DeleteOrganizationProject(
			context.TODO(),
			&portal.DeleteOrganizationProjectRequest{
				Organization: oid,
				Project:      pid,
			},
		)
		if err != nil {
			handleError(err)
		}

		return nil
	})

}

func updateProjectMember(pid, uid, role string) {

	if _, ok := portal.Member_Role_value[role]; !ok {
		log.Fatalf("Bad member role. Must be one of %s", strings.Join(memberRoles(), ", "))
	}

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		_, err := cli.UpdateProjectMember(
			context.TODO(),
			&portal.UpdateProjectMemberRequest{
				Project:  pid,
				Username: uid,
				Member: &portal.Member{
					Role: portal.Member_Role(portal.Member_Role_value[role]),
				},
			},
		)
		if err != nil {
			handleError(err)
		}

		return nil
	})
}

func updateOrganizationMember(oid, uid, role string) {

	if _, ok := portal.Member_Role_value[role]; !ok {
		log.Fatalf("Bad member role. Must be one of %s", strings.Join(memberRoles(), ", "))
	}

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		_, err := cli.UpdateOrganizationMember(
			context.TODO(),
			&portal.UpdateOrganizationMemberRequest{
				Organization: oid,
				Username:     uid,
				Member: &portal.Member{
					Role: portal.Member_Role(portal.Member_Role_value[role]),
				},
			},
		)
		if err != nil {
			handleError(err)
		}

		return nil
	})

}

func updateOrganizationProject(oid, pid, role string) {

	if _, ok := portal.Member_Role_value[role]; !ok {
		log.Fatalf("Bad member role. Must be one of %s", strings.Join(memberRoles(), ", "))
	}

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		_, err := cli.UpdateOrganizationProject(
			context.TODO(),
			&portal.UpdateOrganizationProjectRequest{
				Organization: oid,
				Project:      pid,
				Member: &portal.Member{
					Role: portal.Member_Role(portal.Member_Role_value[role]),
				},
			},
		)
		if err != nil {
			handleError(err)
		}

		return nil
	})

}

func confirmOrganizationMember(org, id string, kind portal.MembershipType) {

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		_, err := cli.ConfirmOrganizationMembership(
			context.TODO(),
			&portal.ConfirmOrganizationMembershipRequest{
				Organization: org,
				Id:           id,
				Kind:         kind,
			},
		)
		if err != nil {
			handleError(err)
		}

		return nil
	})

	checkUser(id)
}

func denyOrganizationMember(org, id string, kind portal.MembershipType) {

	switch kind {
	case portal.MembershipType_ProjectMember:
		delOrganizationProject(org, id)
	case portal.MembershipType_UserMember:
		delOrganizationMember(org, id)
	}

}
func requestOrgMembership(org, username, role string, kind portal.MembershipType) {

	if _, ok := portal.Member_Role_value[role]; !ok {
		log.Fatalf("Bad member role. Must be one of %s", strings.Join(memberRoles(), ", "))
	}

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {
		_, err := cli.RequestOrganizationMembership(
			context.TODO(),
			&portal.RequestOrganizationMembershipRequest{
				Organization: org,
				Id:           username,
				Kind:         kind,
				Member: &portal.Member{
					Role: portal.Member_Role(portal.Member_Role_value[role]),
				},
			},
		)
		if err != nil {
			handleError(err)
		}

		return nil
	})

	// users not logged in can't check if the user is properly created,
	// so don't bother
	if len(token()) > 0 {
		checkUser(username)
	}
}
