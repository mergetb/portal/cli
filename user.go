package main

import (
	"context"
	"fmt"

	"github.com/spf13/cobra"
	"google.golang.org/protobuf/encoding/protojson"

	"gitlab.com/mergetb/portal/cli/clients"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	shcli "gitlab.com/mergetb/tech/shared/cli"
	pviews "gitlab.com/mergetb/tech/shared/cli/portal"
)

func user() {

	uinit := &cobra.Command{
		Use:   "init <username>",
		Short: "Initialize a user",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			userinit(args[0])
		},
	}
	root.AddCommand(uinit)

	users := &cobra.Command{
		Use:   "users",
		Short: "List all users",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			listusers()
		},
	}
	list.AddCommand(users)

	delUser := &cobra.Command{
		Use:   "user <username>",
		Short: "Delete a user",
		Long:  "Delete a user. \n\nNote that this only removes the user from the Merge testbed. The user ID remains.",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			userDelete(args[0])
		},
	}
	del.AddCommand(delUser)

	var withStatus bool
	showUser := &cobra.Command{
		Use:   "user <username>",
		Short: "Show details of the given user",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			userShow(args[0], withStatus)
		},
	}
	showUser.Flags().BoolVarP(&withStatus, "with-status", "S", false, "Show task status of user")
	show.AddCommand(showUser)

	// Update users subcommand.
	updateUser := &cobra.Command{
		Use:   "user <username>",
		Short: "Update an existing user's information",
	}

	updateName := &cobra.Command{
		Use:   "name <username> <new name>",
		Short: "Update a user's colloquial name",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			updateUserDetails(args[0], args[1], "", "", "", "", "")
		},
	}
	updateUser.AddCommand(updateName)

	updateInst := &cobra.Command{
		Use:   "institution <username> <new institution>",
		Short: "Update a user's institution",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			updateUserDetails(args[0], "", args[1], "", "", "", "")
		},
	}
	updateUser.AddCommand(updateInst)

	updateCat := &cobra.Command{
		Use:   "category <username> <new category>",
		Short: "Update a user's category",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			updateUserDetails(args[0], "", "", args[1], "", "", "")
		},
	}
	updateUser.AddCommand(updateCat)

	updateEmail := &cobra.Command{
		Use:   "email <username> <new email>",
		Short: "Update a user's email",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			updateUserDetails(args[0], "", "", "", args[1], "", "")
		},
	}
	updateUser.AddCommand(updateEmail)

	updateCountry := &cobra.Command{
		Use:   "country <username> <new country>",
		Short: "Update a user's country",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			updateUserDetails(args[0], "", "", "", "", args[1], "")
		},
	}
	updateUser.AddCommand(updateCountry)

	updateUsstate := &cobra.Command{
		Use:   "state <username> <new state>",
		Short: "Update a user's state within a country",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			updateUserDetails(args[0], "", "", "", "", "", args[1])
		},
	}
	updateUser.AddCommand(updateUsstate)

	updateMode := &cobra.Command{
		Use:   "mode <username> <accessmode>",
		Short: "Update a user's access mode",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			updateMode(args[0], args[1])
		},
	}
	updateUser.AddCommand(updateMode)

	toggleAdmin := &cobra.Command{
		Use:   "admin <username>",
		Short: "Toggle a user's admin status",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			toggleAdmin(args[0])
		},
	}
	updateUser.AddCommand(toggleAdmin)

	// add update subcommands.
	update.AddCommand(updateUser)
}

func updateMode(user, mode string) {

	m, err := ToAccess(mode)
	if err != nil {
		handleError(err)
	}

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {
		_, err := cli.UpdateUser(
			context.TODO(),
			&portal.UpdateUserRequest{
				Username: user,
				AccessMode: &portal.AccessModeUpdate{
					Value: m,
				},
			},
		)

		if err != nil {
			handleError(err)
		}

		return nil
	})

	checkUser(user)
}

func updateUserDetails(user, name, institution, category, email, country, usstate string) {

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {
		_, err := cli.UpdateUser(
			context.TODO(),
			&portal.UpdateUserRequest{
				Username:    user,
				Name:        name,
				Institution: institution,
				Category:    category,
				Email:       email,
				Country:     country,
				Usstate:     usstate,
			},
		)

		if err != nil {
			handleError(err)
		}

		return nil
	})

	checkUser(user)
}

func toggleAdmin(user string) {

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {
		_, err := cli.UpdateUser(
			context.TODO(),
			&portal.UpdateUserRequest{
				Username:    user,
				ToggleAdmin: true,
			},
		)

		if err != nil {
			handleError(err)
		}

		return nil
	})

	checkUser(user)
}

func userinit(username string) {

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		_, err := cli.InitUser(context.TODO(), &portal.InitUserRequest{
			Username: username,
		})
		if err != nil {
			handleError(err)
		}

		return nil

	})

	checkUser(username)
}

func listusers() {

	var userResp *portal.GetUsersResponse

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		resp, err := cli.GetUsers(context.TODO(), &portal.GetUsersRequest{})
		if err != nil {
			handleError(err)
		}

		userResp = resp

		return nil

	})

	if jsonOut {
		fmt.Printf("%s\n", protojson.Format(userResp))
		return
	}

	users := userResp.Users

	fmt.Fprint(tw, shcli.TableColumns(shcli.Blue, "Username", "Email", "Full Name", "Institution", "Category",
		"Country", "US State", "Mode", "Account State", "UID", "GID"))

	for _, u := range users {
		un := u.Username
		if u.Admin {
			un += "*"
		}
		fmt.Fprint(tw, shcli.TableRow(
			shcli.TableItem{Value: un},
			shcli.TableItem{Value: u.Email},
			shcli.TableItem{Value: u.Name},
			shcli.TableItem{Value: u.Institution},
			shcli.TableItem{Value: u.Category},
			shcli.TableItem{Value: u.Country},
			shcli.TableItem{Value: u.Usstate},
			shcli.TableItem{Value: u.AccessMode},
			shcli.TableItem{Value: u.State},
			shcli.TableItem{Value: u.Uid},
			shcli.TableItem{Value: u.Gid},
		))
	}
	tw.Flush()

}

func userDelete(username string) {

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		_, err := cli.DeleteUser(
			context.TODO(),
			&portal.DeleteUserRequest{
				User: username,
			},
		)
		if err != nil {
			handleError(err)
		}

		return nil
	},
	)
}

func userShow(username string, withStatus bool) {

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		resp, err := cli.GetUser(context.TODO(), &portal.GetUserRequest{
			Username: username,
			StatusMS: timeoutAsBool(withStatus),
		})
		if err != nil {
			handleError(err)
		}

		if jsonOut {
			fmt.Printf("%s\n", protojson.Format(resp))
			return nil
		}

		if withStatus {
			pviews.WriteTaskForest(resp.Status, getDepth(), true, tw)
			osExit1IfUnsuccessful(resp.Status)

			return nil
		}

		u := resp.User

		fmt.Printf("Username: %s\n", u.Username)
		fmt.Printf("Email: %s\n", u.Email)
		fmt.Printf("Name: %s\n", u.Name)
		fmt.Printf("State: %s\n", u.State)
		fmt.Printf("Access: %s\n", u.AccessMode)
		fmt.Printf("UID/GID: %d/%d\n", u.Uid, u.Gid)
		fmt.Printf("Category: %s\n", u.Category)
		fmt.Printf("Institution: %s\n", u.Institution)
		fmt.Printf("US State: %s\n", u.Usstate)
		fmt.Printf("Country: %s\n", u.Country)

		if u.Admin {
			fmt.Fprintf(tw, "Admin: %t\n", u.Admin)
		}

		if len(u.Projects) == 0 {
			fmt.Printf("Projects: \n")
		} else {
			fmt.Fprintf(tw, "Projects: \n")
			fmt.Fprintf(tw, "\tName\tRole\tState\n")
			fmt.Fprintf(tw, "\t----\t----\t----\n")

			for p, m := range u.Projects {
				fmt.Fprintf(tw, "\t%s\t%s\t%s\n", p, m.Role, m.State)
			}

			tw.Flush()
		}

		if len(u.Experiments) == 0 {
			fmt.Printf("Experiments: \n")
		} else {

			fmt.Fprintf(tw, "Experiments: \n")
			fmt.Fprintf(tw, "\tName\n")
			fmt.Fprintf(tw, "\t----\n")
			for _, e := range u.Experiments {
				fmt.Fprintf(tw, "\t%s\n", e)
			}

			tw.Flush()
		}

		// Only show facilities if they exist. Not many users will have them.
		if len(u.Facilities) > 0 {

			fmt.Fprintf(tw, "Facilities: \n")
			fmt.Fprintf(tw, "\tName\tRole\tState\n")
			fmt.Fprintf(tw, "\t----\t----\t----\n")

			for f, m := range u.Facilities {
				fmt.Fprintf(tw, "\t%s\t%s\t%s\n", f, m.Role, m.State)
			}

			tw.Flush()
		}

		// Only show orgs if they exist.
		if len(u.Organizations) > 0 {

			fmt.Fprintf(tw, "Organizations: \n")
			fmt.Fprintf(tw, "\tName\tRole\tState\n")
			fmt.Fprintf(tw, "\t----\t----\t----\n")

			for o, m := range u.Organizations {
				fmt.Fprintf(tw, "\t%s\t%s\t%s\n", o, m.Role, m.State)
			}

			tw.Flush()
		}
		return nil
	})
}
