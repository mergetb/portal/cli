package clients

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	portal "gitlab.com/mergetb/api/portal/v1/go"
)

func Materialize(endpoint, token string, f func(portal.MaterializeClient) error) error {
	conn, err := grpcConn(endpoint, token)
	if err != nil {
		return status.Error(codes.Unavailable, "materialize client connection")
	}
	defer conn.Close()

	cli := portal.NewMaterializeClient(conn)
	return f(cli)
}
