package clients

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	portal "gitlab.com/mergetb/api/portal/v1/go"
)

func Commission(endpoint, token string, f func(portal.CommissionClient) error) error {
	conn, err := grpcConn(endpoint, token)
	if err != nil {
		return status.Error(codes.Unavailable, "commission client connection")
	}
	defer conn.Close()

	cli := portal.NewCommissionClient(conn)
	return f(cli)
}
