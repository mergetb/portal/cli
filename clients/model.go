package clients

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	portal "gitlab.com/mergetb/api/portal/v1/go"
)

func Model(endpoint, token string, f func(portal.ModelClient) error) error {
	conn, err := grpcConn(endpoint, token)
	if err != nil {
		return status.Error(codes.Unavailable, "model client connection")
	}
	defer conn.Close()

	cli := portal.NewModelClient(conn)
	return f(cli)
}
