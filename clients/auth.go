package clients

import (
	"context"
	"fmt"

	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

// TODO handle auth fail with refresh attempt

type mergeGrpcParams struct {
	token  string
	params map[string]string
}

func (mp *mergeGrpcParams) injectParams(
	ctx context.Context,
	method string,
	req, reply interface{},
	cc *grpc.ClientConn,
	invoker grpc.UnaryInvoker,
	opts ...grpc.CallOption,
) error {

	if mp.token != "" {
		ctx = metadata.AppendToOutgoingContext(ctx, "Authorization", fmt.Sprintf("Bearer %s", mp.token))
	}
	for header, value := range mp.params {
		ctx = metadata.AppendToOutgoingContext(ctx, header, value)
	}

	return invoker(ctx, method, req, reply, cc, opts...)
}
