package clients

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	portal "gitlab.com/mergetb/api/portal/v1/go"
)

func XDC(endpoint, token string, f func(portal.XDCClient) error) error {
	conn, err := grpcConn(endpoint, token)
	if err != nil {
		return status.Error(codes.Unavailable, "xdc client connection")
	}
	defer conn.Close()

	cli := portal.NewXDCClient(conn)
	return f(cli)
}
