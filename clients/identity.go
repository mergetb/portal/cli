package clients

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	portal "gitlab.com/mergetb/api/portal/v1/go"
)

func Identity(endpoint, token string, f func(portal.IdentityClient) error) error {
	conn, err := grpcConn(endpoint, token)
	if err != nil {
		return status.Error(codes.Unavailable, "identity client connection")
	}
	defer conn.Close()

	cli := portal.NewIdentityClient(conn)
	return f(cli)
}
