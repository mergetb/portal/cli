package clients

import (
	"crypto/tls"

	"github.com/grpc-ecosystem/go-grpc-middleware"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	portal "gitlab.com/mergetb/api/portal/v1/go"

	"gitlab.com/mergetb/portal/cli/pkg/sudo"
)

var (
	// GRPCMaxMessageSize is the maximum message size of a grpc message
	// the option is then passed to the grpc client connection, and
	// is set in bytes.
	GRPCMaxMessageSize = 512 * 1024 * 1024
	GRPCMaxMessage     = grpc.WithDefaultCallOptions(
		grpc.MaxCallRecvMsgSize(GRPCMaxMessageSize),
		grpc.MaxCallSendMsgSize(GRPCMaxMessageSize),
	)
	GRPCMaxServer = []grpc.ServerOption{
		grpc.MaxRecvMsgSize(GRPCMaxMessageSize),
		grpc.MaxSendMsgSize(GRPCMaxMessageSize),
	}
)

func grpcConn(endpoint, token string) (*grpc.ClientConn, error) {
	creds := credentials.NewTLS(&tls.Config{
		// XXX XXX XXX
		InsecureSkipVerify: true,
	})

	params := &mergeGrpcParams{
		token:  token,
		params: make(map[string]string),
	}
	if sudo_username := sudo.GetSudoUser(); sudo_username != "" {
		// we're in sudo, only pass headers, if we don't have a token for the user
		if sudo.GetSudoToken() == "" {
			// we're authed as another user; add the header
			params.params[string(portal.GrpcParamSudoUsernameHeader)] = sudo_username
		}
	}

	return grpc.Dial(
		endpoint,
		grpc.WithTransportCredentials(creds),
		grpc.WithUnaryInterceptor(grpc_middleware.ChainUnaryClient(
			params.injectParams,
		)),
		GRPCMaxMessage,
	)
}
