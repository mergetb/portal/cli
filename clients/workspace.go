package clients

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	portal "gitlab.com/mergetb/api/portal/v1/go"
)

func Workspace(endpoint, token string, f func(portal.WorkspaceClient) error) error {
	conn, err := grpcConn(endpoint, token)
	if err != nil {
		return status.Error(codes.Unavailable, "workspace client connection")
	}
	defer conn.Close()

	cli := portal.NewWorkspaceClient(conn)
	return f(cli)
}
