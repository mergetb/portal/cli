package clients

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	portal "gitlab.com/mergetb/api/portal/v1/go"
)

func Communications(endpoint, token string, f func(portal.CommunicationsClient) error) error {
	conn, err := grpcConn(endpoint, token)
	if err != nil {
		return status.Error(codes.Unavailable, "communications client connection")
	}
	defer conn.Close()

	cli := portal.NewCommunicationsClient(conn)
	return f(cli)
}
