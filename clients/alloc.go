package clients

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	portal "gitlab.com/mergetb/api/portal/v1/go"
)

func Alloc(endpoint, token string, f func(portal.AllocClient) error) error {
	conn, err := grpcConn(endpoint, token)
	if err != nil {
		return status.Error(codes.Unavailable, "alloc client connection")
	}
	defer conn.Close()

	cli := portal.NewAllocClient(conn)
	return f(cli)
}
