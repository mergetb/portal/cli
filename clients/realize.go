package clients

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	portal "gitlab.com/mergetb/api/portal/v1/go"
)

func Realize(endpoint, token string, f func(portal.RealizeClient) error) error {
	conn, err := grpcConn(endpoint, token)
	if err != nil {
		return status.Error(codes.Unavailable, "realize client connection")
	}
	defer conn.Close()

	cli := portal.NewRealizeClient(conn)
	return f(cli)
}
