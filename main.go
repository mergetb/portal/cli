package main

// Merge API cli

import (
	"fmt"
	"os"
	"strings"
	"text/tabwriter"

	"github.com/mattn/go-isatty"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	shcli "gitlab.com/mergetb/tech/shared/cli"
)

var (
	version           = ""
	tw                = tabwriter.NewWriter(os.Stdout, 0, 0, 4, ' ', 0)
	server            string
	logLevel          string
	jsonOut           bool
	colorless         bool
	progressless      bool
	depth_override    NullableInt
	syncwait_override NullableDuration
	async_override    bool
	sync_override     bool
)

var root = &cobra.Command{
	Use:     "mrg",
	Short:   "The MergeTB CLI Tool",
	Version: version,
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		lvl, err := log.ParseLevel(logLevel)
		if err != nil {
			log.Fatalf("bad log level: %s", err)
		}
		log.SetLevel(lvl)
	},
}

var list = &cobra.Command{
	Use:   "list",
	Short: "List things",
}

var show = &cobra.Command{
	Use:   "show",
	Short: "Show information about things",
}

var newcmd = &cobra.Command{
	Use:   "new",
	Short: "Make new things",
}

var del = &cobra.Command{
	Use:   "delete",
	Short: "Delete things",
}

var update = &cobra.Command{
	Use:   "update",
	Short: "Update existing things",
}

var harbor = &cobra.Command{
	Use:   "harbor",
	Short: "Harbor management",
}

var xdccmd = &cobra.Command{
	Use:   "xdc",
	Short: "XDC tools",
}

var nodecmd = &cobra.Command{
	Use:   "nodes",
	Short: "Manage experimental nodes",
}

func main() {

	if !isatty.IsTerminal(os.Stdout.Fd()) {
		colorless = true
		progressless = true
	}

	log.SetFormatter(&log.TextFormatter{
		DisableTimestamp: true,
	})

	cobra.EnablePrefixMatching = true

	root.PersistentFlags().StringVarP(&server, "server", "s", "", "MergeTB API server address")
	mrgcfg.BindPFlag("server", root.PersistentFlags().Lookup("server"))
	root.PersistentFlags().BoolVarP(&jsonOut, "json", "j", false, "Output JSON")

	root.PersistentFlags().StringVarP(
		&logLevel, "loglevel", "l", "info", "Level to log at. One of "+strings.Join(logLevels(), ", "),
	)

	root.PersistentFlags().BoolVarP(&colorless, "colorless", "c", false, "Disable color output")
	root.PersistentFlags().BoolVar(&progressless, "disable-progress", false, "Disable progress bars")
	root.PersistentFlags().Var(&depth_override, "status-depth", "Override the depth of status display")
	root.PersistentFlags().Var(&syncwait_override, "syncwait", "Makes cli usage synchronous and overrides the command wait to be the specified duration")
	root.PersistentFlags().BoolVar(&sync_override, "sync", false, fmt.Sprintf("Makes cli usage synchronous (if your merge configuration is already synchronous, this flag does not do anything, otherwise sets command wait to the default value of %s)", DEFAULT_WAIT_CONFIG))
	root.PersistentFlags().BoolVar(&async_override, "async", false, "Makes cli usage asynchronous (by overriding command wait to 0)")
	root.MarkFlagsMutuallyExclusive("syncwait", "sync", "async")

	configure()
	identity()
	user()
	project()
	organization()
	experiment()
	facility()
	realization()
	materialization()
	cred()
	xdc()
	member()
	model()
	keys()
	pools()
	vpn()
	nodes()
	update_binary()
	activate()
	ingress()
	sshXdc()
	portalconfig()
	communications()

	root.AddCommand(list)
	root.AddCommand(update)
	root.AddCommand(newcmd)
	root.AddCommand(del)
	root.AddCommand(show)
	root.AddCommand(harbor)
	root.AddCommand(xdccmd)
	root.AddCommand(nodecmd)

	cobra.OnInitialize(func() {
		if colorless {
			shcli.DisableColor()
		}
	})

	_, err := root.ExecuteC()
	if err != nil {
		os.Exit(1)
	}

	os.Exit(0)
}

func logLevels() []string {

	r := []string{}
	for _, l := range log.AllLevels {
		r = append(r, l.String())
	}

	return r
}
