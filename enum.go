package main

import (
	"fmt"
	flag "github.com/spf13/pflag"
	"strings"
)

// Taken from https://github.com/sgreben/flagvar/blob/v1.10.1/enum.go (It's public domain).

// FlagEnum is a `flag.Value` for one-of-a-fixed-set string arguments.
// The value of the `Choices` field defines the valid choices.
// If `CaseSensitive` is set to `true` (default `false`), the comparison is case-sensitive.
type FlagEnum struct {
	Choices       []string
	CaseSensitive bool

	Value string
	Text  string
}

// Help returns a string suitable for inclusion in a flag help message.
func (fe *FlagEnum) Help() string {
	if fe.CaseSensitive {
		return fmt.Sprintf("one of [%v] (case-sensitive)", strings.Join(fe.Choices, ", "))
	}
	return fmt.Sprintf("one of [%v]", strings.Join(fe.Choices, ", "))
}

// Set is flag.Value.Set
func (fe *FlagEnum) Set(v string) error {
	fe.Text = v
	equal := strings.EqualFold
	if fe.CaseSensitive {
		equal = func(a, b string) bool { return a == b }
	}
	for _, c := range fe.Choices {
		if equal(c, v) {
			fe.Value = c
			return nil
		}
	}
	return fmt.Errorf(`"%s" must be one of [%s]`, v, strings.Join(fe.Choices, ", "))
}

func (fe *FlagEnum) String() string {
	return fe.Value
}

func (fe *FlagEnum) Type() string { return "enum" }

// Add the enum flag to the given FlagSet.
// THis is a little awkward in Cobra. I would think there would be a way to ingrate custom
// flags into the Cobra pflags. I didn't see it though.
func (fe *FlagEnum) AddToFlagSet(fs *flag.FlagSet, name, usage string) {
	fs.Var(fe, name, usage)
}

func (fe *FlagEnum) AddToFlagSetShort(fs *flag.FlagSet, name, short, usage string) {
	fs.VarP(fe, name, short, usage)
}

//
// Here is an example of how to use this to create a set of strings as a flag choice.
//
// 	choice := &FlagEnum{Choices: []string{"one", "two", "three"}}
// 	doThing := &cobra.Command{
// 		Use:   "cmd",
// 		Short: "Do a thing",
// 		Args:  cobra.NoArgs(),
// 		Run: func(cmd *cobra.Command, args []string) {
// 			doThing()
// 		},
// 	}
// 	choice.AddToFlagSet(
// 		doThing.Flags(),
// 		"merge",
// 		choice.Choices[0],
// 		"How to do a thing: "+choice.Help()+". Default: "+choice.Choices[0],
// 	)
// 	root.AddCommand(doThing)
//
