package main

import (
	"bufio"
	"bytes"
	"context"
	"encoding/base64"
	"fmt"
	"io"
	"net"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"syscall"

	"github.com/povsister/scp"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/knownhosts"
	terminal "golang.org/x/term"

	"gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/cli/clients"
	mrgssh "gitlab.com/mergetb/portal/cli/ssh"
)

func sshXdc() {
	jump_endpoint := ""
	node := ""
	xdc := ""
	prj := ""
	port_forwards := new(forwards)
	recursive := false
	relaxStrictHostCheck := false
	key := currentSshPrivkeyPath()

	sshCmd := &cobra.Command{
		Use:     "ssh [<xdc>.<project>] [<node>]",
		Example: "# SSH to XDC x0.murphy\nmrg xdc ssh x0.murphy\n\n# SSH to node h0 via XDC x0.murphy\nmrg xdc ssh -x x0.murphy h0\n\n# SSH to node h0 via XDC x0.murphy and forward TCP connections to localhost:4443 to h0:443\nmrg xdc ssh -x x0.murphy -L 4443:443 h0",
		Short:   "SSH to an XDC, or to an experiment node via an XDC",
		Long:    "SSH to an XDC, or to an experiment node via an XDC.\n\nNote: this program implements a simple SSH client. It is not a wrapper around your system's SSH program, and many options you may be familiar with are not implemented. If you need advanced functionality, use your system's SSH program directly instead of this command. Instructions for setting up your local machine's SSH configuration for compatibility with Merge can be found at https://mergetb.org/docs/experimentation/xdc/",
		Args:    cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			// if -x is provided, the XDC is used as a 2nd jump host, with the exp node being the
			// ultimate destination
			if xdc != "" {
				node = args[0]
				xdc, prj = parseXdcid(xdc)
			} else {
				xdc, prj = parseXdcid(args[0])
			}

			// validate jump_endpoint
			if jump_endpoint != "" {
				err := validateEndpoint(jump_endpoint)
				if err != nil {
					log.Fatalf("cannot parse jump endpoint from %s: %v", jump_endpoint, err)
				}
			}

			doSSH(key, jump_endpoint, xdc, prj, node, port_forwards, relaxStrictHostCheck)
		},
	}
	sshCmd.Flags().StringVarP(&jump_endpoint, "jump", "J", "", "Manually specified Merge portal jump endpoint to connect to\n  - Format: <fqdn>:<port>")
	sshCmd.Flags().StringVarP(&key, "identity_file", "i", key, "Private key to authenticate with")
	sshCmd.Flags().StringVarP(&xdc, "xdc", "x", "", "Use XDC as a jump host to reach an experiment node\n  - Format: <xdc>.<project>\n  - Note: The XDC must be attached to a materialization")
	sshCmd.Flags().VarP(port_forwards, "local_forward", "L", "Forward TCP connections from your machine to a remote host\n  - Format: <local_port>[:<remote_host>]:<remote_port>")
	sshCmd.Flags().BoolVarP(&relaxStrictHostCheck, "no-strict-host-check", "H", false, "Relax strict host certificate checking")
	xdccmd.AddCommand(sshCmd)

	scpCmd := &cobra.Command{
		Use:   "scp <source> <destination>",
		Short: "Copy data to/from an XDC, or to/from an experiment node via an XDC",
		Long:  "Copy data to/from an XDC, or to/from an experiment node via an XDC.\n\nNote: this program implements file transfers over SSH using a simple SSH client. It is not a wrapper around your system's SSH client, and many options you may be familiar with are not implemented. If you need advanced functionality, use your system's SSH client directly instead of this command. Instructions for setting up your local machine's SSH configuration for compatibility with Merge can be found at https://mergetb.org/docs/experimentation/xdc/",
	}
	xdccmd.AddCommand(scpCmd)

	scpUploadCmd := &cobra.Command{
		Use:     "upload <local source> <remote node>:[<remote path>]",
		Example: "# Upload a file to XDC x0.murphy\nmrg xdc scp upload test.txt x0.murphy:\n\n# Upload a file to node h0 via XDC x0.murphy\nmrg xdc scp upload -x x0.murphy test.txt h0:\n\n# Recursively upload an entire directory to node h0:/tmp/data via XDC x0.murphy:\nmrg xdc scp upload -x x0.murphy -r data h0:/tmp/data",
		Short:   "Upload data to an XDC, or to an experiment node via an XDC",
		Long:    "Upload data to an XDC, or to an experiment node via an XDC.\n\nNote: this program implements file transfers over SSH using a simple SSH client. It is not a wrapper around your system's SCP program, and many options you may be familiar with are not implemented. If you need advanced functionality, use your system's SCP program directly instead of this command. Instructions for setting up your local machine's SSH configuration for compatibility with Merge can be found at https://mergetb.org/docs/experimentation/xdc/",
		Args:    cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			source, dest, xdc, prj, node := parseSCPUploadArgs(args[0], args[1], xdc)

			// validate jump_endpoint
			if jump_endpoint != "" {
				err := validateEndpoint(jump_endpoint)
				if err != nil {
					log.Fatalf("cannot parse jump endpoint from %s: %v", jump_endpoint, err)
				}
			}

			doSCP(key, jump_endpoint, xdc, prj, node, source, dest, recursive, true, relaxStrictHostCheck)
		},
	}
	scpUploadCmd.Flags().StringVarP(&jump_endpoint, "jump", "J", "", "Manually specified Merge portal jump endpoint to connect to\n  - Format: <fqdn>:<port>")
	scpUploadCmd.Flags().StringVarP(&key, "identity_file", "i", key, "Private key to authenticate with")
	scpUploadCmd.Flags().StringVarP(&xdc, "xdc", "x", "", "Use XDC as a jump host to reach an experiment node\n  - Format: <xdc>.<project>\n  - Note: The XDC must be attached to a materialization")
	scpUploadCmd.Flags().BoolVarP(&recursive, "recursive", "r", false, "Recursively upload entire directory")
	scpCmd.Flags().BoolVarP(&relaxStrictHostCheck, "no-strict-host-check", "H", false, "Relax strict host certificate checking")
	scpCmd.AddCommand(scpUploadCmd)

	scpDownloadCmd := &cobra.Command{
		Use:     "download <remote node>:<remote path> <local path>]",
		Example: "# Download a file from XDC x0.murphy\nmrg xdc scp download x0.murphy:test.txt .\n\n# Download a file from node h0 via XDC x0.murphy\nmrg xdc scp download -x x0.murphy h0:test.txt .\n\n# Recursively download an entire directory from node h0:/tmp/data via XDC x0.murphy:\nmrg xdc scp download -x x0.murphy -r h0:/tmp/data .",
		Short:   "Download data from an XDC, or from an experiment node via an XDC",
		Long:    "Download data from an XDC, or from an experiment node via an XDC.\n\nNote: this program implements file transfers over SSH using a simple SSH client. It is not a wrapper around your system's SCP program, and many options you may be familiar with are not implemented. If you need advanced functionality, use your system's SCP program directly instead of this command. Instructions for setting up your local machine's SSH configuration for compatibility with Merge can be found at https://mergetb.org/docs/experimentation/xdc/",
		Args:    cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			source, dest, xdc, prj, node := parseSCPDownloadArgs(args[0], args[1], xdc)

			// validate jump_endpoint
			if jump_endpoint != "" {
				err := validateEndpoint(jump_endpoint)
				if err != nil {
					log.Fatalf("cannot parse jump endpoint from %s: %v", jump_endpoint, err)
				}
			}

			doSCP(key, jump_endpoint, xdc, prj, node, source, dest, recursive, false, relaxStrictHostCheck)
		},
	}
	scpDownloadCmd.Flags().StringVarP(&jump_endpoint, "jump", "J", "", "Manually specified Merge portal jump endpoint to connect to\n  - Format: <fqdn>:<port>")
	scpDownloadCmd.Flags().StringVarP(&key, "identity_file", "i", key, "Private key to authenticate with")
	scpDownloadCmd.Flags().StringVarP(&xdc, "xdc", "x", "", "Use XDC as a jump host to reach an experiment node\n  - Format: <xdc>.<project>\n  - Note: The XDC must be attached to a materialization")
	scpDownloadCmd.Flags().BoolVarP(&recursive, "recursive", "r", false, "Recursively download entire directory")
	scpDownloadCmd.Flags().BoolVarP(&relaxStrictHostCheck, "no-strict-host-check", "H", false, "Relax strict host certificate checking")
	scpCmd.AddCommand(scpDownloadCmd)
}

func validateEndpoint(endpoint string) error {
	_, _, err := net.SplitHostPort(endpoint)
	return err
}

func parseSCPUploadArgs(source, dest, xdc string) (string, string, string, string, string) {
	prj := ""
	node := ""

	// parse dest
	dtokens := strings.Split(dest, ":")
	if len(dtokens) != 2 {
		log.Fatalf("destination '%s' does not conform to <remote node>:[<remote path>]", dest)
	}

	if xdc == "" {
		// no xdc provided, so we must be able to parse an xdc from the first token
		xdc, prj = parseXdcid(dtokens[0])
	} else {
		// xdc provided; assume first token is a node
		node = dtokens[0]
		xdc, prj = parseXdcid(xdc)
	}

	dest = dtokens[1]

	// if no dest, put in home directory
	if dest == "" {
		dest = "."
	}

	return source, dest, xdc, prj, node
}

func parseSCPDownloadArgs(source, dest, xdc string) (string, string, string, string, string) {
	prj := ""
	node := ""

	// parse source
	stokens := strings.Split(source, ":")
	if len(stokens) != 2 {
		log.Fatalf("source '%s' does not conform to <remote node>:<remote path>", dest)
	}

	if xdc == "" {
		// no xdc provided, so we must be able to parse an xdc from the first token
		xdc, prj = parseXdcid(stokens[0])
	} else {
		// xdc provided; assume first token is a node
		node = stokens[0]
		xdc, prj = parseXdcid(xdc)
	}

	source = stokens[1]

	return source, dest, xdc, prj, node
}

func closeIfError(cli *ssh.Client, err error) {
	if err != nil {
		cli.Close()
	}
}

func establishSSHConnections(keypath, jumphost, xdc, project, node string, relaxStrictHostCheck bool) ([]*ssh.Client, net.Conn, error) {

	var err error

	if jumphost == "" {
		jumps, err := getJumpHosts()
		if err != nil {
			log.Warn("unable to query jump hosts from portal; defaulting to portal GRPC endpoint")
		} else {
			log.Tracef("Merge API returned jump hosts %+v", jumps)
			// use first jump host
			jumphost = fmt.Sprintf("%s:%d", jumps[0].Fqdn, jumps[0].Port)
			log.Debugf("using jump host %s", jumphost)
		}
	}

	// append port to xdd fqdn to get the hostname
	xdcHostname := fmt.Sprintf("%s-%s:22", xdc, project)

	nodeHostname := ""
	if node != "" {
		nodeHostname = fmt.Sprintf("%s:22", node)
	}

	// get logged in username
	username := getLoggedInUsername()

	// load the private key
	key, err := os.ReadFile(keypath)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to read private key: %v", err)
	}

	// create the signer
	signer, err := ssh.ParsePrivateKey(key)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to parse private key: %v", err)
	}

	ca_keys, err := loadKnownCerts()
	if err != nil {
		return nil, nil, fmt.Errorf("error loading known_hosts file: %v", err)
	}
	if len(ca_keys) == 0 {
		log.Warn("No CA keys in known_hosts file, try mrg logout/mrg login to fix this")
	}

	cc := &ssh.CertChecker{
		IsHostAuthority: func(caKey ssh.PublicKey, address string) bool {
			// check if matches any CAs we have on file
			host_ca_key := caKey.Marshal()
			for _, ca_key := range ca_keys {
				if bytes.Equal(host_ca_key, ca_key.Marshal()) {
					return true
				}
			}
			if !relaxStrictHostCheck {
				log.Errorf("None of the known_hosts ca certificates matched for host %s, ssh connection failed", address)
				return false
			}

			// falling back to the old method
			log.Warnf("None of the known_hosts ca certificates matched for host %s, this is insecure", address)

			isAuth := address == xdcHostname || address == jumphost ||
				(nodeHostname != "" && address == nodeHostname)
			log.WithFields(log.Fields{
				"address":      address,
				"xdcHostname":  xdcHostname,
				"nodeHostname": nodeHostname,
				"jumphost":     jumphost,
			}).Debugf("IsHostAuthority: isAuth=%v", isAuth)
			return isAuth
		},
		HostKeyFallback: ssh.HostKeyCallback(func(host string, remote net.Addr, pubKey ssh.PublicKey) error {
			// XDCs and exp nodes get re-created frequently. Rather than store host keys for them,
			// which will need to be over-written on every recreation, just don't store them at all.
			// This is not a security issue, as we've already validated the jump host key if we get
			// to this point
			log.Debugf("non-certificate hostkey provided by %s; verifying against knownhosts", host)

			if host == xdcHostname {
				log.Debugf("skipping host key check for XDC")
				return nil
			} else if host == nodeHostname {
				log.Debugf("skipping host key check for experiment node")
				return nil
			} else if host != jumphost {
				return fmt.Errorf("request to check key for unknown host: %s", host)
			}

			log.Debugf("performing host key check for host %s", host)
			kh, err := mrgssh.CheckKnownHost(host, remote, pubKey, "")

			if err != nil {
				if kh {
					ke := err.(*knownhosts.KeyError)
					return fmt.Errorf("A host key for %s was found on line %d of file %s, but it does not match that offered by %s. This could be a man-in-the-middle attack. If you want to accept the new key, delete old keys matching %s from your known_hosts file and run this command again", host, ke.Want[0].Line, ke.Want[0].Filename, host, host)
				}

				// malformed known_hosts?
				if relaxStrictHostCheck {
					log.Warnf("could not verify host key for host %s - error checking known_hosts database: %s", host, err.Error())
					return nil
				}
				return fmt.Errorf("error checking known_hosts database: %v\n", err)
			}

			if kh {
				// found key and matches that which was offered; accept
				log.Debugf("found host key for host: %s", host)
				return nil
			}
			// do not ask user to confirm the key and do not add it in the relaxStrictHostCheck mode
			if relaxStrictHostCheck {
				log.Warnf("could not verify host key for host %s - host key is not in ~/.ssh/known_hosts", host)
				return nil
			}

			// no key found; ask for confirmation to accept the offered key now
			serialized := pubKey.Type() + " " + base64.StdEncoding.EncodeToString(pubKey.Marshal())
			fmt.Printf("server %s provided host key:\n%s\n", host, serialized)
			accept := askForConfirmation("Do you accept this key?")
			if !accept {
				return fmt.Errorf("host key rejected")
			}

			log.Debugf("adding host key for host %s", host)
			err = mrgssh.AddKnownHost(host, remote, pubKey, "")
			if err != nil {
				return fmt.Errorf("failed to add known hosts entry: %v", err)
			}

			return nil
		}),
	}

	config := &ssh.ClientConfig{
		User: username,
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(signer),
		},
		HostKeyCallback: cc.CheckHostKey,
	}

	// dial a connection to the jump host
	jClient, err := ssh.Dial("tcp", jumphost, config)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to connect to jump host: %v", err)
	}
	defer closeIfError(jClient, err)

	log.Debugf("connected to jump host: %s", jumphost)

	// dial a connection to the XDC through the jump host
	log.Debugf("dialing XDC: %s", xdcHostname)
	conn, err := jClient.Dial("tcp", xdcHostname)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to connect to XDC: %v", err)
	}

	ncc, chans, reqs, err := ssh.NewClientConn(conn, xdcHostname, config)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to establish connection to XDC: %v", err)
	}

	xClient := ssh.NewClient(ncc, chans, reqs)
	defer closeIfError(xClient, err)

	log.Debugf("connected to XDC: %s", xdcHostname)

	if node == "" {
		return []*ssh.Client{xClient, jClient}, conn, nil
	}

	// if requested, dial a connection through the XDC to the exp node
	log.Debugf("dialing exp node:%s", nodeHostname)
	nconn, err := xClient.Dial("tcp", nodeHostname)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to connect to exp node: %v", err)
	}

	ncc, chans, reqs, err = ssh.NewClientConn(nconn, nodeHostname, config)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to establish connection to exp node: %v", err)
	}

	nClient := ssh.NewClient(ncc, chans, reqs)
	defer closeIfError(nClient, err)

	log.Debugf("connected to exp node: %s", nodeHostname)

	return []*ssh.Client{nClient, xClient, jClient}, nconn, nil
}

func doSCP(keypath, jumphost, xdc, project, node, src, dest string, recursive, upload bool, relaxStrictHostCheck bool) {
	var err error

	// validate that this XDC exists in this project
	xdcinfo, err := getXDC(project, xdc)
	if err != nil {
		log.Fatalf("get XDCs: %v", err)
	}
	log.Tracef("Merge API returned XDCInfo %+v", xdcinfo)

	// if connectivity to an exp node is requested, validate connectivity here
	if node != "" {
		err = validateNodeConnectivity(xdcinfo, node)
		if err != nil {
			log.Fatal(err)
		}
	}

	// clients contains the set of SSH clients created to reach the terminal node, including jumps
	clients, _, err := establishSSHConnections(keypath, jumphost, xdc, project, node, relaxStrictHostCheck)
	if err != nil {
		log.Fatal(err)
	}

	// after we're done, close connections
	defer func(clis []*ssh.Client) {
		for _, c := range clis {
			log.Tracef("closing connection %+v", c)
			c.Close()
		}
	}(clients)

	// tClient -> terminal client
	tClient := clients[0]

	scpClient, err := scp.NewClientFromExistingSSH(tClient, &scp.ClientOption{})
	if err != nil {
		log.Fatalf("create SCP client: %v", err)
	}

	if upload {
		if node != "" {
			log.Infof("uploading %s to %s:%s via %s.%s ...", src, node, dest, xdc, project)
		} else {
			log.Infof("uploading %s to %s.%s:%s ...", src, xdc, project, dest)
		}

		if recursive {
			err = scpClient.CopyDirToRemote(src, dest, &scp.DirTransferOption{})
			if err != nil {
				log.Fatalf("copy dir to remote: %v", err)
			}
		} else {
			err = scpClient.CopyFileToRemote(src, dest, &scp.FileTransferOption{})
			if err != nil {
				log.Fatalf("copy file to remote: %v", err)
			}
		}
	} else {
		if recursive {
			// append basename of the source dir to the local dir, to match scp -r behavior
			dest = dest + "/" + filepath.Base(src)
		}

		if node != "" {
			log.Infof("downloading %s:%s to %s via %s.%s ...", node, src, dest, xdc, project)
		} else {
			log.Infof("downloading %s.%s:%s to %s ...", xdc, project, src, dest)
		}

		if recursive {
			// create directory locally, if it does not exist
			err = os.MkdirAll(dest, os.ModePerm)
			if err != nil {
				log.Fatalf("mkdir %s: %v", dest, err)
			}

			err = scpClient.CopyDirFromRemote(src, dest, &scp.DirTransferOption{})
			if err != nil {
				log.Fatalf("copy dir from remote: %v", err)
			}
		} else {
			err = scpClient.CopyFileFromRemote(src, dest, &scp.FileTransferOption{})
			if err != nil {
				log.Fatalf("copy file from remote: %v", err)
			}
		}
	}
}

func doSSH(keypath, jumphost, xdc, project, node string, port_forwards *forwards, relaxStrictHostCheck bool) {
	var err error

	// validate that this XDC exists in this project
	xdcinfo, err := getXDC(project, xdc)
	if err != nil {
		log.Fatalf("get XDCs: %v", err)
	}
	log.Tracef("Merge API returned XDCInfo %+v", xdcinfo)

	// if connectivity to an exp node is requested, validate connectivity here
	if node != "" {
		err = validateNodeConnectivity(xdcinfo, node)
		if err != nil {
			log.Fatal(err)
		}
	}

	// clients contains the set of SSH clients created to reach the terminal node, including jumps
	clients, conn, err := establishSSHConnections(keypath, jumphost, xdc, project, node, relaxStrictHostCheck)
	if err != nil {
		log.Fatal(err)
	}

	// tClient -> terminal client
	tClient := clients[0]

	// after we're done, close connections
	defer func(clis []*ssh.Client) {
		for _, c := range clis {
			log.Tracef("closing connection %+v", c)
			c.Close()
		}
	}(clients)

	// create port forwarders in separate goroutines
	for _, fwd := range port_forwards.fwds {
		go func(f forward) {
			log.Debugf("processing fwd %+v", f)
			for {
				localok, err := f.run(tClient)
				if err != nil {
					log.Error(err)
				}
				if !localok {
					// stop if we aren't even able to listen on the local port
					break
				}
			}
			log.Debugf("done processing fwd %+v", f)
		}(*fwd)
	}

	// create a new session on the XDC
	session, err := tClient.NewSession()
	if err != nil {
		log.Fatalf("cannot open new session: %v", err)
	}
	defer session.Close()

	fd := int(os.Stdin.Fd())
	state, err := terminal.MakeRaw(fd)
	if err != nil {
		log.Fatalf("terminal make raw: %s", err)
	}
	defer terminal.Restore(fd, state)

	// run the SSH session in the current terminal through a goroutine
	sig := make(chan os.Signal, 1)

	sigs := []os.Signal{syscall.SIGTERM, syscall.SIGINT}
	sigwinch := mrgssh.WindowResizeSignal()
	if sigwinch != nil {
		sigs = append(sigs, sigwinch)
	}

	signal.Notify(sig, sigs...)
	ctx, cancel := context.WithCancel(context.Background())

	go func() {
		if err := runSession(session, ctx, conn); err != nil {
			log.Error(err)
		}
		cancel()
	}()

	// wait for the session to end or a signal to be raised
	for {
		quit := true
		select {
		case s := <-sig:
			if sigwinch != nil && s == sigwinch {
				// window resize
				fd := int(os.Stdout.Fd())
				w, h, _ := terminal.GetSize(fd)
				session.WindowChange(h, w)
				quit = false
			} else if s == syscall.SIGTERM || s == syscall.SIGINT {
				cancel()
			}
		case <-ctx.Done():
		}

		if quit {
			break
		}
	}
}

func runSession(session *ssh.Session, ctx context.Context, conn net.Conn) error {
	if runtime.GOOS == "windows" {
		log.Warning("on windows, various keyboard keys do not work normally")
	}

	go func() {
		<-ctx.Done()
		conn.Close()
	}()

	w, h, err := terminal.GetSize(int(os.Stdout.Fd()))
	if err != nil {
		return fmt.Errorf("terminal get size: %s", err)
	}

	modes := ssh.TerminalModes{
		ssh.ECHO:          1,
		ssh.TTY_OP_ISPEED: 14400,
		ssh.TTY_OP_OSPEED: 14400,
	}

	term := os.Getenv("TERM")
	if term == "" {
		term = "xterm-256color"
	}

	if err := session.RequestPty(term, h, w, modes); err != nil {
		return fmt.Errorf("session xterm: %s", err)
	}

	session.Stdout = os.Stdout
	session.Stderr = os.Stderr
	session.Stdin = os.Stdin

	if err := session.Shell(); err != nil {
		log.Fatalf("session shell: %s", err)
	}

	if err := session.Wait(); err != nil {
		if e, ok := err.(*ssh.ExitError); ok {
			switch e.ExitStatus() {
			case 130:
				return nil
			}
		}
		log.Debugf("ssh: %s", err)
	}

	return nil
}

func askForConfirmation(s string) bool {
	reader := bufio.NewReader(os.Stdin)

	for {
		fmt.Printf("%s [y/n]: ", s)

		response, err := reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}

		response = strings.ToLower(strings.TrimSpace(response))

		if response == "y" || response == "yes" {
			return true
		} else if response == "n" || response == "no" {
			return false
		}
	}
}

func runTunnel(local, remote net.Conn) {
	defer local.Close()
	defer remote.Close()
	done := make(chan struct{}, 2)

	go func() {
		io.Copy(local, remote)
		done <- struct{}{}
	}()

	go func() {
		io.Copy(remote, local)
		done <- struct{}{}
	}()

	<-done
}

type forward struct {
	local_port  int
	remote_host string
	remote_port int
}

func (f *forward) parse(value string) error {
	var err error

	tokens := strings.Split(value, ":")

	switch len(tokens) {
	case 2:
		// assume no remote_host
		f.local_port, err = strconv.Atoi(tokens[0])
		if err != nil {
			return fmt.Errorf("could not convert %s to int: %v", tokens[0], err)
		}

		f.remote_port, err = strconv.Atoi(tokens[1])
		if err != nil {
			return fmt.Errorf("could not convert %s to int: %v", tokens[1], err)
		}

		f.remote_host = ""
		return nil
	case 3:
		f.local_port, err = strconv.Atoi(tokens[0])
		if err != nil {
			return fmt.Errorf("could not convert %s to int: %v", tokens[0], err)
		}

		f.remote_port, err = strconv.Atoi(tokens[2])
		if err != nil {
			return fmt.Errorf("could not convert %s to int: %v", tokens[2], err)
		}

		f.remote_host = tokens[1]
		return nil
	default:
		return fmt.Errorf("%s does not conform to forwarding format: '<local port>[:<remote host>]:<remote port>'", value)
	}
}

func (f *forward) run(client *ssh.Client) (bool, error) {
	// listen on local port
	listener, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", f.local_port))
	if err != nil {
		return false, err
	}
	defer listener.Close()

	log.Debugf("listening for connections on localhost:%d", f.local_port)

	for {
		// connect to remote port
		local, err := listener.Accept()
		if err != nil {
			return false, err
		}

		remote_host := f.remote_host
		if remote_host == "" {
			remote_host = "localhost" // localhost -> XDC:localhost
		}

		log.Debugf("dialing remote host %s:%d", remote_host, f.remote_port)
		remote, err := client.Dial("tcp", fmt.Sprintf("%s:%d", remote_host, f.remote_port))
		if err != nil {
			return true, fmt.Errorf("error dialing remote host %s:%d: %v", remote_host, f.remote_port, err)
		}

		log.Debugf("established tunnel with %s", local.LocalAddr())

		// run tunnel in separate thread, so that we can listen for more connection requests
		// in the main thread
		go func(l, r net.Conn) {
			runTunnel(l, r)
			log.Debugf("tunnel terminated")
		}(local, remote)
	}

	// not reached
}

type forwards struct {
	fwds []*forward
}

func (f *forwards) String() string {
	var s string
	for _, fwd := range f.fwds {
		s = s + fmt.Sprintf("%v\n", fwd)
	}
	return s
}

func (f *forwards) Set(value string) error {
	fwd := &forward{}
	err := fwd.parse(value)
	if err != nil {
		return err
	}

	f.fwds = append(f.fwds, fwd)
	return nil
}

func (f *forwards) Type() string {
	return ""
}

func getJumpHosts() ([]*portal.SSHJump, error) {

	var jumps []*portal.SSHJump

	err := clients.XDC(endpoint(), token(), func(cli portal.XDCClient) error {
		resp, err := cli.GetXDCJumpHosts(
			context.TODO(),
			&portal.GetXDCJumpHostsRequest{},
		)
		if err != nil {
			handleError(err)
		}

		jumps = resp.JumpHosts
		return nil
	})

	return jumps, err
}

func getXDC(project, xdc string) (*portal.XDCInfo, error) {

	var xdcs []*portal.XDCInfo

	err := clients.XDC(endpoint(), token(), func(cli portal.XDCClient) error {
		resp, err := cli.ListXDCs(
			context.TODO(),
			&portal.ListXDCsRequest{
				Project: project,
			},
		)
		if err != nil {
			handleError(err)
		}

		xdcs = resp.XDCs
		return nil
	})

	if err != nil {
		return nil, err
	}

	// fully qualified xdc name is of the form <xdc>.<project>
	fqxn := fmt.Sprintf("%s.%s", xdc, project)

	for _, x := range xdcs {
		if x.Name == fqxn {
			return x, nil
		}
	}

	return nil, fmt.Errorf("xdc '%s' does not exist", fqxn)
}

// The user requested SSH connectivity to 'node', vectored through XDC 'xdcinfo'
// Validate that the XDC is connected to an experiment, and that that experiment
// has a node named 'node'
func validateNodeConnectivity(xdcinfo *portal.XDCInfo, node string) error {
	var mtz *portal.Materialization

	if xdcinfo.Materialization == "" {
		return fmt.Errorf("XDC '%s' is not attached to a materialization", xdcinfo.Name)
	}

	rlz, exp, proj := parseRzid(xdcinfo.Materialization)

	err := clients.Materialize(endpoint(), token(), func(cli portal.MaterializeClient) error {
		resp, err := cli.GetMaterialization(
			context.TODO(),
			&portal.GetMaterializationRequest{
				Project:     proj,
				Experiment:  exp,
				Realization: rlz,
			},
		)
		if err != nil {
			handleError(err)
		}

		mtz = resp.Materialization
		return nil
	})

	if err != nil {
		return err
	}

	// search for node in the metal nodes
	for _, n := range mtz.Metal {
		if strings.EqualFold(n.Model.Id, node) {
			return nil
		}
	}

	// search for node in the VMs
	for _, v := range mtz.Vms {
		if strings.EqualFold(v.VmAlloc.Node, node) {
			return nil
		}
	}

	return fmt.Errorf("XDC '%s' is attached to materialization '%s' which does not have a node named '%s'",
		xdcinfo.Name,
		xdcinfo.Materialization,
		node,
	)
}

// loadKnownCerts load all cert keys, from the known_hosts file
func loadKnownCerts() ([]ssh.PublicKey, error) {
	certkeys := []ssh.PublicKey{}
	kh, err := mrgssh.DefaultKnownHostsPath()
	if err != nil {
		return nil, err
	}

	kh_content, err := os.ReadFile(kh)
	if err != nil && !os.IsNotExist(err) {
		return nil, fmt.Errorf("unable to read cert key: %v", err)
	}

	errors := false
	for len(kh_content) > 0 {
		marker, hosts, caKey, _, rest, err := ssh.ParseKnownHosts(kh_content)
		if err != nil {
			errors = true
		} else {
			if marker == "cert-authority" && len(hosts) == 1 && hosts[0] == "*" {
				certkeys = append(certkeys, caKey)
			}
		}
		kh_content = rest
	}
	if errors {
		log.Warn("loadKnownCerts: parsing .ssh/known_hosts detected errors")
	}
	return certkeys, nil
}
