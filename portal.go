package main

import (
	"context"
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/cli/clients"
	shcli "gitlab.com/mergetb/tech/shared/cli"
	"google.golang.org/protobuf/encoding/protojson"
)

func portalconfig() {

	// portal user configuiration.
	portalConfListCmds := &cobra.Command{
		Use:   "portal",
		Short: "List portal configurations",
	}

	listUserUSStates := &cobra.Command{
		Use:   "usstates",
		Short: "List US states from which users may come",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			listUserUSStates()
		},
	}
	portalConfListCmds.AddCommand(listUserUSStates)

	listCountries := &cobra.Command{
		Use:   "countries",
		Short: "List user countries",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			listCountries()
		},
	}
	portalConfListCmds.AddCommand(listCountries)

	listInsts := &cobra.Command{
		Use:   "institutions",
		Short: "List user institutions",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			listInsts()
		},
	}
	portalConfListCmds.AddCommand(listInsts)

	listCats := &cobra.Command{
		Use:   "user-categories",
		Short: "List user categories",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			listCats()
		},
	}
	portalConfListCmds.AddCommand(listCats)

	listETs := &cobra.Command{
		Use:   "entity-types",
		Short: "List types of projects/organizations",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			listETs()
		},
	}
	portalConfListCmds.AddCommand(listETs)

	list.AddCommand(portalConfListCmds)

	portalConfNewCmds := &cobra.Command{
		Use:   "portal",
		Short: "New portal configuration",
	}

	newEntityTypes := &cobra.Command{
		Use:   "entity-type",
		Short: "Add new project/organization types and optional subtypes",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			addEntityTypes(args[0], args[1:])
		},
	}
	portalConfNewCmds.AddCommand(newEntityTypes)

	newInsts := &cobra.Command{
		Use:   "institutions",
		Short: "Add new institutions",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			addInsts(args)
		},
	}
	portalConfNewCmds.AddCommand(newInsts)

	newCats := &cobra.Command{
		Use:   "user-categories",
		Short: "Add new user categories",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			addCats(args)
		},
	}
	portalConfNewCmds.AddCommand(newCats)

	newcmd.AddCommand(portalConfNewCmds)

	portalConfDeleteCmds := &cobra.Command{
		Use:   "portal",
		Short: "Delete portal configuration",
	}

	deleteEntityTypes := &cobra.Command{
		Use:   "entity-type",
		Short: "Delete entity types",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			deleteEntityTypes(args[0], args[1:])
		},
	}
	portalConfDeleteCmds.AddCommand(deleteEntityTypes)

	deleteInsts := &cobra.Command{
		Use:   "institutions",
		Short: "Delete institutions",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			deleteInsts(args)
		},
	}
	portalConfDeleteCmds.AddCommand(deleteInsts)

	deleteCats := &cobra.Command{
		Use:   "user-categories",
		Short: "Delete user categories",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			deleteCats(args)
		},
	}
	portalConfDeleteCmds.AddCommand(deleteCats)

	del.AddCommand(portalConfDeleteCmds)
}

func listUserUSStates() {

	c := userConfig()

	fmt.Print(shcli.TableColumns(shcli.Blue, "US States"))
	fmt.Fprint(tw, shcli.TableColumns(shcli.Blue, "Name", "USPS Two Letter State Code"))

	for _, s := range c.Usstates {
		fmt.Fprint(tw, shcli.TableRow(
			shcli.TableItem{Value: s.Name},
			shcli.TableItem{Value: s.USPS2},
		))
	}

	tw.Flush()
}

func listCountries() {

	c := userConfig()

	fmt.Print(shcli.TableColumns(shcli.Blue, "Counties"))
	fmt.Fprint(tw, shcli.TableColumns(shcli.Blue, "Name", "ISO2 Code"))

	for _, cn := range c.Countries {
		fmt.Fprint(tw, shcli.TableRow(
			shcli.TableItem{Value: cn.Name},
			shcli.TableItem{Value: cn.ISO2},
		))
	}

	tw.Flush()
}

func listInsts() {

	c := userConfig()

	fmt.Print(shcli.TableColumns(shcli.Blue, "Institutions"))
	fmt.Fprint(tw, shcli.TableColumns(shcli.Blue, "Name"))

	for _, i := range c.Institutions {
		fmt.Fprint(tw, shcli.TableRow(
			shcli.TableItem{Value: i},
		))
	}

	tw.Flush()
}

func listCats() {

	c := userConfig()

	fmt.Print(shcli.TableColumns(shcli.Blue, "User Categories"))
	fmt.Fprint(tw, shcli.TableColumns(shcli.Blue, "Category"))

	for _, c := range c.Categories {
		fmt.Fprint(tw, shcli.TableRow(
			shcli.TableItem{Value: c},
		))
	}

	tw.Flush()
}
func userConfig() *portal.GetUserConfigurationsResponse {
	var resp *portal.GetUserConfigurationsResponse

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {
		r, err := cli.GetUserConfigurations(
			context.TODO(),
			&portal.GetUserConfigurationsRequest{},
		)
		if err != nil {
			handleError(err)
		}
		resp = r
		return nil
	})

	if jsonOut {
		fmt.Printf("%s\n", resp)
		os.Exit(0)
	}

	return resp
}

func addEntityTypes(etype string, subtypes []string) {
	updateEntityTypes(etype, subtypes, true)
}

func deleteEntityTypes(etype string, subtypes []string) {
	updateEntityTypes(etype, subtypes, false)
}

func updateEntityTypes(etype string, subtypes []string, add bool) {

	ps := &portal.PatchStrategy{}
	if add {
		ps.Strategy = portal.PatchStrategy_replace
	} else {
		ps.Strategy = portal.PatchStrategy_remove
	}

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {
		_, err := cli.UpdateEntityTypeConfigurations(
			context.TODO(),
			&portal.UpdateEntityTypeConfigurationsRequest{
				Types: []*portal.EntityType{
					{
						Etype:    etype,
						Subtypes: subtypes,
					},
				},
				Patchstrategy: ps,
			},
		)
		if err != nil {
			handleError(err)
		}

		return nil
	})
}

func listETs() {

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {
		r, err := cli.GetEntityTypeConfigurations(
			context.TODO(),
			&portal.GetEntityTypeConfigurationsRequest{},
		)
		if err != nil {
			handleError(err)
		}

		if jsonOut {
			fmt.Printf("%s\n", protojson.Format(r))
			os.Exit(1)
		}

		fmt.Print(shcli.TableColumns(shcli.Blue, "Entity Types"))
		fmt.Fprint(tw, shcli.TableColumns(shcli.Blue, "Type", "Subtypes"))

		for _, t := range r.Types {
			fmt.Fprint(tw, shcli.TableRow(
				shcli.TableItem{Value: t.Etype},
				shcli.TableItem{Value: strings.Join(t.Subtypes, ", ")},
			))
		}

		tw.Flush()
		return nil
	})
}

func addInsts(is []string) {
	updateInsts(is, true)
}

func deleteInsts(is []string) {
	updateInsts(is, false)
}

func updateInsts(is []string, add bool) {

	ps := &portal.PatchStrategy{}
	if add {
		ps.Strategy = portal.PatchStrategy_expand
	} else {
		ps.Strategy = portal.PatchStrategy_subtract
	}

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {
		_, err := cli.UpdateUserConfigurations(
			context.TODO(),
			&portal.UpdateUserConfigurationsRequest{
				Institutions:  is,
				Patchstrategy: ps,
			},
		)
		if err != nil {
			handleError(err)
		}

		return nil
	})
}

func addCats(cats []string) {
	updateCats(cats, true)
}

func deleteCats(cats []string) {
	updateCats(cats, false)
}

func updateCats(cats []string, add bool) {

	ps := &portal.PatchStrategy{}
	if add {
		ps.Strategy = portal.PatchStrategy_expand
	} else {
		ps.Strategy = portal.PatchStrategy_subtract
	}

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {
		_, err := cli.UpdateUserConfigurations(
			context.TODO(),
			&portal.UpdateUserConfigurationsRequest{
				Categories:    cats,
				Patchstrategy: ps,
			},
		)
		if err != nil {
			handleError(err)
		}

		return nil
	})
}
