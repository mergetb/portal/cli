package main

import (
	"fmt"
	"os"
	"sort"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v3"

	"gitlab.com/mergetb/portal/cli/pkg/config"
)

const DEFAULT_WAIT_CONFIG = "1h"

type ConfigSetter func(cfg *config.Config, option string, args []string) error
type ArgParser struct {
	ArgN         int
	Usage        string
	Description  string
	InvalidToGet bool
	ConfigSetter ConfigSetter
}

func StringSetter(cfg *config.Config, option string, args []string) error {
	cfg.Set(option, args[0])
	return nil
}

func IntSetter(cfg *config.Config, option string, args []string) error {
	d, err := strconv.Atoi(args[0])
	if err != nil {
		return fmt.Errorf("'%s' failed to parse as an int", args[0])
	}

	cfg.Set(option, d)
	return nil
}

func DurationSetter(cfg *config.Config, option string, args []string) error {
	_, err := time.ParseDuration(args[0])
	if err != nil {
		return err
	}

	// set as string for storage
	cfg.Set(option, args[0])
	return nil
}

func NoneSetter(option, value string) ConfigSetter {
	return func(cfg *config.Config, _ string, _ []string) error {
		cfg.Set(option, value)
		return nil
	}
}

func NilSetter(cfg *config.Config, option string, args []string) error {
	return nil
}

var VALID_OPTIONS = map[string]ArgParser{
	"port": {
		ArgN:         1,
		Usage:        "port <int>",
		Description:  "set the port to use",
		ConfigSetter: IntSetter,
	},
	"server": {
		ArgN:         1,
		Usage:        "server <string>",
		Description:  "set the server to use",
		ConfigSetter: StringSetter,
	},
	"username": {
		ArgN:         1,
		Usage:        "username <string>",
		Description:  "the username of the logged in user",
		ConfigSetter: NilSetter,
	},
	"token": {
		ArgN:         1,
		Usage:        "token <string>",
		Description:  "the token of the logged in user",
		ConfigSetter: NilSetter,
	},
	"command-wait": {
		ArgN:         1,
		Usage:        "command-wait <duration>",
		Description:  "for commands that have a status tree, how long to wait before the status tree is successful; 0 means to skip the wait",
		ConfigSetter: DurationSetter,
	},
	"status-depth": {
		ArgN:         1,
		Usage:        "status-depth <int>",
		Description:  "at what level to print the status tree when requested, negative numbers are interpreted from the bottom of the tree",
		ConfigSetter: IntSetter,
	},
	"sync": {
		ArgN:  0,
		Usage: "sync",
		Description: fmt.Sprintf(
			"sets the cli to be synchronous for objects that have a status tree; this sets command-wait to the default of %s",
			DEFAULT_WAIT_CONFIG,
		),
		InvalidToGet: true,
		ConfigSetter: NoneSetter("command-wait", DEFAULT_WAIT_CONFIG),
	},
	"async": {
		ArgN:         0,
		Usage:        "async",
		Description:  "sets the cli to be asynchronous for objects that have a status tree; this sets command-wait to 0",
		InvalidToGet: true,
		ConfigSetter: NoneSetter("command-wait", "0"),
	},
}

var VALID_SET_OPTIONS_KEYS = make([]string, len(VALID_OPTIONS))
var VALID_GET_OPTIONS_KEYS = make([]string, 0, len(VALID_OPTIONS))

var mrgcfg *config.Config = nil

func endpoint() string {
	return fmt.Sprintf("%s:%d", mrgcfg.GetString("server"), mrgcfg.GetInt("port"))
}

func init() {
	i := 0
	for k, setter := range VALID_OPTIONS {
		VALID_SET_OPTIONS_KEYS[i] = k
		i++

		if setter.InvalidToGet {
			VALID_GET_OPTIONS_KEYS = append(VALID_GET_OPTIONS_KEYS, k)
		}
	}
	sort.Strings(VALID_SET_OPTIONS_KEYS)
	sort.Strings(VALID_GET_OPTIONS_KEYS)

	initConfig()
}

func configure() {
	config := &cobra.Command{
		Use:   "config",
		Short: "Manage CLI configuration",
	}
	root.AddCommand(config)

	configSet := &cobra.Command{
		Use:   "set",
		Short: "Set a particular CLI behavior",
	}
	negative_one := false
	config.AddCommand(configSet)

	for _, option := range VALID_SET_OPTIONS_KEYS {
		setter := VALID_OPTIONS[option]

		optionSet := &cobra.Command{
			Use:   setter.Usage,
			Short: setter.Description,
			Args:  cobra.MinimumNArgs(setter.ArgN - 1),
			RunE: func(cmd *cobra.Command, args []string) error {

				if negative_one {
					args = append(args, "-1")
				}

				if len(args) != setter.ArgN {
					return fmt.Errorf("accepts %d arg(s), received %d", setter.ArgN, len(args))
				}

				err := setter.ConfigSetter(mrgcfg, option, args)
				if err != nil {
					return err
				}

				err = mrgcfg.Write()
				return err
			},
		}
		optionSet.Flags().BoolVarP(&negative_one, "1", "1", false, "Shortcut to set -1 without using '--' to delimit negative numbers")
		configSet.AddCommand(optionSet)
	}

	configGet := &cobra.Command{
		Use:   fmt.Sprintf("get %s", VALID_GET_OPTIONS_KEYS),
		Short: fmt.Sprintf("Get configuration properties, must be the empty string or one of %s", VALID_GET_OPTIONS_KEYS),
		RunE: func(cmd *cobra.Command, args []string) error {

			var c interface{}

			if len(args) > 0 {
				s, ok := VALID_OPTIONS[args[0]]
				if !ok {
					return fmt.Errorf("unknown property %s", args[0])
				}
				if s.InvalidToGet {
					return fmt.Errorf("permission denied %s", args[0])
				}

				c = mrgcfg.Get(args[0])
			} else {
				c = mrgcfg.GetAll()
			}

			bs, err := yaml.Marshal(c)
			if err != nil {
				return fmt.Errorf("error marshaling config: %v", err)
			}
			fmt.Print(string(bs))

			return nil
		},
	}
	config.AddCommand(configGet)
}

func initConfig() {
	homedir, err := os.UserHomeDir()
	if err != nil {
		log.Fatalf("cannot get user home dir: %v", err)
	}

	cfgdir := fmt.Sprintf("%s/.config/mergetb", homedir)
	mrgcfg, err = config.NewConfig(cfgdir)
	if err != nil {
		log.Fatalf("new config: %+v", err)
	}

	upgradeConfig()

	mrgcfg.SetDefault("server", "")
	mrgcfg.SetDefault("port", 443)
	mrgcfg.SetDefault("command-wait", DEFAULT_WAIT_CONFIG)
	mrgcfg.SetDefault("status-depth", 0)
}

func upgradeConfig() {
	upgraded := false

	defer func() {
		if upgraded {
			log.Info("upgraded mrg config")
			err := mrgcfg.Write()

			if err != nil {
				log.Fatal("error writing mrgcfg: %+v", err)
			}
		}
	}()

	if mrgcfg.IsSet("timeout") {
		upgraded = true
		mrgcfg.DeleteKeys("timeout")
	}

	// this was caused by a bug in 1.3.2 of the golang loopvar
	if mrgcfg.IsSet("sync") {
		upgraded = true
		mrgcfg.DeleteKeys("sync")
	}
}

func getTimeout() time.Duration {
	if syncwait_override.value != nil {
		return *syncwait_override.value
	}

	if async_override {
		return 0
	}

	s := mrgcfg.GetString("command-wait")
	t, err := time.ParseDuration(s)
	if err != nil {
		log.Fatalf("config command-wait %s failed to parse: %+v", s, err)
	}

	if sync_override && t == 0 {
		t, err = time.ParseDuration(DEFAULT_WAIT_CONFIG)
		if err != nil {
			log.Fatalf("DEFAULT_TIMEOUT_CONFIG %s cannot be parsed, this is a bug!", DEFAULT_WAIT_CONFIG)
		}
	}

	return t
}

func getDepth() int {
	if depth_override.value != nil {
		return *depth_override.value
	}

	return mrgcfg.GetInt("status-depth")
}
