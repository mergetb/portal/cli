package sudo

import (
	"os"
)

const (
	SudoEnvUsername string = "MERGE_SUDO_USER"
	SudoEnvToken    string = "MERGE_SUDO_TOKEN"
	SudoEnvSshkey   string = "MERGE_SUDO_SSH_KEY"
)

// InSudo returns true if we're running in mrg sudo; otherwise false
func InSudo() bool {
	return os.Getenv(SudoEnvUsername) != ""
}

// GetSudoUser returns sudo username if we're running in mrg sudo; otherwise an empty string
func GetSudoUser() string {
	return os.Getenv(SudoEnvUsername)
}

func SetSudoUser(username string) {
	os.Setenv(SudoEnvUsername, username)
}

func GetSudoToken() string {
	return os.Getenv(SudoEnvToken)
}

func SetSudoToken(token string) {
	os.Setenv(SudoEnvToken, token)
}

func GetSudoSshKeyPath() string {
	return os.Getenv(SudoEnvSshkey)
}

func SetSudoSshKeyPath(privkey string) {
	os.Setenv(SudoEnvSshkey, privkey)
}

func ClearSudoState() {
	os.Unsetenv(SudoEnvUsername)
	os.Unsetenv(SudoEnvToken)
	os.Unsetenv(SudoEnvSshkey)
}
