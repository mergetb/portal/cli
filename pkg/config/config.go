package config

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os"
	"runtime"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

type Config struct {
	// internal
	viper    *viper.Viper
	confdir  string
	conffile string
}

func (c *Config) Get(key string) interface{} {
	return c.viper.Get(key)
}

func (c *Config) GetAll() map[string]any {
	return c.viper.AllSettings()
}

func (c *Config) GetString(key string) string {
	return c.viper.GetString(key)
}

func (c *Config) GetInt(key string) int {
	return c.viper.GetInt(key)
}

func (c *Config) Set(key string, val any) {
	c.viper.Set(key, val)
}

func (c *Config) SetDefault(key string, val any) {
	c.viper.SetDefault(key, val)
}

func (c *Config) DeleteKeys(keys ...string) error {
	configMap := c.GetAll()
	for _, k := range keys {
		delete(configMap, k)
	}
	encodedConfig, _ := json.MarshalIndent(configMap, "", " ")
	err := c.viper.ReadConfig(bytes.NewReader(encodedConfig))
	c.Write()
	return err
}

func (c *Config) Write() error {
	// TODO: os.Rename() is not atomic on non-Unix systems. We thus may have a race between concurrent
	// uses of mrg on Windows. If this becomes something to worry about, consider a Windows
	// specific filelock implementation like this:
	// https://github.com/juju/fslock/blob/master/fslock_windows.go
	if runtime.GOOS == "windows" {
		return c.viper.WriteConfig()
	}

	return c.safeWriteConfig()
}

func (c *Config) Read() error {
	return c.viper.ReadInConfig()
}

func (c *Config) IsSet(key string) bool {
	return c.viper.IsSet(key)
}

func (c *Config) BindPFlag(key string, flag *pflag.Flag) error {
	return c.viper.BindPFlag(key, flag)
}

// viper is not thread safe. Perform atomic update to the config by writing to a temporary
// file and then renaming, as recommended
// see: https://github.com/tailscale/tailscale/blob/main/atomicfile/atomicfile.go
func (c *Config) safeWriteConfig() error {
	// create tmp file
	tmp, err := os.CreateTemp(c.confdir, "config.*.yml")
	if err != nil {
		return err
	}

	tmpName := tmp.Name()
	defer func() {
		if err != nil {
			tmp.Close()
			os.Remove(tmpName)
		}
	}()

	// set viper to use the tmp file for config
	c.viper.SetConfigFile(tmpName)

	// write the config
	err = c.viper.WriteConfig()
	if err != nil {
		return err
	}

	// rename the temp file to actual config file
	err = os.Rename(tmpName, c.conffile)
	return err
}

func NewConfig(cfgdir string) (*Config, error) {
	cfg := new(Config)

	cfg.confdir = cfgdir
	cfg.conffile = fmt.Sprintf("%s/config.yml", cfg.confdir)

	cfg.viper = viper.New()
	cfg.viper.SetConfigName("config")
	cfg.viper.SetConfigType("yaml")
	cfg.viper.AddConfigPath(cfg.confdir)

	err := os.MkdirAll(cfg.confdir, 0755)
	if err != nil {
		return nil, fmt.Errorf("cannot create config dir: %v", err)
	}

	// if config file exists, read in the config from it
	_, err = os.Stat(cfg.conffile)
	if !os.IsNotExist(err) {
		err = cfg.Read()
		if err != nil {
			return nil, fmt.Errorf("error reading config: %v", err)
		}
	}

	return cfg, nil
}
