package main

import (
	"context"
	"fmt"
	"net"
	"sort"
	"strings"

	"github.com/maruel/natural"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/cli/clients"
)

func generation() {
	generate := &cobra.Command{
		Use:   "generate",
		Short: "Generate files or configurations pertaining to nodes of a materialization",
	}
	nodecmd.AddCommand(generate)

	var prefix string
	etchosts := &cobra.Command{
		Use:   "etchosts <realization>.<experiment>.<project>",
		Short: "Generate an /etc/hosts stanza for the realization",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			generateEtcHosts(args[0], prefix)
		},
	}
	etchosts.Flags().StringVarP(&prefix, "prefix", "p", "", "prepend hostnames with with prefix")
	generate.AddCommand(etchosts)

	inventory := &cobra.Command{
		Use:   "inventory <realization>.<experiment>.<project>",
		Short: "Generate an ansible inventory for the realization",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			generateInventory(args[0])
		},
	}
	generate.AddCommand(inventory)
}

func nodes() {
	generation()
	reboot()
}

func generateEtcHosts(realization, prefix string) {

	if jsonOut {
		log.Fatalf("this command only supports /etc/hosts format")
	}

	rlz, exp, proj := parseRzid(realization)

	err := clients.Realize(endpoint(), token(), func(cli portal.RealizeClient) error {

		resp, err := cli.GetRealization(context.TODO(), &portal.GetRealizationRequest{
			Project:     proj,
			Experiment:  exp,
			Realization: rlz,
		})
		if err != nil {
			handleError(err)
		}

		data := map[string][]string{}
		seen := map[string]bool{}

		for xnode, rnode := range resp.Result.Realization.Nodes {
			for idx, sock := range rnode.Node.Sockets {
				for _, addr := range sock.Addrs {
					ip, _, _ := net.ParseCIDR(addr)
					a := ip.String()

					// If this is the first time we've seen this node, make this the default
					// entry for the node name.
					if _, ok := seen[xnode]; !ok {
						data[a] = append(data[a], fmt.Sprintf("%s%s", prefix, xnode))
						seen[xnode] = true
					} else {
						data[a] = append(data[a], fmt.Sprintf("%s%s-%d", prefix, xnode, idx))
					}
				}
			}
		}

		keys := []string{}
		for addr := range data {
			keys = append(keys, addr)
		}
		sort.Sort(natural.StringSlice(keys))

		for _, key := range keys {
			fmt.Printf("%s %s\n", key, strings.Join(data[key], "\t"))
		}

		return nil
	})

	handleError(err)

}

func generateInventory(realization string) {

	if jsonOut {
		log.Fatalf("this command only supports ansible INI format")
	}

	rlz, exp, proj := parseRzid(realization)

	err := clients.Realize(endpoint(), token(), func(cli portal.RealizeClient) error {

		resp, err := cli.GetRealization(context.TODO(), &portal.GetRealizationRequest{
			Project:     proj,
			Experiment:  exp,
			Realization: rlz,
		})
		if err != nil {
			handleError(err)
		}

		groups := map[string][]string{}
		all := []string{}

		for xnode, rnode := range resp.Result.Realization.Nodes {
			all = append(all, xnode)

			kv := rnode.Node.Properties.GetKeyvalues()
			if kv == nil {
				continue
			}

			if grps, ok := kv["group"]; ok {
				for _, val := range grps.GetValues() {
					groups[val] = append(groups[val], xnode)
				}
			}
		}

		fmt.Println("[all]")

		keys := append([]string{}, all...)
		sort.Strings(keys)

		for _, n := range keys {
			fmt.Println(n)
		}

		gkeys := []string{}
		for g := range groups {
			gkeys = append(gkeys, g)
		}
		sort.Strings(gkeys)

		for _, grp := range gkeys {
			names := groups[grp]
			fmt.Println()
			fmt.Printf("[%s]\n", grp)

			keys = append([]string{}, names...)
			sort.Strings(keys)

			for _, n := range keys {
				fmt.Println(n)
			}
		}

		return nil
	})

	handleError(err)
}

func reboot() {

	mode := &FlagEnum{Choices: rebootModes()}
	allNodes := false
	rbcmd := &cobra.Command{
		Use:   "reboot <realization>.<experiment>.<project> [<nodes>...]",
		Short: "Reboot nodes in a materialization via their names in the experiment model",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			if mode.String() == "" {
				mode.Set(mode.Choices[0])
			}

			nodes := []string{}
			if len(args) > 1 {
				nodes = args[1:]
			}

			rlz, exp, proj := parseRzid(args[0])
			rb(rlz, exp, proj, mode.String(), allNodes, nodes)
		},
	}
	rbcmd.Flags().BoolVarP(&allNodes, "all", "a", false, "Reboot all nodes in the materialization")
	mode.AddToFlagSetShort(
		rbcmd.Flags(),
		"mode",
		"m",
		fmt.Sprintf("reboot mode: %s (default: %s)", mode.Help(), mode.Choices[0]),
	)
	nodecmd.AddCommand(rbcmd)
}

func rb(rid, eid, pid, mode string, allNodes bool, nodes []string) {

	m, err := ToReboot(mode)
	if err != nil {
		log.Fatal(err)
	}

	if len(nodes) == 0 && !allNodes {
		log.Fatal("No nodes provided")
	}

	err = clients.Materialize(endpoint(), token(), func(cli portal.MaterializeClient) error {
		_, err := cli.RebootMaterialization(
			context.TODO(),
			&portal.RebootMaterializationRequest{
				Mode:        m,
				Project:     pid,
				Experiment:  eid,
				Realization: rid,
				Hostnames:   nodes,
				AllNodes:    allNodes,
			},
		)
		return err
	})
	if err != nil {
		handleError(err)
	}

	checkMaterialization(pid, eid, rid)
}
