package main

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/cli/clients"
	scli "gitlab.com/mergetb/tech/shared/cli"
	pviews "gitlab.com/mergetb/tech/shared/cli/portal"
	"google.golang.org/protobuf/encoding/protojson"
)

func materialization() {

	mtz := &cobra.Command{
		Use:   "materialize <realization>.<experiment>.<project>",
		Short: "Materialize a realization",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			rlz, exp, proj := parseRzid(args[0])
			mtz(rlz, exp, proj)
		},
	}
	root.AddCommand(mtz)

	demtz := &cobra.Command{
		Use:   "dematerialize <realization>.<experiment>.<project>",
		Short: "Dematerialize a realization",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			rlz, exp, proj := parseRzid(args[0])
			demtz(rlz, exp, proj)
		},
	}
	root.AddCommand(demtz)

	filter := &FlagEnum{Choices: []string{"user", "all"}}
	listmat := &cobra.Command{
		Use:   "materializations",
		Short: "List materializations",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			listmat(filter.String())
		},
	}
	filter.AddToFlagSet(
		listmat.Flags(),
		"filter",
		"Filter materializations: "+filter.Help()+". Default: "+filter.Choices[0],
	)
	list.AddCommand(listmat)

	var withStatus bool
	showmat := &cobra.Command{
		Use:   "materialization <realization>.<experiment>.<project>",
		Short: "Show a materialization",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {

			rlz, exp, proj := parseRzid(args[0])
			if withStatus {
				showmatstatus(rlz, exp, proj)
			} else {
				showmat(rlz, exp, proj)
			}
		},
	}
	showmat.Flags().BoolVarP(&withStatus, "with-status", "S", false, "Show status of materialization")
	show.AddCommand(showmat)

}

func mtz(rid, eid, pid string) {

	err := clients.Materialize(endpoint(), token(), func(cli portal.MaterializeClient) error {

		_, err := cli.Materialize(
			context.TODO(),
			&portal.MaterializeRequest{
				Project:     pid,
				Experiment:  eid,
				Realization: rid,
			},
		)
		return err
	},
	)
	if err != nil {
		handleError(err)
	}

	checkMaterialization(pid, eid, rid)
}

func demtz(rid, eid, pid string) {

	err := clients.Materialize(endpoint(), token(), func(cli portal.MaterializeClient) error {

		_, err := cli.Dematerialize(
			context.TODO(),
			&portal.DematerializeRequest{
				Project:     pid,
				Experiment:  eid,
				Realization: rid,
			},
		)
		return err
	},
	)
	if err != nil {
		handleError(err)
	}
}

func listmat(filter string) {

	fm := portal.FilterMode_ByUser
	if filter == "all" {
		fm = portal.FilterMode_ByAll
	}

	clients.Materialize(endpoint(), token(), func(cli portal.MaterializeClient) error {

		resp, err := cli.GetMaterializationsV2(
			context.TODO(),
			&portal.GetMaterializationsRequestV2{
				Filter: fm,
			},
		)
		if err != nil {
			handleError(err)
		}

		if jsonOut {
			fmt.Printf("%s\n", protojson.Format(resp))
			return nil

		}

		if colorless {
			scli.DisableColor()
		}

		if len(resp.Materializations) > 0 {
			table := pviews.ListMaterializationsToTable(resp.Materializations, resp.Statuses)
			fmt.Fprint(tw, table.ToString())
		}

		return nil

	})

}

func showmat(realization, experiment, project string) {

	clients.Materialize(endpoint(), token(), func(cli portal.MaterializeClient) error {

		resp, err := cli.GetMaterialization(context.TODO(), &portal.GetMaterializationRequest{
			Project:     project,
			Experiment:  experiment,
			Realization: realization,
		})
		if err != nil {
			handleError(err)
		}

		if jsonOut {

			fmt.Printf("%s\n", protojson.Format(resp))
			return nil
		}

		dumpmat(resp.Materialization, resp.Ingresses)

		return nil

	})

}

func showmatstatus(realization, experiment, project string) {

	clients.Materialize(endpoint(), token(), func(cli portal.MaterializeClient) error {

		resp, err := cli.GetMaterializationV2(context.TODO(), &portal.GetMaterializationRequestV2{
			Project:     project,
			Experiment:  experiment,
			Realization: realization,
			StatusMS:    -1,
		})

		if err != nil {
			log.Fatal(err)
		}

		if jsonOut {
			fmt.Println(protojson.Format(resp))
			return nil
		}

		if colorless {
			scli.DisableColor()
		}

		pviews.WriteTaskForest(resp.Status, getDepth(), true, tw)
		osExit1IfUnsuccessful(resp.Status)

		return nil

	})

}

func dumpmat(r *portal.Materialization, ings *portal.Ingresses) {

	fmt.Println("Metal:")
	for _, x := range r.Metal {
		fmt.Printf("%s -> %s\n", x.Model.Id, x.Resource)
	}

	fmt.Println("Vm:")
	for _, x := range r.Vms {
		fmt.Printf("%s -> %s\n", x.VmAlloc.Model.Id, x.VmAlloc.Resource)
	}

	fmt.Println("Links:")
	for _, x := range r.Links {
		fmt.Printf("%s", x.Realization.ToString("  "))
	}

	if ings != nil && ings.Ingresses != nil {
		fmt.Printf("Ingresses:\n")
		for _, x := range ings.Ingresses {
			fmt.Printf("\t%s:%d [%s] --> %s\n", x.Hostname, x.Hostport, x.Protocol, x.Ingress)
		}
	}

	fmt.Printf("Infraserver: %s\n", r.GetParams().GetInfrapodServer())
}
