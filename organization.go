package main

import (
	"context"
	"fmt"
	"sort"

	"github.com/maruel/natural"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"google.golang.org/protobuf/encoding/protojson"

	"gitlab.com/mergetb/portal/cli/clients"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	shcli "gitlab.com/mergetb/tech/shared/cli"
	pviews "gitlab.com/mergetb/tech/shared/cli/portal"
)

func organization() {

	var desc string

	mode := &FlagEnum{Choices: accessModes()}
	filter := &FlagEnum{Choices: []string{"all", "user"}}

	organizations := &cobra.Command{
		Use:   "organizations",
		Short: "List organizations that you are a member of",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			listorganizations(filter.String())
		},
	}
	filter.AddToFlagSet(
		organizations.Flags(),
		"filter",
		"Filter organizations: "+filter.Help()+". Default: "+filter.Choices[0],
	)
	list.AddCommand(organizations)

	var cat string
	var subcat string
	no := &cobra.Command{
		Use:   "organization <name>",
		Short: "Create a new organization",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			if mode.String() == "" {
				mode.Set(mode.Choices[0])
			}
			newOrganization(args[0], desc, mode.String(), cat, subcat)
		},
	}
	no.Flags().StringVarP(&cat, "category", "", "", "Specify the organization category")
	no.Flags().StringVarP(&subcat, "subcategory", "", "", "Specify the organization sub-category")
	no.Flags().StringVarP(&desc, "desc", "d", "", "organization description")
	mode.AddToFlagSetShort(
		no.Flags(),
		"mode",
		"m",
		"access mode: "+mode.Help()+" (default: "+mode.Choices[0]+")",
	)

	newcmd.AddCommand(no)

	do := &cobra.Command{
		Use:   "organization <name>",
		Short: "Delete an organization",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			delOrganization(args[0])
		},
	}
	del.AddCommand(do)

	var withStatus bool
	so := &cobra.Command{
		Use:   "organization <name>",
		Short: "Show organization deatils",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			showOrganization(args[0], withStatus)
		},
	}
	so.Flags().BoolVarP(&withStatus, "with-status", "S", false, "Show task status for the organization")
	show.AddCommand(so)

	uo := &cobra.Command{
		Use:   "organization",
		Short: "Update organization information",
	}

	setMode := &cobra.Command{
		Use:   "mode <name> <mode>",
		Short: "Set the organization access mode (" + mode.Help() + ")",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			setOrgMode(args[0], args[1])
		},
	}
	uo.AddCommand(setMode)

	setCat := &cobra.Command{
		Use:   "category <name> <category> <subcategory>",
		Short: "Set the organization category and subcategory (subcategory is optional)",
		Args:  cobra.RangeArgs(2, 3),
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) == 2 {
				setOrgCat(args[0], args[1], "")
			} else {
				setOrgCat(args[0], args[1], args[2])
			}

		},
	}
	uo.AddCommand(setCat)

	update.AddCommand(uo)

	projCmd := &cobra.Command{
		Use:   "project",
		Short: "Add or remove a project form an organization",
	}
	uo.AddCommand(projCmd)

	addProj := &cobra.Command{
		Use:   "add <organization> <project>",
		Short: "Add the given project to the given organization",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			addProjToOrg(args[0], args[1])
		},
	}
	projCmd.AddCommand(addProj)

	delProj := &cobra.Command{
		Use:   "delete <organization> <project>",
		Short: "Delete the given project from the given organization",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			delProjFromOrg(args[0], args[1])
		},
	}
	projCmd.AddCommand(delProj)

}

func listorganizations(filter string) {

	var organizations []*portal.Organization

	fm := portal.FilterMode_ByAll
	if filter == "user" {
		fm = portal.FilterMode_ByUser
	}

	err := clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		resp, err := cli.GetOrganizations(context.TODO(), &portal.GetOrganizationsRequest{Filter: fm})
		if err != nil {
			handleError(err)
		}

		if jsonOut {
			fmt.Printf("%s\n", protojson.Format(resp))
			return fmt.Errorf("JSON")
		}

		organizations = resp.Organizations

		return nil

	})

	if err != nil && err.Error() == "JSON" {
		return
	}

	fmt.Fprint(tw, shcli.TableColumns(
		shcli.Blue, "Name", "Description", "Category", "Subcategory", "OID", "Mode", "State"),
	)
	for _, o := range organizations {
		fmt.Fprint(tw, shcli.TableRow(
			shcli.TableItem{Value: o.Name},
			shcli.TableItem{Value: o.Description},
			shcli.TableItem{Value: o.GetCategory()},
			shcli.TableItem{Value: o.GetSubcategory()},
			shcli.TableItem{Value: o.Oid},
			shcli.TableItem{Value: o.AccessMode},
			shcli.TableItem{Value: o.State},
		))
	}

	tw.Flush()
}

func setOrgMode(pid, mode string) {

	m, err := ToAccess(mode)
	if err != nil {
		log.Fatal(err)
	}

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {
		_, err := cli.UpdateOrganization(
			context.TODO(),
			&portal.UpdateOrganizationRequest{
				Name: pid,
				AccessMode: &portal.AccessModeUpdate{
					Value: m,
				},
			},
		)

		if err != nil {
			handleError(err)
		}

		return nil
	})
}

func setOrgCat(pid, cat, subcat string) {

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {
		_, err := cli.UpdateOrganization(
			context.TODO(),
			&portal.UpdateOrganizationRequest{
				Name:        pid,
				Category:    cat,
				Subcategory: subcat,
			},
		)

		if err != nil {
			handleError(err)
		}

		return nil
	})
}

func newOrganization(oid, desc, mode, cat, subcat string) {

	uid := getLoggedInUsername()

	m, err := ToAccess(mode)
	if err != nil {
		log.Fatal(err)
	}

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		_, err := cli.CreateOrganization(context.TODO(), &portal.CreateOrganizationRequest{
			User: uid, // will be set as Creator Member
			Organization: &portal.Organization{
				Name:        oid,
				Description: desc,
				AccessMode:  m,
				Category:    cat,
				Subcategory: subcat,
			},
		})
		if err != nil {
			handleError(err)
		}

		return nil
	})

	checkOrganization(oid)
}

func delOrganization(oid string) {

	uid := getLoggedInUsername()

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		_, err := cli.DeleteOrganization(context.TODO(), &portal.DeleteOrganizationRequest{
			User: uid,
			Name: oid,
		})
		if err != nil {
			handleError(err)
		}

		return nil
	})
}

func showOrganization(pid string, withStatus bool) {

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		resp, err := cli.GetOrganization(
			context.TODO(),
			&portal.GetOrganizationRequest{
				Name:     pid,
				StatusMS: timeoutAsBool(withStatus),
			},
		)
		if err != nil {
			handleError(err)
		}

		if jsonOut {
			fmt.Printf("%s\n", protojson.Format(resp))
			return nil
		}

		if withStatus {
			pviews.WriteTaskForest(resp.Status, getDepth(), true, tw)
			osExit1IfUnsuccessful(resp.Status)

			return nil
		}

		o := resp.Organization
		fmt.Fprintf(tw, "Name: %s\n", o.Name)
		if o.Description != "" {
			fmt.Fprintf(tw, "Description: %s\n", o.Description)
		}
		fmt.Fprintf(tw, "Mode: %s\n", o.AccessMode)
		fmt.Fprintf(tw, "State: %s\n", o.State)
		fmt.Fprintf(tw, "OID: %d\n", o.Oid)

		if o.Category != "" {
			fmt.Fprintf(tw, "Category: %s\n", o.Category)
		}

		if o.Subcategory != "" {
			fmt.Fprintf(tw, "Subcategory: %s\n", o.Subcategory)
		}

		sortedKeys := func(members map[string]*portal.Member) []string {
			keys := make([]string, 0, len(members))
			for k := range members {
				keys = append(keys, k)
			}
			sort.Sort(natural.StringSlice(keys))
			return keys
		}

		if len(o.Members) > 0 {

			fmt.Fprintf(tw, "Members:\n")
			fmt.Fprintf(tw, "\tMember\tState\tRole\n")
			fmt.Fprintf(tw, "\t------\t-----\t----\n")
			for _, key := range sortedKeys(o.Members) {
				fmt.Fprintf(tw, "\t%s\t%s\t%s\n", key, o.Members[key].State, o.Members[key].Role)
			}
		}

		if len(o.Projects) > 0 {
			fmt.Fprintf(tw, "Projects:\n")
			fmt.Fprintf(tw, "\tMember\tState\tRole\n")
			fmt.Fprintf(tw, "\t------\t-----\t----\n")
			for _, key := range sortedKeys(o.Projects) {
				fmt.Fprintf(tw, "\t%s\t%s\t%s\n", key, o.Projects[key].State, o.Projects[key].Role)
			}
		}

		tw.Flush()

		return nil
	})
}

func addProjToOrg(o, p string) {

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {
		_, err := cli.UpdateOrganization(
			context.TODO(),
			&portal.UpdateOrganizationRequest{
				Name: o,
				Projects: &portal.MembershipUpdate{
					Set: map[string]*portal.Member{
						p: {
							Role:  portal.Member_Member,
							State: portal.Member_Active,
						},
					},
				},
			},
		)
		if err != nil {
			handleError(err)
		}
		return nil
	})
}

func delProjFromOrg(o, p string) {

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {
		_, err := cli.UpdateOrganization(
			context.TODO(),
			&portal.UpdateOrganizationRequest{
				Name: o,
				Projects: &portal.MembershipUpdate{
					Remove: []string{p},
				},
			},
		)
		if err != nil {
			handleError(err)
		}
		return nil
	})
}
