package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/cli/clients"
	"google.golang.org/protobuf/encoding/protojson"
)

func model() {

	var quiet = false
	compile := &cobra.Command{
		Use:   "compile <model-file>",
		Short: "Compile a model. Return success or compile error stack on failure.",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			compile(args[0], quiet)
		},
	}
	compile.Flags().BoolVarP(&quiet, "quiet", "q", false, "Do not show the compiled model on success.")
	root.AddCommand(compile)

	var branch, tag = "", ""
	push := &cobra.Command{
		Use:   "push <model-file> <experiment>.<project>",
		Short: "Push a model to the given experiment repository",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			exp, proj := parseXpid(args[1])
			push(args[0], exp, proj, branch, tag)
		},
	}
	push.Flags().StringVarP(&branch, "branch", "", "", "Repository branch to push the model to.")
	push.Flags().StringVarP(&tag, "tag", "", "", "Tag the commit with the given tag.")
	root.AddCommand(push)
}

func compile(path string, quiet bool) {

	m, err := os.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}

	clients.Model(endpoint(), token(), func(cli portal.ModelClient) error {

		resp, err := cli.Compile(
			context.TODO(),
			&portal.CompileRequest{
				Model: string(m),
			},
		)

		if err != nil {
			handleError(err)
		}

		if jsonOut {
			fmt.Printf("%s\n", protojson.Format(resp))
			return nil
		}

		if !resp.Success {
			fmt.Printf("Compilation Error:\n")
			for _, line := range resp.Errors {
				fmt.Println(line)
			}
		} else if !quiet {
			var buf bytes.Buffer
			err := json.Indent(&buf, []byte(resp.Network), "", "    ")
			if err != nil {
				handleError(err)
			}

			fmt.Println(buf.String())
		}

		return nil
	},
	)
}

func push(path, eid, pid, branch, tag string) {

	m, err := os.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}

	clients.Model(endpoint(), token(), func(cli portal.ModelClient) error {

		resp, err := cli.Push(
			context.TODO(),
			&portal.PushRequest{
				Model:      string(m),
				Experiment: eid,
				Project:    pid,
				Branch:     branch,
				Tag:        tag,
			},
		)

		if err != nil {
			handleError(err)
		}

		if jsonOut {
			fmt.Printf("%s\n", protojson.Format(resp))
		} else {
			fmt.Printf("Push succeeded. Revision: %s\n", resp.Revision)
		}

		return nil
	},
	)

	checkExperiment(pid, eid)
}
