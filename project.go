package main

import (
	"context"
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"google.golang.org/protobuf/encoding/protojson"

	"gitlab.com/mergetb/portal/cli/clients"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	shcli "gitlab.com/mergetb/tech/shared/cli"
	pviews "gitlab.com/mergetb/tech/shared/cli/portal"
)

func project() {

	filter := &FlagEnum{Choices: []string{"user", "all"}}
	mode := &FlagEnum{Choices: accessModes()}

	projects := &cobra.Command{
		Use:   "projects",
		Short: "List projects that you are a member of",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			listprojects(filter.String())
		},
	}
	filter.AddToFlagSet(
		projects.Flags(),
		"filter",
		"Filter projects: "+filter.Help()+". Default: "+filter.Choices[0],
	)
	list.AddCommand(projects)

	var org string
	var cat string
	var subcat string
	np := &cobra.Command{
		Use:   "project <name> <description>",
		Short: "Create a new project",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			if mode.String() == "" {
				mode.Set(mode.Choices[0])
			}
			newProject(args[0], args[1], org, mode.String(), cat, subcat)
		},
	}
	np.Flags().StringVarP(&cat, "category", "", "", "Specify the project category")
	np.Flags().StringVarP(&subcat, "subcategory", "", "", "Specify the project sub-category")
	np.Flags().StringVarP(&org, "organization", "o", "", "add the project to the given organization")
	mode.AddToFlagSetShort(
		np.Flags(),
		"mode",
		"m",
		"access mode: "+mode.Help()+" (default: "+mode.Choices[0]+")",
	)
	newcmd.AddCommand(np)

	dp := &cobra.Command{
		Use:   "project <name>",
		Short: "Delete a project",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			delProject(args[0])
		},
	}
	del.AddCommand(dp)

	var withStatus bool
	sp := &cobra.Command{
		Use:   "project <name>",
		Short: "Show project deatils",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			showProject(args[0], withStatus)
		},
	}
	sp.Flags().BoolVarP(&withStatus, "with-status", "S", false, "Show task status of project")
	show.AddCommand(sp)

	updateProj := &cobra.Command{
		Use:   "project",
		Short: "Update project information",
	}

	setMode := &cobra.Command{
		Use:   "mode project <mode>",
		Short: "Set the project access mode. Access mode: " + mode.Help(),
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			setProjMode(args[0], args[1])
		},
	}
	updateProj.AddCommand(setMode)

	update.AddCommand(updateProj)

}

func setProjMode(pid, mode string) {

	m, err := ToAccess(mode)
	if err != nil {
		log.Fatal(err)
	}

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {
		_, err := cli.UpdateProject(
			context.TODO(),
			&portal.UpdateProjectRequest{
				Name: pid,
				AccessMode: &portal.AccessModeUpdate{
					Value: m,
				},
			},
		)

		if err != nil {
			handleError(err)
		}

		return nil
	})
}

func listprojects(filter string) {

	var projects []*portal.Project

	fm := portal.FilterMode_ByUser
	if filter == "all" {
		fm = portal.FilterMode_ByAll
	}

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		resp, err := cli.GetProjects(context.TODO(), &portal.GetProjectsRequest{Filter: fm})
		if err != nil {
			handleError(err)
		}

		if jsonOut {
			fmt.Printf("%s\n", protojson.Format(resp))
			return nil
		}

		projects = resp.Projects

		return nil

	})

	if len(projects) == 0 {
		return
	}

	fmt.Fprint(tw, shcli.TableColumns(shcli.Blue, "Name", "Description", "Category", "Subcategory", "Mode", "GID", "Organization", "Org. Membership"))

	for _, p := range projects {

		m := p.GetOrgMembership()
		var membership string
		if m != nil {
			membership = fmt.Sprintf("%s/%s", m.GetRole().String(), m.GetState().String())
		}

		fmt.Fprint(tw, shcli.TableRow(
			shcli.TableItem{Value: p.GetName()},
			shcli.TableItem{Value: p.GetDescription()},
			shcli.TableItem{Value: p.GetCategory()},
			shcli.TableItem{Value: p.GetSubcategory()},
			shcli.TableItem{Value: p.GetAccessMode()},
			shcli.TableItem{Value: p.GetGid()},
			shcli.TableItem{Value: p.GetOrganization()},
			shcli.TableItem{Value: membership},
		))
	}
	tw.Flush()
}

func newProject(pid, desc, org, mode, cat, subcat string) {

	uid := getLoggedInUsername()

	m, err := ToAccess(mode)
	if err != nil {
		log.Fatal(err)
	}

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		_, err := cli.CreateProject(context.TODO(), &portal.CreateProjectRequest{
			User: uid, // will be set as Creator Member
			Project: &portal.Project{
				Name:         pid,
				Description:  desc,
				Organization: org,
				AccessMode:   m,
				Category:     cat,
				Subcategory:  subcat,
			},
		})
		if err != nil {
			handleError(err)
		}

		return nil
	})
}

func delProject(pid string) {

	uid := getLoggedInUsername()

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		_, err := cli.DeleteProject(context.TODO(), &portal.DeleteProjectRequest{
			User: uid,
			Name: pid,
		})
		if err != nil {
			handleError(err)
		}

		return nil
	})
}

func showProject(pid string, withStatus bool) {

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		resp, err := cli.GetProject(
			context.TODO(),
			&portal.GetProjectRequest{
				Name:     pid,
				StatusMS: timeoutAsBool(withStatus),
			},
		)
		if err != nil {
			handleError(err)
		}

		if jsonOut {
			fmt.Printf("%s\n", protojson.Format(resp))
			return nil
		}

		if withStatus {
			pviews.WriteTaskForest(resp.Status, getDepth(), true, tw)
			osExit1IfUnsuccessful(resp.Status)
			return nil
		}

		// Members     map[string]*Member
		// Experiments []string
		// Gid         uint32
		// Organizations string

		p := resp.Project
		fmt.Fprintf(tw, "Name: %s\n", p.Name)
		if p.Description != "" {
			fmt.Fprintf(tw, "Description: %s\n", p.Description)
		}
		fmt.Fprintf(tw, "Mode: %s\n", p.AccessMode)
		fmt.Fprintf(tw, "GID: %d\n", p.Gid)

		if p.Category != "" {
			fmt.Fprintf(tw, "Category: %s\n", p.Category)
		}

		if p.Subcategory != "" {
			fmt.Fprintf(tw, "Subcategory: %s\n", p.Subcategory)
		}

		fmt.Fprintf(tw, "\nMembers:\n")
		fmt.Fprintf(tw, "\tUser\tState\tRole\n")
		fmt.Fprintf(tw, "\t------\t-----\t----\n")
		for user, m := range p.Members {
			fmt.Fprintf(tw, "\t%s\t%s\t%s\n", user, m.State, m.Role)
		}

		if p.Organization != "" {
			fmt.Fprintf(tw, "\nOrganization:\n")
			fmt.Fprintf(tw, "\tName\tState\tRole\n")
			fmt.Fprintf(tw, "\t------\t-----\t----\n")
			fmt.Fprintf(tw, "\t%s\t%s\t%s\n", p.Organization, p.OrgMembership.State, p.OrgMembership.Role)
		}

		if len(p.Experiments) > 0 {
			fmt.Fprintf(tw, "\nExperiments: %s\n", strings.Join(p.Experiments, ", "))
		}

		tw.Flush()

		return nil
	})
}
