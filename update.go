package main

import (
	"context"
	"fmt"
	"os"
	"path"

	"github.com/creativeprojects/go-selfupdate"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

func update_binary() {

	url := "https://gitlab.com"
	owner := "mergetb"
	repo := "portal/cli"
	target := ""
	force := false

	updateBin := &cobra.Command{
		Use:   "binary <VERSION>",
		Short: "Update the current mrg binary to the supplied version, or the latest version if not supplied",
		Args:  cobra.MaximumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) > 0 {
				target = args[0]
			}

			_, err := doupdate(url, owner, repo, target, force)
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	updateBin.Flags().StringVarP(&url, "url", "u", url, "The base URL to look")
	updateBin.Flags().StringVarP(&owner, "owner", "o", owner, "The owner of the repository")
	updateBin.Flags().StringVarP(&repo, "repo", "r", repo, "The repo itself")
	updateBin.Flags().BoolVarP(&force, "force", "f", force, "Force update, ignoring version and missing checksums.txt file")

	update.AddCommand(updateBin)

}

func doupdate(url, owner, repo, target string, force bool) (bool, error) {
	source, err := selfupdate.NewGitLabSource(selfupdate.GitLabConfig{
		BaseURL: url,
	})
	if err != nil {
		return false, err
	}

	updater, err := selfupdate.NewUpdater(selfupdate.Config{
		Source:    source,
		Validator: &selfupdate.ChecksumValidator{UniqueFilename: "checksums.txt"},
	})

	if err != nil {
		return false, err
	}

	var release *selfupdate.Release
	found := false

	// -1 for a downgrade
	// 0 for same version
	// 1 for upgrade
	direction := 0

	if target == "" {
		release, found, err = updater.DetectLatest(context.Background(), selfupdate.NewRepositorySlug(owner, repo))

		// if force, try again without checksums.txt file
		if err != nil && force {
			updater, err = selfupdate.NewUpdater(selfupdate.Config{
				Source: source,
			})

			release, found, err = updater.DetectLatest(context.Background(), selfupdate.NewRepositorySlug(owner, repo))
		}

		if err != nil {
			return false, err
		}

		if !found {
			return false, fmt.Errorf("Could not find the latest release at %s", path.Join(url, owner, repo))
		}

		switch {
		case release.LessThan(version):
			direction = -1
		case release.GreaterThan(version):
			direction = 1
		default:
			direction = 0
		}

		if !force && direction < 1 {
			log.Infof("Current version (%s) is the latest, not updating", version)
			return false, nil
		}
	} else {
		release, found, err = updater.DetectVersion(context.Background(), selfupdate.NewRepositorySlug(owner, repo), target)

		// if force, try again without checksums.txt file
		if err != nil && force {
			updater, err = selfupdate.NewUpdater(selfupdate.Config{
				Source: source,
			})

			release, found, err = updater.DetectLatest(context.Background(), selfupdate.NewRepositorySlug(owner, repo))
		}

		if err != nil {
			return false, err
		}

		if !found {
			return false, fmt.Errorf("Could not find the target release of %s at %s", target, path.Join(url, owner, repo))
		}

		switch {
		case release.LessThan(version):
			direction = -1
		case release.GreaterThan(version):
			direction = 1
		default:
			direction = 0
		}

		if !force && direction == 0 {
			log.Infof("Current version is identical to %s, not updating", version)
			return false, nil
		}
	}

	exe, err := os.Executable()
	if err != nil {
		return false, fmt.Errorf("Could not locate executable path")
	}
	err = updater.UpdateTo(context.Background(), release, exe)
	if err != nil {
		return false, err
	}

	switch direction {
	case 0:
		log.Printf("Successfully reinstalled version %s", release.Version())
	case -1:
		log.Printf("Successfully downgraded to version %s", release.Version())
	case 1:
		log.Printf("Successfully updated to version %s", release.Version())
	}

	return true, nil
}
