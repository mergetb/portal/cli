//go:build linux || darwin || freebsd
// +build linux darwin freebsd

package mrgssh

import (
	"os"
	"syscall"
)

func WindowResizeSignal() os.Signal {
	return syscall.SIGWINCH
}
