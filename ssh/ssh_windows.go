//go:build windows
// +build windows

package mrgssh

import (
	"os"
)

func WindowResizeSignal() os.Signal {
	return nil
}
