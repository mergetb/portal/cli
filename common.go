package main

func cfunc(s, c string) string {
	if colorless {
		return s
	}
	return "\033[" + c + s + "\033[39m"
}

func red(s string) string     { return cfunc(s, "31m") }
func green(s string) string   { return cfunc(s, "32m") }
func yellow(s string) string  { return cfunc(s, "33m") }
func blue(s string) string    { return cfunc(s, "34m") }
func magenta(s string) string { return cfunc(s, "35m") }
func cyan(s string) string    { return cfunc(s, "36m") }
func neutral(s string) string { return cfunc(s, "39m") }
