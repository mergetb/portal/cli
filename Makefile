PREFIX  ?= /usr/local
DESTDIR ?=
BINDIR  ?= $(PREFIX)/bin
VERSION  = $(shell git describe --tags)
LDFLAGS  = "-X main.version=$(VERSION)"

SRC=activate.go common.go config.go cred.go errors.go enum.go experiment.go facility.go \
	identity.go ingress.go keys.go main.go materialize.go member.go model.go node.go communications.go \
	pool.go project.go organization.go realize.go ssh.go update.go user.go util.go vpn.go command_timeout.go \
	xdc.go portal.go ssh/hosts.go ssh/ssh_unix.go ssh/ssh_windows.go \
	pkg/config/config.go clients/*.go custom_arg_types.go \
	go.mod go.sum

.PHONY: all clean install_linux

all: \
    build/x86_64/linux/mrg \
    build/arm64/linux/mrg \
    build/x86_64/darwin/mrg \
    build/arm64/darwin/mrg \
    build/x86_64/freebsd/mrg \
    build/arm64/freebsd/mrg \
    build/x86_64/windows/mrg.exe \
    build/amd64/windows/mrg.exe \
    build/arm64/windows/mrg.exe

build/x86_64/linux/mrg: $(SRC)
	go build -ldflags=${LDFLAGS} -o $@

build/arm64/linux/mrg: $(SRC)
	GOARCH=arm64 go build -ldflags=${LDFLAGS} -o $@

build/x86_64/darwin/mrg: $(SRC)
	GOOS=darwin go build -ldflags=${LDFLAGS} -o $@

build/arm64/darwin/mrg: $(SRC)
	GOOS=darwin GOARCH=arm64 go build -ldflags=${LDFLAGS} -o $@

build/x86_64/freebsd/mrg: $(SRC)
	GOOS=freebsd go build -ldflags=${LDFLAGS} -o $@

build/arm64/freebsd/mrg: $(SRC)
	GOOS=freebsd GOARCH=arm64 go build -ldflags=${LDFLAGS} -o $@

build/x86_64/windows/mrg.exe:
	GOOS=windows GOARCH=386 go build -ldflags=${LDFLAGS} -o $@

build/arm64/windows/mrg.exe:
	GOOS=windows GOARCH=arm go build -ldflags=${LDFLAGS} -o $@

build/amd64/windows/mrg.exe:
	GOOS=windows GOARCH=amd64 go build -ldflags=${LDFLAGS} -o $@

install_linux:
	install -d $(DESTDIR)$(BINDIR)
	install -m 755 build/x86_64/linux/mrg $(DESTDIR)$(BINDIR)

clean:
	go clean
	rm -rf build
