package main

import (
	"context"
	"fmt"
	"sort"
	"strings"
	"time"

	"github.com/dustin/go-humanize"
	"github.com/maruel/natural"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/cli/clients"
	shcli "gitlab.com/mergetb/tech/shared/cli"
	"google.golang.org/protobuf/encoding/protojson"
)

func realization() {

	var duration string
	realize := &cobra.Command{
		Use:   "realize <realization>.<experiment>.<project> <revision|tag|branch> <reference>",
		Short: "Realize an experiment",
		Long:  "Realize an experiment with the given reference. The reference can be a repository tag, branch, or revistion. Default is a revision (commit hash). Only one of tag, branch, or revision is allowed.",
		Args:  cobra.ExactArgs(3),
		Run: func(cmd *cobra.Command, args []string) {

			rlz, exp, proj := parseRzid(args[0])
			realize(rlz, exp, proj, args[1], args[2], duration)
		},
	}
	realize.Flags().StringVar(&duration, "duration", "1w", "Duration of the realization resources reservation. Format is NwNdNhNs, where N is an integer and w=week, d=day, h=hour, and s=second. Any field can be missing. If \"NEVER\", the realization will never expire.")
	root.AddCommand(realize)

	relinquish := &cobra.Command{
		Use:   "relinquish <realization>.<experiment>.<project>",
		Short: "Relinquish a realization",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {

			rlz, exp, proj := parseRzid(args[0])
			relenquish(rlz, exp, proj)

		},
	}
	root.AddCommand(relinquish)

	showreal := &cobra.Command{
		Use:   "realization <realization>.<experiment>.<project>",
		Short: "Show a realization",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {

			rlz, exp, proj := parseRzid(args[0])
			showreal(rlz, exp, proj)

		},
	}
	show.AddCommand(showreal)

	filter := &FlagEnum{Choices: []string{"user", "all"}}
	listreal := &cobra.Command{
		Use:   "realizations",
		Short: "List realizations",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			listreal(filter.String())
		},
	}
	filter.AddToFlagSet(
		listreal.Flags(),
		"filter",
		"Filter realizations: "+filter.Help()+". Default: "+filter.Choices[0],
	)
	list.AddCommand(listreal)

	var available bool
	res := &cobra.Command{
		Use:   "resources",
		Short: "Show testbed resources",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			showResources(available)
		},
	}
	res.Flags().BoolVarP(&available, "available", "", false, "Only show available resources")
	show.AddCommand(res)

	updateRlz := &cobra.Command{
		Use:   "realization",
		Short: "Update a realization",
		Args:  cobra.NoArgs,
	}
	update.AddCommand(updateRlz)

	updateRlzDuration := &cobra.Command{
		Use:   "expiration <realization>.<experiment>.<project> duration",
		Short: "Update the reservation duration. Format is NwNdNhNs, where N is an integer and w=week, d=day, h=hour, and s=second. Any field can be missing. If \"NEVER\", the realization will never expire.",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			rlz, exp, proj := parseRzid(args[0])
			updateRlzDuration(rlz, exp, proj, args[1])
		},
	}
	updateRlz.AddCommand(updateRlzDuration)
}

var rzidErr = `%s is not a valid realization id.
  Use <realization>.<experiment>.<project> where
    - <realization> is the name of the realization
    - <experiment> is the name of the experiment
    - <project> is the name of the project the experiment belongs to`

func parseRzid(s string) (string, string, string) {

	parts := strings.Split(s, ".")
	if len(parts) != 3 {
		log.Fatalf(xpidErr, s)
	}
	return parts[0], parts[1], parts[2]

}

func realize(realization, experiment, project, kind, reference, duration string) {

	dur := &portal.ReservationDuration{}
	if duration == "NEVER" { // not great, but useful.
		dur.When = portal.ReservationDuration_never
	} else {
		dur.Duration = duration
	}

	req := &portal.RealizeRequest{
		Project:     project,
		Experiment:  experiment,
		Realization: realization,
		Duration:    dur,
	}

	if kind == "revision" {
		req.Revision = reference
	} else if kind == "tag" {
		req.Tag = reference
	} else if kind == "branch" {
		req.Branch = reference
	} else {
		log.Fatalf("reference kind must be one of revision, tag, or branch")
	}

	clients.Realize(endpoint(), token(), func(cli portal.RealizeClient) error {

		_, err := cli.Realize(context.TODO(), req)
		if err != nil {
			handleError(err)
		}

		return nil

	})

	checkRealization(project, experiment, realization)
}

func relenquish(realization, experiment, project string) {

	clients.Realize(endpoint(), token(), func(cli portal.RealizeClient) error {

		_, err := cli.Relinquish(context.TODO(), &portal.RelinquishRequest{
			Project:     project,
			Experiment:  experiment,
			Realization: realization,
		})

		if err != nil {
			handleError(err)
		}

		return nil

	})

}

func showreal(realization, experiment, project string) {

	clients.Realize(endpoint(), token(), func(cli portal.RealizeClient) error {

		resp, err := cli.GetRealization(context.TODO(), &portal.GetRealizationRequest{
			Project:     project,
			Experiment:  experiment,
			Realization: realization,
		})
		if err != nil {
			handleError(err)
		}

		if jsonOut {

			fmt.Printf("%s\n", protojson.Format(resp))
			return nil

		}

		dumpreal(resp.Result.Realization)

		if len(resp.Result.Diagnostics.Value) > 0 {
			dumpdiags(resp.Result.Diagnostics)
		}

		return nil

	})

}

type facResources struct {
	cores_total uint32
	cores_res   uint32
	cores_used  uint32
	mem_total   uint64
	mem_used    uint64
	mem_res     uint64
}

func showResources(showOnlyAvailable bool) {

	clients.Realize(endpoint(), token(), func(cli portal.RealizeClient) error {

		resp, err := cli.GetResources(
			context.TODO(),
			&portal.GetResourcesRequest{},
		)

		if err != nil {
			handleError(err)
		}

		if jsonOut {
			fmt.Printf("%s\n", protojson.Format(resp))
			return nil
		}

		fmt.Fprintf(tw, "Facility\tResource\tAllocation Mode\tExperiment Nodes\tCores:\tFree\tUsed\tReserved\tMemory:\tFree\tUsed\tReserved\n")

		fac_resources := make(map[string]*facResources)

		for _, r := range resp.Resources {
			if _, ok := fac_resources[r.Resource.Facility]; ok {
				fac_resources[r.Resource.Facility].cores_total += r.Resource.TotalCores()
				fac_resources[r.Resource.Facility].cores_res += r.Resource.ReservedCores()
				fac_resources[r.Resource.Facility].mem_total += r.Resource.TotalMem()
				fac_resources[r.Resource.Facility].mem_res += r.Resource.ReservedMem()
			} else {
				fac_resources[r.Resource.Facility] = &facResources{
					cores_total: r.Resource.TotalCores(),
					cores_res:   r.Resource.ReservedCores(),
					mem_total:   r.Resource.TotalMem(),
					mem_res:     r.Resource.ReservedMem(),
					cores_used:  0,
					mem_used:    0,
				}
			}

			// map mzid to user nodes
			node_map := make(map[string][]string)

			// total in use per facility resource
			cores := uint32(0)
			mem := uint64(0)

			metal := false

			for _, a := range r.Allocated {
				mzid := a.Rid + "." + a.Eid + "." + a.Pid

				if _, ok := node_map[mzid]; !ok {
					node_map[mzid] = []string{a.Node}
				} else {
					node_map[mzid] = append(node_map[mzid], a.Node)
				}

				cores += a.CoresUsed
				mem += a.MemoryUsed

				fac_resources[r.Resource.Facility].cores_used += a.CoresUsed
				fac_resources[r.Resource.Facility].mem_used += a.MemoryUsed

				if !a.Virt {
					metal = true
				}
			}

			node_str := []string{}
			for mzid, nodes := range node_map {
				node_str = append(node_str, fmt.Sprintf("%s [%s]", mzid, strings.Join(nodes, ",")))
			}

			var mode string
			var avail bool
			if len(r.Allocated) == 0 {
				mode = "Idle"
				avail = true
			} else if len(r.Allocated) >= 2 || !metal {
				mode = "Hypervisor"
				avail = cores != r.Resource.Cores() && mem != r.Resource.Mem()
			} else {
				mode = "Bare Metal"
				avail = false
			}

			if !showOnlyAvailable || avail {
				fmt.Fprintf(tw, "%s\t%s\t%s\t%s\t\t%d\t%d\t%d\t\t%s\t%s\t%s\n",
					r.Resource.Facility,
					r.Resource.Id,
					mode,
					strings.Join(node_str, "; "),
					r.Resource.Cores()-cores,
					cores,
					r.Resource.ReservedCores(),
					humanize.IBytes(r.Resource.Mem()-mem),
					humanize.IBytes(mem),
					humanize.IBytes(r.Resource.ReservedMem()),
				)
			}
		}

		tw.Flush()

		keys := make([]string, 0, len(fac_resources))
		for k := range fac_resources {
			keys = append(keys, k)
		}

		sort.Sort(natural.StringSlice(keys))

		fmt.Fprintf(tw, "\nFacility\tCommissioned Cores\tCommisioned Memory\tFree Cores\tFree Memory\n")
		// for fac, rsc := range fac_resources {
		for _, key := range keys {
			rsc := fac_resources[key]
			fmt.Fprintf(tw, "%s\t%d\t%s\t%d\t%s\n",
				key,
				rsc.cores_total-rsc.cores_res,
				humanize.IBytes(rsc.mem_total-rsc.mem_res),
				rsc.cores_total-rsc.cores_res-rsc.cores_used,
				humanize.IBytes(rsc.mem_total-rsc.mem_res-rsc.mem_used),
			)
		}

		tw.Flush()

		return nil
	})

}

func dumpdiags(d *portal.DiagnosticList) {

	fmt.Println("Diagnostics:")
	fmt.Println(d.ToString())

}

func dumpreal(r *portal.Realization) {

	created := "Unknown"
	if r.Created != nil && r.Created.Seconds != 0 && r.Created.Nanos != 0 {
		ct := r.Created.AsTime()
		created = ct.Round(time.Second).String()
	}

	expIn := "Never"
	expires := "Never"
	if r.Expires != nil && r.Expires.Seconds != 0 && r.Expires.Nanos != 0 {
		et := r.Expires.AsTime()
		expires = et.Round(time.Second).String()
		expIn = time.Until(et).Round(time.Second).String()
	}

	fmt.Println("Creator: ", r.Creator)
	fmt.Println("Created: ", created)
	fmt.Println("Expires: ", expires)
	fmt.Println("Until expires: ", expIn)
	fmt.Printf("%s", r.ToString())

	if r.LinkEmulation != nil {
		fmt.Println("Emulation:")
		fmt.Printf("  Backend: %s\n", r.GetLinkEmulation().GetBackend().String())
		fmt.Println("  Links:")
		for _, lnk := range r.LinkEmulation.Links {
			fmt.Printf("    nodes=[%s] server=%s vteps=[%s] capacity=%d latency=%d loss=%f\n", strings.Join(lnk.Nodes, " "), lnk.Server, strings.Join(lnk.Interfaces, " "), lnk.GetCapacity(), lnk.GetLatency(), lnk.GetLoss())
		}
	}

	fmt.Printf("Infraserver: %s (%s)\n", r.GetInfranet().GetInfrapodServer(), r.GetInfranet().GetInfrapodSite())
}

func listreal(filter string) {

	fm := portal.FilterMode_ByUser
	if filter == "all" {
		fm = portal.FilterMode_ByAll
	}

	clients.Realize(endpoint(), token(), func(cli portal.RealizeClient) error {

		resp, err := cli.GetRealizations(context.TODO(), &portal.GetRealizationsRequest{Filter: fm})
		if err != nil {
			handleError(err)
		}

		if jsonOut {

			fmt.Printf("%s\n", protojson.Format(resp))
			return nil

		}

		if len(resp.Results) > 0 {

			fmt.Fprint(tw, shcli.TableColumns(shcli.Blue, "Realization", "Virtual", "Physical", "Total Nodes", "Links", "Creator", "Until Expires", "Created", "Expires", "Status"))
			for _, x := range resp.Results {

				status := "Succeeded"
				if x.Diagnostics.Error() {
					status = "Failed"
				}

				seenNodes := make(map[string]bool)
				vms, phy := 0, 0

				for _, n := range x.Realization.Nodes {
					if n.VmAlloc != nil {
						vms++
					} else {
						phy++
					}

					seenNodes[n.Resource.Id] = true
				}

				created := "Unknown"
				if x.Realization.Created != nil && x.Realization.Created.Seconds != 0 && x.Realization.Created.Nanos != 0 {
					ct := x.Realization.Created.AsTime()
					created = ct.Round(time.Second).String()
				}

				expIn := "Never"
				expires := "Never"
				if x.Realization.Expires != nil && x.Realization.Expires.Seconds != 0 && x.Realization.Expires.Nanos != 0 {
					et := x.Realization.Expires.AsTime()
					expires = et.Round(time.Second).String()
					expIn = time.Until(et).Round(time.Second).String()
				}

				fmt.Fprint(tw, shcli.TableRow(
					shcli.TableItem{Value: fmt.Sprintf("%s.%s.%s", x.Realization.Id, x.Realization.Eid, x.Realization.Pid)},
					shcli.TableItem{Value: vms},
					shcli.TableItem{Value: phy},
					shcli.TableItem{Value: len(seenNodes)},
					shcli.TableItem{Value: len(x.Realization.Links)},
					shcli.TableItem{Value: x.Realization.Creator},
					shcli.TableItem{Value: expIn},
					shcli.TableItem{Value: created},
					shcli.TableItem{Value: expires},
					shcli.TableItem{Value: status},
				))
			}
			tw.Flush()
		}

		return nil

	})

}

func updateRlzDuration(rlz, exp, proj, duration string) {

	dur := &portal.ReservationDuration{}
	if duration == "NEVER" { // not great, but useful.
		dur.When = portal.ReservationDuration_never
	} else {
		dur.Duration = duration
	}

	clients.Realize(endpoint(), token(), func(cli portal.RealizeClient) error {

		resp, err := cli.UpdateRealization(
			context.TODO(),
			&portal.UpdateRealizationRequest{
				Project:     proj,
				Experiment:  exp,
				Realization: rlz,
				Duration:    dur,
			},
		)

		if jsonOut {
			fmt.Printf("%s\n", protojson.Format(resp))
			return nil
		}

		if err != nil {
			handleError(err)
		}

		return nil
	})
}
