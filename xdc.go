package main

import (
	"context"
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/cli/clients"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/encoding/protojson"

	shcli "gitlab.com/mergetb/tech/shared/cli"
	pviews "gitlab.com/mergetb/tech/shared/cli/portal"
)

func xdc() {

	xdcs := &cobra.Command{
		Use:   "xdcs [project]",
		Short: "List XDCs. If [project] is empty, list all XDCs.",
		Args:  cobra.RangeArgs(0, 1),
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) == 1 {
				listXDCs(args[0])
			} else {
				listXDCs("")
			}
		},
	}
	list.AddCommand(xdcs)

	var withStatus bool
	showCmd := &cobra.Command{
		Use:   "xdc <xdc>.<project>",
		Short: "Show the XDC's information",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			x, p := parseXdcid(args[0])
			showXDC(x, p, withStatus)
		},
	}
	showCmd.PersistentFlags().BoolVarP(&withStatus, "status", "S", false, "get status of XDC")
	show.AddCommand(showCmd)

	var image string
	var mem, cores uint
	xdc_type := &FlagEnum{Choices: getEnumNames(portal.XDCType_name)}
	defaultXdcType := portal.XDCType_name[int32(portal.XDCType_shared)]

	nx := &cobra.Command{
		Use:   "xdc <xdc>.<project>",
		Short: "Create a new XDC named <xdc> under the project named <project>.",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			x, p := parseXdcid(args[0])
			xt := xdc_type.String()
			if xt == "" {
				xt = defaultXdcType
			}

			newXDC(x, p, xt, image, mem, cores)
		},
	}
	nx.PersistentFlags().StringVar(&image, "image", "", "image to spawn")
	nx.PersistentFlags().UintVar(&mem, "mem", 0, "max available mem")
	nx.PersistentFlags().UintVar(&cores, "cores", 0, "max available cores")
	xdc_type.AddToFlagSetShort(
		nx.Flags(),
		"type",
		"t",
		"type of the xdc: "+xdc_type.Help()+". Default: "+defaultXdcType,
	)
	newcmd.AddCommand(nx)

	dx := &cobra.Command{
		Use:   "xdc <xdc>.<project>",
		Short: "Delete the given XDC.",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			x, p := parseXdcid(args[0])
			delXDC(x, p)
		},
	}
	del.AddCommand(dx)

	// Commands rooted at "mrg xdc"
	{
		attach := &cobra.Command{
			Use:   "attach <xdc> <realization>.<experiment>.<project>",
			Short: "Attach the given XDC to the given materialization",
			Args:  cobra.ExactArgs(2),
			Run: func(cmd *cobra.Command, args []string) {
				attachXdc(args[0], args[1])
			},
		}
		xdccmd.AddCommand(attach)

		detach := &cobra.Command{
			Use:   "detach <xdc>.<project>",
			Short: "Detach the given XDC from its currently attached materialization",
			Args:  cobra.ExactArgs(1),
			Run: func(cmd *cobra.Command, args []string) {
				detachXdc(args[0])
			},
		}
		xdccmd.AddCommand(detach)
	}

}

var xdcidErr = `%s is not a valid XDC id.
  Use <xdc>.<project> where
    - <xdc> is the name of the XDC
    - <project> is the name of the project the XDC belongs to`

func parseXdcid(s string) (string, string) {

	parts := strings.Split(s, ".")
	if len(parts) != 2 {
		log.Fatalf(xdcidErr, s)
	}
	return parts[0], parts[1]
}

func add_xdcinfo_to_table(table *shcli.Table, infos ...*portal.XDCInfo) {
	for _, x := range infos {
		xdc_type, ok := portal.XDCType_name[int32(x.Type)]
		if !ok {
			xdc_type = "unknown"
		}

		table.Rows = append(table.Rows, []shcli.TableItem{
			{
				Key:   "Name.Project",
				Value: x.Name,
			},
			{
				Key:   "Type",
				Value: xdc_type,
			},
			{
				Key:   "Attached",
				Value: x.Materialization,
			},
			{
				Key:   "Reference",
				Value: x.Fqdn,
			},
			{
				Key:   "Creator",
				Value: x.Creator,
			},
			{
				Key:   "Memory",
				Value: x.Memlimit,
			},
			{
				Key:   "CPU",
				Value: x.Cpulimit,
			},
			{
				Key:   "Image",
				Value: x.Image,
			},
			{
				Key:   "URL",
				Value: x.Url,
			},
		},
		)
	}
}

func showXDC(name, project string, withStatus bool) {

	clients.XDC(endpoint(), token(), func(cli portal.XDCClient) error {

		resp, err := cli.GetXDC(
			context.TODO(),
			&portal.GetXDCRequest{
				Xdc:      name,
				Project:  project,
				StatusMS: timeoutAsBool(withStatus),
			},
		)

		if err != nil {
			handleError(err)
		}

		if jsonOut {
			fmt.Printf("%s\n", protojson.Format(resp))
			return nil
		}

		if !withStatus {
			table := new(shcli.Table)
			table.Title = fmt.Sprintf("XDC: %s.%s", name, project)

			add_xdcinfo_to_table(table, resp.Xdc)
			fmt.Fprintf(tw, table.ColumnsToString())
			fmt.Fprintf(tw, table.RowsToString())

			tw.Flush()
		} else {
			pviews.WriteTaskForest(resp.Status, getDepth(), true, tw)
			osExit1IfUnsuccessful(resp.Status)
		}

		return nil
	})
}

func listXDCs(project string) {

	clients.XDC(endpoint(), token(), func(cli portal.XDCClient) error {

		resp, err := cli.ListXDCs(
			context.TODO(),
			&portal.ListXDCsRequest{
				Project: project,
			},
		)
		if err != nil {
			handleError(err)
		}

		if jsonOut {
			fmt.Printf("%s\n", protojson.Format(resp))
			return nil
		}

		if len(resp.XDCs) > 0 {
			table := new(shcli.Table)
			table.Title = "List XDCs"

			add_xdcinfo_to_table(table, resp.XDCs...)
			fmt.Fprintf(tw, table.ColumnsToString())
			fmt.Fprintf(tw, table.RowsToString())

			tw.Flush()

		}

		return nil
	})
}

func newXDC(xdc, project, xdc_type, image string, mem, cores uint) {

	clients.XDC(endpoint(), token(), func(cli portal.XDCClient) error {

		req := &portal.CreateXDCRequest{
			Project: project,
			Xdc:     xdc,
			Type:    portal.XDCType(portal.XDCType_value[xdc_type]),
		}

		admin := false
		if image != "" {
			req.Image = image
			admin = true
		}
		if mem != 0 {
			req.Memlimit = int32(mem)
			admin = true
		}
		if cores != 0 {
			req.Cpulimit = int32(cores)
			admin = true
		}

		_, err := cli.CreateXDC(context.TODO(), req)
		if admin && status.Code(err) == codes.PermissionDenied {
			fmt.Printf("You are not authorized to use xdc configuration flags\n")
			return nil
		}
		if err != nil {
			handleError(err)
		}

		return nil
	})

	checkXDC(xdc, project)
}

func delXDC(xdc, project string) {

	clients.XDC(endpoint(), token(), func(cli portal.XDCClient) error {

		_, err := cli.DeleteXDC(
			context.TODO(),
			&portal.DeleteXDCRequest{
				Project: project,
				Xdc:     xdc,
			},
		)
		if status.Code(err) == codes.NotFound {
			fmt.Printf("%s.%s does not exist\n", xdc, project)
			return nil
		}
		if err != nil {
			handleError(err)
		}

		return nil
	})
}

func attachXdc(xid, rlz string) {

	rTkns := strings.Split(rlz, ".")
	if len(rTkns) != 3 {
		log.Fatal("badly formatted realization id: expecting <rid>.<experiment><project>")
	}

	xTkns := strings.Split(xid, ".")
	if l := len(xTkns); l != 2 {
		log.Fatal("badly formatted xdc id: expecting <xdcname>.<xdcproject>")
	}

	clients.XDC(endpoint(), token(), func(cli portal.XDCClient) error {

		_, err := cli.AttachXDC(
			context.TODO(),
			&portal.AttachXDCRequest{
				// what xdc
				Xdc:     xTkns[0], // xdc name
				Project: xTkns[1], // xdc proj
				// attaching to what
				Experiment:         rTkns[1],
				Realization:        rTkns[0],
				RealizationProject: rTkns[2],
			},
		)

		if err != nil {
			handleError(err)
		}

		return err
	})

	checkXDC(xTkns[0], xTkns[1])
}

func detachXdc(xdc string) {

	xid, pid := parseXdcid(xdc)

	clients.XDC(endpoint(), token(), func(cli portal.XDCClient) error {

		_, err := cli.DetachXDC(
			context.TODO(),
			&portal.DetachXDCRequest{
				Xdc:     xid,
				Project: pid,
			},
		)

		if err != nil {
			handleError(err)
		}

		return err
	})

	checkXDC(xid, pid)
}
