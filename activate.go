package main

import (
	"context"
	_ "fmt"
	_ "strings"

	"github.com/spf13/cobra"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/cli/clients"
	_ "google.golang.org/protobuf/encoding/protojson"
)

func activate() {

	activate := &cobra.Command{
		Use:   "activate",
		Short: "Activate a user or organization",
	}

	freeze := &cobra.Command{
		Use:   "freeze",
		Short: "Freeze a user or organization",
	}

	activateUser := &cobra.Command{
		Use:   "user <username>",
		Short: "Activate a user",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			userActivate(args[0])
		},
	}

	freezeUser := &cobra.Command{
		Use:   "user <username>",
		Short: "Freeze a user",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			userFreeze(args[0])
		},
	}

	activateOrganization := &cobra.Command{
		Use:   "organization <organization>",
		Short: "Activate an organization",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			organizationActivate(args[0])
		},
	}

	freezeOrganization := &cobra.Command{
		Use:   "organization <organization>",
		Short: "Freeze an organization",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			organizationFreeze(args[0])
		},
	}

	activate.AddCommand(activateUser)
	activate.AddCommand(activateOrganization)

	freeze.AddCommand(freezeUser)
	freeze.AddCommand(freezeOrganization)

	root.AddCommand(activate)
	root.AddCommand(freeze)
}

func userActivate(username string) {

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		_, err := cli.ActivateUser(context.TODO(), &portal.ActivateUserRequest{
			Username: username,
		})
		if err != nil {
			handleError(err)
		}

		return nil

	})

	checkUser(username)
}

func userFreeze(username string) {

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		_, err := cli.FreezeUser(context.TODO(), &portal.FreezeUserRequest{
			Username: username,
		})
		if err != nil {
			handleError(err)
		}

		return nil

	})

	checkUser(username)

}

func organizationActivate(organization string) {

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		_, err := cli.ActivateOrganization(context.TODO(), &portal.ActivateOrganizationRequest{
			Organization: organization,
		})
		if err != nil {
			handleError(err)
		}

		return nil

	})

	checkOrganization(organization)

}

func organizationFreeze(organization string) {

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		_, err := cli.FreezeOrganization(context.TODO(), &portal.FreezeOrganizationRequest{
			Organization: organization,
		})
		if err != nil {
			handleError(err)
		}

		return nil

	})

	checkOrganization(organization)

}
