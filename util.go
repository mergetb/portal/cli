package main

import (
	"fmt"
	"os"
	"path"
	"sort"
	"strings"

	log "github.com/sirupsen/logrus"

	portal "gitlab.com/mergetb/api/portal/v1/go"

	"gitlab.com/mergetb/portal/cli/pkg/sudo"
)

func token() string {
	if token := sudo.GetSudoToken(); token != "" {
		return token // running in sudo
	}
	return mrgcfg.GetString("token")
}

// newSshPrivkeyPath returns a path where a new private key will be stored under ~/.ssh/.
// If sudo_username is not an empty string, the key is marked as sudo key.
func newSshPrivkeyPath(sudo_username string) string {
	basekey := defaultKeyBasename
	if sudo_username != "" {
		basekey = basekey + "-sudo-" + sudo_username
	}
	home, err := os.UserHomeDir()
	if err != nil {
		log.Fatalf("user home dir not set: %+v", err)
	}
	return path.Join(home, ".ssh", basekey)
}

// currentSshPrivkeyPath returns a full path of the user's private key.
func currentSshPrivkeyPath() string {
	if key := sudo.GetSudoSshKeyPath(); key != "" {
		return key
	}
	return newSshPrivkeyPath("")
}

func cleanupSshKeys(pSuffix, cSuffix string) {
	privkey := currentSshPrivkeyPath()

	os.Remove(privkey)
	os.Remove(privkey + pSuffix)
	os.Remove(privkey + cSuffix)
}

func getPortalHostname() (string, error) {
	if !mrgcfg.IsSet("server") {
		return "", fmt.Errorf("no server set: please user `mrg config set server` to configure the portal address")
	}

	return mrgcfg.GetString("server"), nil
}

func osExit1IfUnsuccessful(status *portal.TaskTree) {
	if status.HighestStatus != portal.TaskStatus_Success {
		os.Exit(1)
	}
}

func accessModes() []string {

	keys := []int{}
	for k := range portal.AccessMode_name {
		keys = append(keys, int(k))
	}
	sort.Ints(keys)

	m := []string{}
	for k := range keys {
		m = append(m, portal.AccessMode_name[int32(k)])
	}

	return m
}

func ToAccess(access string) (portal.AccessMode, error) {

	a, ok := portal.AccessMode_value[access]
	if !ok {
		valid := []string{}
		for k := range portal.AccessMode_value {
			valid = append(valid, k)
		}
		return portal.AccessMode_Public, fmt.Errorf("mode must be one of: [%s]", strings.Join(valid, ", "))
	}

	return portal.AccessMode(a), nil
}

// getEnumNames returns a slice of strings from a protobuf-generated-like
// map of enum names, e.g. portal.RebootMaterializationMode_name
func getEnumNames(e_name map[int32]string) []string {
	keys := []int{}
	for k := range e_name {
		keys = append(keys, int(k))
	}
	sort.Ints(keys)

	m := []string{}
	for k := range keys {
		m = append(m, e_name[int32(k)])
	}

	return m
}

func rebootModes() []string {
	return getEnumNames(portal.RebootMaterializationMode_name)
}

func ToReboot(reboot string) (portal.RebootMaterializationMode, error) {

	a, ok := portal.RebootMaterializationMode_value[reboot]
	if !ok {
		valid := []string{}
		for k := range portal.RebootMaterializationMode_value {
			valid = append(valid, k)
		}
		return portal.RebootMaterializationMode_Reboot, fmt.Errorf("mode must be one of: [%s]", strings.Join(valid, ", "))
	}

	return portal.RebootMaterializationMode(a), nil
}
