package main

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

type NullableInt struct {
	value *int
}

func (i *NullableInt) String() string {
	if i.value == nil {
		return "nil"
	}

	return fmt.Sprintf("%d", *i.value)
}

func (i *NullableInt) Set(s string) error {
	switch strings.ToLower(s) {
	case "nil":
		fallthrough
	case "null":
		fallthrough
	case "":
		i.value = nil
	}

	d, err := strconv.Atoi(s)
	if err != nil {
		return err
	}

	i.value = &d

	return nil
}

func (i *NullableInt) Type() string {
	return "int"
}

type NullableDuration struct {
	value *time.Duration
}

func (d *NullableDuration) String() string {
	if d.value == nil {
		return "nil"
	}

	return (*d.value).String()
}

func (d *NullableDuration) Set(s string) error {
	switch strings.ToLower(s) {
	case "nil":
		fallthrough
	case "null":
		fallthrough
	case "":
		d.value = nil
	}

	x, err := time.ParseDuration(s)
	if err != nil {
		return err
	}

	if x == 0 {
		return fmt.Errorf("duration must be greater than 0")
	}

	d.value = &x

	return nil
}

func (d *NullableDuration) Type() string {
	return "duration"
}
