package main

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"os"
	"sort"
	"strings"

	"github.com/maruel/natural"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/proto"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/cli/clients"
	xir "gitlab.com/mergetb/xir/v0.3/go"
)

func facility() {

	newFacility := &cobra.Command{
		Use:   "facility <name> <address> <model.xir> <certificate>",
		Short: "Create a new facility",
		Args:  cobra.ExactArgs(4),
		Run: func(cmd *cobra.Command, args []string) {
			newfacility(args[0], args[1], args[2], args[3])
		},
	}
	newcmd.AddCommand(newFacility)

	listFacilities := &cobra.Command{
		Use:   "facilities",
		Short: "List facilities",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			listfacilities()
		},
	}
	list.AddCommand(listFacilities)

	deleteFacility := &cobra.Command{
		Use:   "facility <name>",
		Short: "Delete a facility",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			deleteFacility(args[0])
		},
	}
	del.AddCommand(deleteFacility)

	var saveXir, withModel bool
	showfac := &cobra.Command{
		Use:   "facility <name>",
		Short: "Show the given facility.",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			showfac(args[0], withModel, saveXir)
		},
	}
	showfac.Flags().BoolVarP(
		&withModel,
		"withModel",
		"m",
		false,
		"fetch model data along with facility metadata",
	)
	showfac.Flags().BoolVarP(
		&saveXir,
		"saveXir",
		"x",
		false,
		"fetch model data along with facility metadata",
	)
	show.AddCommand(showfac)

	harborInit := &cobra.Command{
		Use:   "init <name>",
		Short: "Initialize the harbor for a given facility",
		Args:  cobra.ExactArgs(1),
		Run:   func(cmd *cobra.Command, args []string) { initharbor(args[0]) },
	}
	harbor.AddCommand(harborInit)

	harborDeinit := &cobra.Command{
		Use:   "deinit <name>",
		Short: "Deinitialize the harbor for a given facility",
		Args:  cobra.ExactArgs(1),
		Run:   func(cmd *cobra.Command, args []string) { deinitharbor(args[0]) },
	}
	harbor.AddCommand(harborDeinit)

	facility := &cobra.Command{
		Use:   "facility",
		Short: "Facility specific commands",
	}
	root.AddCommand(facility)

	facResources := &cobra.Command{
		Use:   "resources <facility>",
		Short: "Show facility resources",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			resources(args[0])
		},
	}
	facility.AddCommand(facResources)

	facActRes := &cobra.Command{
		Use:   "activate <facility> <resource> ... <resource>",
		Short: "Mark facility resources as active",
		Args:  cobra.MinimumNArgs(2),
		Run:   func(cmd *cobra.Command, args []string) { activateResources(args[0], args[1:]) },
	}
	facility.AddCommand(facActRes)

	facDeactRes := &cobra.Command{
		Use:   "deactivate <facility> <resource> ... <resource>",
		Short: "Mark facility resources as inactive",
		Args:  cobra.MinimumNArgs(2),
		Run:   func(cmd *cobra.Command, args []string) { deactivateResources(args[0], args[1:]) },
	}
	facility.AddCommand(facDeactRes)
}

func newfacility(name, address, modelfile, certfile string) {

	model, err := xir.FacilityFromFile(modelfile)
	if err != nil {
		log.Fatal(err)
	}

	cert, err := os.ReadFile(certfile)
	if err != nil {
		log.Fatal(err)
	}

	clients.Commission(endpoint(), token(), func(cli portal.CommissionClient) error {

		_, err := cli.RegisterFacility(context.TODO(), &portal.RegisterFacilityRequest{
			Facility: &portal.Facility{
				Name:    name,
				Address: address,
				Members: map[string]*portal.Member{
					getLoggedInUsername(): {
						State: portal.Member_Active,
						Role:  portal.Member_Creator,
					},
				},
				Certificate: string(cert),
			},
			Model: model,
		})

		if err != nil {
			handleError(err)
		}

		return nil

	})

}

func deleteFacility(name string) {

	clients.Commission(endpoint(), token(), func(cli portal.CommissionClient) error {

		_, err := cli.DeleteFacility(context.TODO(),
			&portal.DeleteFacilityRequest{Name: name},
		)
		if err != nil {
			handleError(err)
		}

		return nil

	})

}

func listfacilities() {

	var facilities []*portal.Facility

	clients.Commission(endpoint(), token(), func(cli portal.CommissionClient) error {

		resp, err := cli.GetFacilities(context.TODO(), &portal.GetFacilitiesRequest{})
		if err != nil {
			handleError(err)
		}

		facilities = resp.Facilities

		return nil

	})

	if len(facilities) > 0 {
		fmt.Fprintf(tw, "Name\tAddress\tDescription\tMode\n")
		fmt.Fprintf(tw, "----\t-------\t-----------\t----\n")
	}
	for _, x := range facilities {
		fmt.Fprintf(tw, "%s\t%s\t%s\t%s\n",
			x.Name,
			x.Address,
			x.Description,
			x.AccessMode,
		)
	}
	tw.Flush()

}

func showfac(fac string, withmodel, savexir bool) {

	clients.Commission(endpoint(), token(), func(cli portal.CommissionClient) error {

		resp, err := cli.GetFacility(
			context.TODO(),
			&portal.GetFacilityRequest{
				Name:      fac,
				WithModel: withmodel || savexir,
			},
		)

		if err != nil {
			handleError(err)
		}

		f := resp.Facility

		if jsonOut {

			fmt.Printf("%s\n", protojson.Format(resp))

		} else {

			fmt.Printf("Name: %s\n", f.Name)
			fmt.Printf("Address: %s\n", f.Address)
			if f.Description != "" {
				fmt.Printf("Description: %s\n", f.Description)
			}
			fmt.Printf("Access Mode: %s\n", f.AccessMode.String())
			fmt.Printf("Members: %s\n", fm2st(f.Members))
			fmt.Printf("Certificate: %t\n", f.Certificate != "")
			fmt.Printf("CA Certificate: %t\n", f.Cacertificate != "")

		}

		if savexir {

			buf, err := proto.Marshal(resp.Model)
			if err != nil {
				log.Fatalf("marshal facility: %v", err)
			}
			os.WriteFile(
				fmt.Sprintf("%s.xir", fac),
				[]byte(base64.StdEncoding.EncodeToString(buf)),
				0644,
			)

		}

		return nil
	})
}

func resources(fac string) {

	var m *xir.Facility

	clients.Commission(
		endpoint(), token(), func(cli portal.CommissionClient) error {

			resp, err := cli.GetFacility(
				context.TODO(),
				&portal.GetFacilityRequest{
					Name:      fac,
					WithModel: true,
				},
			)

			if err != nil {
				handleError(err)
			}

			m = resp.Model

			return nil
		},
	)

	active := []string{}
	inactive := []string{}

	for _, r := range m.Resources {

		for _, role := range r.Roles {
			if role == xir.Role_TbNode {

				a := true
				for _, am := range r.Alloc {
					if am == xir.AllocMode_NoAlloc {
						a = false
					}
				}

				if a {
					active = append(active, r.Id)
				} else {
					inactive = append(inactive, r.Id)
				}
				break
			}
		}
	}

	if jsonOut {

		type ResOut struct {
			Active   []string
			Inactive []string
		}
		out, _ := json.MarshalIndent(ResOut{active, inactive}, "", "\t")
		fmt.Println(string(out))

	} else {
		if len(active) > 0 {
			sort.Sort(natural.StringSlice(active))
			fmt.Println("Active Nodes:")
			fmt.Println(strings.Join(active, ", "))
		}
		if len(inactive) > 0 {
			sort.Sort(natural.StringSlice(inactive))
			fmt.Println("Inactive Nodes:")
			fmt.Println(strings.Join(inactive, ", "))
		}
	}
}

func deactivateResources(facility string, resources []string) {

	clients.Commission(endpoint(), token(), func(cli portal.CommissionClient) error {

		_, err := cli.DeactivateResources(
			context.TODO(),
			&portal.DeactivateResourcesRequest{
				Facility:  facility,
				Resources: resources,
			},
		)

		if err != nil {
			handleError(err)
		}

		return nil
	})
}

func activateResources(facility string, resources []string) {

	clients.Commission(endpoint(), token(), func(cli portal.CommissionClient) error {

		_, err := cli.ActivateResources(
			context.TODO(),
			&portal.ActivateResourcesRequest{
				Facility:  facility,
				Resources: resources,
			},
		)

		if err != nil {
			handleError(err)
		}

		return nil
	})
}

func initharbor(fac string) {

	clients.Commission(endpoint(), token(), func(cli portal.CommissionClient) error {

		_, err := cli.InitializeHarbor(
			context.TODO(),
			&portal.InitHarborRequest{
				Facility: fac,
			},
		)

		if err != nil {
			handleError(err)
		}

		return nil

	})

}

func deinitharbor(fac string) {

	clients.Commission(endpoint(), token(), func(cli portal.CommissionClient) error {

		_, err := cli.DeinitializeHarbor(
			context.TODO(),
			&portal.DeinitHarborRequest{
				Facility: fac,
			},
		)

		if err != nil {
			handleError(err)
		}

		return nil

	})

}

// convert facility members into a readable one line string.
func fm2st(ms map[string]*portal.Member) string {

	ss := []string{}

	for name, m := range ms {
		ss = append(ss, name+" ("+m.Role.String()+"/"+m.State.String()+")")
	}

	return strings.Join(ss, ", ")
}
