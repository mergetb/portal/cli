package main

import (
	"errors"
	"fmt"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/portal/services/pkg/merror"
	"google.golang.org/grpc/status"
)

func handleError(err error) {

	if err == nil {
		os.Exit(0)
	}

	if jsonOut {
		fmt.Printf("error: %+v", err)
		os.Exit(1)
	}

	me := merror.FromGRPCError(err)
	if errors.Is(me, merror.ErrFailedToRealize) {
		handleFailToRealize(me)
	}

	if me.Detail == "Uncategorized" {

		// Assume it's a GRPC Status error.
		if e, ok := status.FromError(err); ok {

			// we get code 14 if the connection fails.
			if e.Code() == 14 {

				srv := mrgcfg.GetString("server")
				if srv == "" {
					fmt.Printf("%s\n", red("Connection error. There is no merge portal server configured."))
					fmt.Printf("%s\n", red("Use the command \"mrg config set server [server]\" to set it."))
					fmt.Println()
				} else {
					fmt.Printf("%s %s\n", red("Connection error. Please confirm that mrg can connect to the server and port that is configured:"), blue(endpoint()))
					fmt.Printf("%s\n", red("The server can be changed with the command \"mrg config set server [server]\" if need be."))
					fmt.Println()
				}
			} else if e.Code() == 12 {
				// we get 12 if the connection inits, but the server has no idea
				// what we're talking about or there is not a GRPC server listening
				fmt.Printf("%s %s\n", red("Server error. Please confirm that there is a Merge Portal running and available at:"), blue(endpoint()))
				fmt.Printf("%s\n", red("The server can be changed with the command \"mrg config set server [server]\" if need be."))
				fmt.Println()
			}

			fmt.Fprintf(tw, "%s:\t%s\n", blue("Error"), red(e.Code().String()))
			fmt.Fprintf(tw, "%s:\t%s\n", blue("Message"), red(e.Message()))
			tw.Flush()
			os.Exit(1)
		}

		log.Fatal(err)
	}

	dumpMergeError(me)
	os.Exit(1)
}

func dumpMergeError(me *merror.MergeError) {
	if me.Title != "" {
		fmt.Fprintf(tw, "%s\t%s\n", blue("Title: "), red(me.Title))
	}
	if me.Detail != "" {
		fmt.Fprintf(tw, "%s\t%s\n", blue("Detail: "), red(me.Detail))
	}
	if me.Evidence != "" {
		fmt.Fprintf(tw, "%s\t%s\n", blue("Evidence: "), green(me.Evidence))
	}
	if me.Instance != "" {
		fmt.Fprintf(tw, "%s\t%s\n", blue("Instance: "), green(me.Instance))
	}
	if me.Type != "" {
		fmt.Fprintf(tw, "%s\t%s\n", blue("Type: "), green(me.Type))
	}
	if me.Timestamp != "" {
		fmt.Fprintf(tw, "%s\t%s\n", blue("Timestamp: "), green(me.Timestamp))
	}
	tw.Flush()
}

func handleFailToRealize(me *merror.MergeError) {

	evidence := me.Evidence
	whathappened, err := merror.ExplainDiagnostics(evidence)
	if err == nil {
		// Remove evidence before we dump the rest of the error.
		me.Evidence = ""
		fmt.Fprintf(tw, "\n%s:\n%s\n\n",
			blue("Failed to Realize"),
			red(strings.Join(whathappened, "\n")),
		)
	}

	os.Exit(1)
}
