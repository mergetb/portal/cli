package main

import (
	"bufio"
	"context"
	"fmt"
	"os"
	"os/exec"
	"path"
	"sort"
	"strings"

	terminal "golang.org/x/term"
	"golang.org/x/text/cases"
	"golang.org/x/text/language"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	portal "gitlab.com/mergetb/api/portal/v1/go"

	"gitlab.com/mergetb/portal/cli/clients"
	"gitlab.com/mergetb/portal/cli/pkg/sudo"
)

var (
	defaultKeyBasename string = "merge_key"
	pubSuffix          string = ".pub"
	certSuffix         string = "-cert.pub"
)

func identity() {

	var password string = ""
	var organization string = ""
	var organization_role string = ""
	var usstate string = ""
	var username string = ""
	register := &cobra.Command{
		Use:   "register <username> <email> <full name> <institution> <category> <country>",
		Short: "Register a new identity with the portal",
		Args:  cobra.ExactArgs(6),
		Run: func(cmd *cobra.Command, args []string) {

			var err error
			if password == "" {
				password, err = createPassword(args[0])
			}

			if err != nil {
				log.Fatal(err)
			}

			register(
				args[0], args[1], args[2], args[3], args[4], args[5], password,
				organization, organization_role, usstate,
			)
		},
	}
	register.Flags().StringVarP(&password, "passwd", "p", "", "user password")
	register.Flags().StringVarP(&usstate, "usstate", "", "", "user US state")
	register.Flags().StringVarP(&organization, "organization", "o", "", "request organization membership")
	register.Flags().StringVarP(&organization_role, "organization_role", "r", portal.Member_Member.String(), "request organization role (one of "+strings.Join(memberRoles(), ",")+")")
	root.AddCommand(register)

	unregister := &cobra.Command{
		Use:   "unregister <username>",
		Short: "Unregister an existing identity from the portal",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			unregister(args[0])
		},
	}
	root.AddCommand(unregister)

	nokeys := false
	login := &cobra.Command{
		Use:   "login <username>",
		Short: "Log in to a portal",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			if password == "" {
				password = getPassword(args[0])
			}
			login(args[0], password, nokeys, false)
		},
	}
	login.Flags().BoolVar(&nokeys, "nokeys", nokeys, "Do not copy merge generated keys and certificate into ~/.ssh")
	login.Flags().StringVarP(&password, "passwd", "p", "", "User password")
	root.AddCommand(login)

	logout := &cobra.Command{
		Use:   "logout",
		Short: "Logout the currently logged in user from the portal",
		Args:  cobra.ExactArgs(0),
		Run: func(cmd *cobra.Command, args []string) {
			logout(true)
		},
	}
	root.AddCommand(logout)

	sudo_cmd := &cobra.Command{
		Use:   "sudo -u <username> -- <command>...",
		Short: "Execute a command as a different user",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			if username == "" {
				log.Fatal("missing required [-u|--user] flag")
			}
			sudo_run(username, password, nokeys, args)
		},
	}
	sudo_cmd.Flags().StringVarP(&username, "user", "u", "", "User name")
	sudo_cmd.Flags().StringVarP(&password, "passwd", "p", "", "User password")
	sudo_cmd.Flags().BoolVar(&nokeys, "nokeys", nokeys, "Do not attempt to retrieve ssh keys and certs")
	root.AddCommand(sudo_cmd)

	listIds := &cobra.Command{
		Use:   "ids",
		Short: "Get portal identities",
		Run: func(cmd *cobra.Command, args []string) {
			listIds()
		},
	}
	list.AddCommand(listIds)

	showId := &cobra.Command{
		Use:   "id",
		Short: "Show portal identity",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			showId(args[0])
		},
	}
	show.AddCommand(showId)

	var token bool
	whoami := &cobra.Command{
		Use:   "whoami",
		Short: "Show the identity of the logged in user",
		Run: func(cmd *cobra.Command, args []string) {
			whoami(token)
		},
	}
	whoami.Flags().BoolVarP(&token, "token", "t", false,
		"Display session token instead of username")
	root.AddCommand(whoami)

}

func getPassword(username string) string {

	fmt.Printf("Enter password for %s: ", username)
	pswd, err := terminal.ReadPassword(int(os.Stdin.Fd()))
	if err != nil {
		log.Fatalf("failed to read password: %v", err)
	}
	fmt.Println("")

	return string(pswd)

}

func createPassword(username string) (string, error) {

	fmt.Printf("Enter password for %s: ", username)
	pswd1, err := terminal.ReadPassword(int(os.Stdin.Fd()))
	if err != nil {
		log.Fatalf("failed to read password: %v", err)
	}
	fmt.Println("")

	fmt.Printf("Enter password again for %s: ", username)
	pswd2, err := terminal.ReadPassword(int(os.Stdin.Fd()))
	if err != nil {
		log.Fatalf("failed to read password: %v", err)
	}
	fmt.Println("")

	if string(pswd1) != string(pswd2) {
		return "", fmt.Errorf("passwords do not match, aborting")
	}

	return string(pswd1), nil

}

// login() logs user identified by `username` and `password` in: obtains auth token
// and places keys in user's ~/.ssh/ (can be suppressed by nokeys=true.  Returns
// the token as a string.
func login(username, password string, nokeys, sudo_login bool) string {

	if sudo.InSudo() {
		log.Fatal("can't login while running in mrg sudo")
	}
	// Logout user if different than current user
	u, err := getWhoami()
	if err == nil && u != username {
		logout(false)
	}

	user_token := ""

	err = clients.Identity(endpoint(), "", func(cli portal.IdentityClient) error {
		resp, err := cli.Login(context.TODO(), &portal.LoginRequest{
			Username: username,
			Password: password,
		})
		if err != nil {
			handleError(err)
		}

		user_token = resp.Token
		if !sudo_login {
			mrgcfg.Set("token", user_token)
			err = mrgcfg.Write()
			if err != nil {
				log.Fatalf("write config: %v", err)
			}
		}
		return err
	})

	if err != nil {
		handleError(err)
	}

	if !nokeys { // so keys then
		placeUserKeys(username, sudo_login)
	}
	return user_token
}

// sudo_run runs a command in `cmdargs` as user `username` with that user's `password`
// instead of saving/changing the username and token in the config, it will save them
// in the environment passed on to the executed command.
// Ssh keys will be saved in .ssh/ but under different name derived from `username`,
// see getSudoKeyBasename()
func sudo_run(username, password string, nokeys bool, cmdargs []string) {

	if password != "" {
		// we know a password for this account
		token := login(username, password, nokeys, true)
		sudo.SetSudoUser(username)
		sudo.SetSudoToken(token)
	} else {
		sudo.SetSudoUser(username)
		// rely on our current token
		if !nokeys {
			placeUserKeys(username, true)
		}
	}
	if !nokeys {
		// set env to a full private key path
		privkey := newSshPrivkeyPath(username)
		sudo.SetSudoSshKeyPath(privkey)
	}

	cmd := exec.Command(cmdargs[0], cmdargs[1:]...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()

	if password != "" {
		logout(false) // this will cleanup the sudo keys
	} else {
		cleanupSshKeys(pubSuffix, certSuffix)
	}
	if err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			os.Exit(exitError.ExitCode())
		}
	}
}

func logout(checkError bool) {

	if sudo.InSudo() {
		log.Fatal("can't logout while running in mrg sudo")
	}

	username, logoutErr := getWhoami() // xxx why do we need username for logout?

	if logoutErr == nil {
		// attempt to invalidate the token on the portal
		logoutErr = clients.Identity(endpoint(), token(), func(cli portal.IdentityClient) error {

			_, err := cli.Logout(context.TODO(), &portal.LogoutRequest{
				Username: username,
			})

			return err
		})
	}

	// if there is keying material, remove it.
	cleanupSshKeys(pubSuffix, certSuffix)

	// unset either env or config
	mrgcfg.DeleteKeys("username", "token") // Writes new contig.

	if checkError {
		handleError(logoutErr)
	}
}

func register(username, email, fullname, institution, category, country, password, org, orgRole, usstate string) {

	err := clients.Identity(endpoint(), token(), func(cli portal.IdentityClient) error {

		_, err := cli.Register(context.TODO(), &portal.RegisterRequest{
			Username:    username,
			Email:       email,
			Password:    password,
			Institution: institution,
			Category:    category,
			Name:        fullname,
			Country:     country,
			Usstate:     usstate,
		})

		return err

	})

	if err != nil {
		handleError(err)
	}

	// optional organization membership
	if org == "" {
		return
	}

	// login, then add self to organization
	login(username, password, true, false)

	requestOrgMembership(org, username, orgRole, portal.MembershipType_UserMember)

	// users not logged in can't check if the user is properly created,
	// so don't bother
	if len(token()) > 0 {
		checkUser(username)
	}

}

func unregister(username string) {

	err := clients.Identity(endpoint(), token(), func(cli portal.IdentityClient) error {

		_, err := cli.Unregister(context.TODO(), &portal.UnregisterRequest{
			Username: username,
		})

		return err

	})

	if err != nil {
		handleError(err)
	}

}

func listIds() {

	clients.Identity(endpoint(), token(), func(cli portal.IdentityClient) error {

		resp, err := cli.ListIdentities(context.TODO(), &portal.ListIdentityRequest{})
		if err != nil {
			handleError(err)
		}

		if len(resp.Identities) > 0 {

			fmt.Fprintf(tw, "Username\tEmail\tTraits\n")
			fmt.Fprintf(tw, "--------\t-----\t------------------\n")

			for _, x := range resp.Identities {

				traits := []string{}
				keys := make([]string, 0, len(x.Traits))
				for k := range x.Traits {
					keys = append(keys, k)
				}
				sort.Strings(keys)

				for _, k := range keys {
					traits = append(traits, k+": "+x.Traits[k])
				}

				fmt.Fprintf(tw, "%s\t%s\t%v\n", x.Username, x.Email, strings.Join(traits, ", "))
			}
			tw.Flush()
		}

		return nil
	})

}

func showId(username string) {

	clients.Identity(endpoint(), token(), func(cli portal.IdentityClient) error {

		resp, err := cli.GetIdentity(context.TODO(), &portal.GetIdentityRequest{Username: username})
		if err != nil {
			handleError(err)
		}

		fmt.Printf("Username: %s\n", resp.Identity.Username)
		fmt.Printf("EMail: %s\n", resp.Identity.Email)

		for k, v := range resp.Identity.Traits {
			fmt.Printf("%s: %s\n", cases.Title(language.Und).String(k), v)
		}

		return nil
	})

}

// whoami prints username or token on stdout
func whoami(showtoken bool) {
	if showtoken {
		fmt.Println(token())
	} else {
		fmt.Println(getLoggedInUsername())
	}
	return
}

// getWhoami queries the server for the username of the currently logged in user
// If the user isn't logged-in, the error is returned.
func getWhoami() (username string, err error) {
	err = clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		resp, err := cli.Whoami(context.TODO(), &portal.WhoamiRequest{})
		if err == nil {
			username = resp.Whoami
		}
		return err // not reached
	})
	return
}

// getLoggedInUsername convenience wrapper around getWhoami
func getLoggedInUsername() string {
	username, err := getWhoami()
	if err != nil {
		handleError(err)
		// not reached
	}
	return username
}

// placeUserKeys gets private and public keys, as well as a user cert
// and saves them in ~/.ssh (created if necessary)
func placeUserKeys(username string, forSudo bool) {

	keys, err := getKeys(username)
	if err != nil {
		log.Fatalf("get ssh keys error: %+v", err)
	}

	sudo_user := ""
	if forSudo {
		sudo_user = username
	}
	privkey := newSshPrivkeyPath(sudo_user)
	// make sure dir exists
	dir := path.Dir(privkey)
	os.MkdirAll(dir, 0700) // does nothing if the dir exists.

	err = os.WriteFile(privkey, []byte(keys.Private), 0600)
	if err != nil {
		log.Fatalf("private key write: %+v", err)
	}

	err = os.WriteFile(privkey+pubSuffix, []byte(keys.Public), 0644)
	if err != nil {
		log.Fatalf("public key write: %+v", err)
	}

	// certs are generated at login; so if the sudo user isn't currently logged in,
	// we can't get the certs, ignore this error
	cert, err := getCert(username)
	if err != nil {
		if !forSudo {
			log.Fatalf("get user ssh certificate: %+v", err)
		}
	} else {
		err = os.WriteFile(privkey+certSuffix, []byte(cert.Cert), 0644)
		if err != nil {
			log.Fatalf("ssh cert write: %+v", err)
		}

		if !forSudo {
			// when sudoing, aren't going to overwrite CA creds
			err = placeCaHostKey(dir, cert.CAHostPubKey)
			if err != nil {
				log.Fatalf("place CA host pub key: %+v", err)
			}
		}
	}
}

func placeCaHostKey(dir, key string) error {

	line := "@cert-authority * " + key
	line = strings.TrimRight(line, "\n") + "\n" // make sure line ends with eol

	// There should be a better way to do this. I read the entire known_hosts looking for this line
	// and append if not there.
	p := path.Join(dir, "known_hosts")

	if _, err := os.Stat(p); os.IsNotExist(err) {
		// just add it
		os.WriteFile(p, []byte(line), 0600)
		return nil
	}

	if !lineExists(p, line) {

		f, err := os.OpenFile(p, os.O_APPEND|os.O_WRONLY, 0600)
		if err != nil {
			return fmt.Errorf("open file %s: %w", p, err)
		}
		defer f.Close()

		_, err = f.WriteString(line)
		if err != nil {
			return fmt.Errorf("append file %s: %w", p, err)
		}
	} else {
		log.Debugf("ca host key is already in known_hosts")
	}

	return nil
}

func lineExists(path, line string) bool {

	l := strings.TrimRight(line, "\n")
	file, err := os.Open(path)
	if err != nil {
		return false
	}
	defer file.Close()

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		if l == scan.Text() {
			return true
		}
	}

	return false
}
