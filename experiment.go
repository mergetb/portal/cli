package main

import (
	"context"
	"fmt"
	"sort"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"google.golang.org/protobuf/encoding/protojson"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/cli/clients"
	shcli "gitlab.com/mergetb/tech/shared/cli"
	pviews "gitlab.com/mergetb/tech/shared/cli/portal"
)

func experiment() {

	newxp := &cobra.Command{
		Use:   "experiment <experiment>.<project> [description]",
		Short: "Create a new experiment",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {

			exp, proj := parseXpid(args[0])

			desc := ""
			if len(args) == 2 {
				desc = args[1]
			}

			newxp(exp, proj, desc)

		},
	}
	newcmd.AddCommand(newxp)

	filter := &FlagEnum{Choices: []string{"user", "all"}}
	experiments := &cobra.Command{
		Use:   "experiments",
		Short: "List experiments",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {

			getexperiments(filter.String())

		},
	}
	filter.AddToFlagSet(
		experiments.Flags(),
		"filter",
		"Filter experiments: "+filter.Help()+". Default: "+filter.Choices[0],
	)
	list.AddCommand(experiments)

	delxp := &cobra.Command{
		Use:   "experiment <experiment>.<project>",
		Short: "Delete an experiment",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {

			exp, proj := parseXpid(args[0])

			delxp(exp, proj)

		},
	}
	del.AddCommand(delxp)

	var withStatus, withModels bool
	showxp := &cobra.Command{
		Use:   "experiment <experiment>.<project>",
		Short: "Show an experiment",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {

			exp, proj := parseXpid(args[0])

			showxp(exp, proj, withStatus, withModels)

		},
	}
	showxp.Flags().BoolVarP(&withStatus, "with-status", "S", false, "Show compilation status for each revision")
	showxp.Flags().BoolVarP(&withModels, "with-models", "m", false, "Save models for each revision")
	show.AddCommand(showxp)

	// might as well stick revision here.
	encoding := ""
	validEnc := []string{"xir", "dot"}
	showrev := &cobra.Command{
		Use:   "revision <experiment>.<project> <revision>",
		Short: "Show the given experiment revision. If optional encoding is given, show in that format.",
		Long:  "Show the experiment revision. Valid encodings are " + strings.Join(validEnc, ", ") + ". The xir encoding is a base 64 encoded xir.Network",
		Args:  cobra.ExactArgs(2),
		PersistentPreRun: func(cmd *cobra.Command, args []string) {
			if encoding != "" {
				for _, e := range validEnc {
					if strings.ToLower(encoding) == e {
						return
					}
				}
				log.Fatalf("Invalid encoding, %s. Must be one of %s", encoding, strings.Join(validEnc, ", "))
			}
		},
		Run: func(cmd *cobra.Command, args []string) {
			exp, proj := parseXpid(args[0])
			showRev(exp, proj, args[1], encoding, withModels)
		},
	}
	showrev.Flags().StringVarP(&encoding, "encoding", "e", "", "Show the revision with optional encoding")
	showrev.Flags().BoolVarP(&withModels, "with-model", "m", false, "Show model for the revision")
	show.AddCommand(showrev)

}

var xpidErr = `%s is not a valid experiment id.
  Use <experiment>.<project> where
    - <experiment> is the name of the experiment
    - <project> is the name of the project the experiment belongs to`

func parseXpid(s string) (string, string) {

	parts := strings.Split(s, ".")
	if len(parts) != 2 {
		log.Fatalf(xpidErr, s)
	}
	return parts[0], parts[1]

}

func newxp(name, project, description string) {

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		_, err := cli.CreateExperiment(context.TODO(), &portal.CreateExperimentRequest{
			Experiment: &portal.Experiment{
				Name:        name,
				Project:     project,
				Description: description,
			},
		})

		if err != nil {
			handleError(err)
		}

		return nil

	})

	checkExperiment(project, name)

}

func getexperiments(filter string) {

	fm := portal.FilterMode_ByUser
	if filter == "all" {
		fm = portal.FilterMode_ByAll
	}

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		resp, err := cli.GetExperiments(context.TODO(), &portal.GetExperimentsRequest{Filter: fm})
		if err != nil {
			handleError(err)
		}

		if jsonOut {

			fmt.Printf("%s\n", protojson.Format(resp))
			return nil

		}

		if len(resp.Experiments) > 0 {
			fmt.Fprintf(tw, "Name.Project\tDescription\tMode\n")
			fmt.Fprintf(tw, "------------\t-----------\t----\n")
		}
		for _, x := range resp.Experiments {
			fmt.Fprintf(tw, "%s.%s\t%s\t%s\n",
				x.Name,
				x.Project,
				x.Description,
				x.AccessMode,
			)
		}
		tw.Flush()

		return nil

	})

}

func delxp(name, project string) {

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		_, err := cli.DeleteExperiment(context.TODO(), &portal.DeleteExperimentRequest{
			Experiment: name,
			Project:    project,
		})

		if err != nil {
			handleError(err)
		}

		return nil

	})

}

func showxp(name, project string, withStatus, withModels bool) {

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		resp, err := cli.GetExperiment(context.TODO(), &portal.GetExperimentRequest{
			Experiment: name,
			Project:    project,
			WithModels: withModels,
			StatusMS:   timeoutAsBool(withStatus),
		})
		if err != nil {
			handleError(err)
		}

		if jsonOut {
			fmt.Printf("%s\n", protojson.Format(resp))
			return nil
		}

		if withStatus {
			pviews.WriteTaskForest(resp.Status, getDepth(), true, tw)
			osExit1IfUnsuccessful(resp.Status)
			return nil
		}

		e := resp.Experiment

		fmt.Fprintf(tw, "Name: %s\n", e.Name)
		fmt.Fprintf(tw, "Project: %s\n", e.Project)
		fmt.Fprintf(tw, "Repo: %s\n", e.Repository)
		fmt.Fprintf(tw, "Mode: %s\n", e.AccessMode)
		fmt.Fprintf(tw, "Description: %s\n", e.Description)
		fmt.Fprintf(tw, "Creator: %s\n", e.Creator)
		fmt.Fprintf(tw, "Maintainers: %s\n", strings.Join(e.Maintainers, ", "))

		tw.Flush()

		keys := sortedModelKeys(e)

		table := new(shcli.Table)
		padding := "  "

		for _, rev := range keys {
			model := e.Models[rev]

			if len(model.GetRealizations()) == 0 {
				cur_row := []shcli.TableItem{
					{
						Key:   padding + "Revision",
						Value: padding + rev,
					},
					{
						Key:   "Realizations",
						Value: "(none)",
					},
					{
						Key:   "Status",
						Value: model.Msg,
					},
					{
						Key:   "Timestamp",
						Value: model.CompileTime.AsTime().Local().Format(time.UnixDate),
					},
				}

				table.Rows = append(table.Rows, cur_row)
			}

			for i, rlz := range model.GetRealizations() {
				rlz_name := fmt.Sprintf("%s.%s.%s", rlz, name, project)

				if i == 0 {
					cur_row := []shcli.TableItem{
						{
							Key:   padding + "Revision",
							Value: padding + rev,
						},
						{
							Key:   "Realizations",
							Value: rlz_name,
						},
						{
							Key:   "Status",
							Value: model.Msg,
						},
						{
							Key:   "Timestamp",
							Value: model.CompileTime.AsTime().Local().Format(time.UnixDate),
						},
					}

					table.Rows = append(table.Rows, cur_row)
				} else {
					cur_row := []shcli.TableItem{
						{
							Key:   padding + "Revision",
							Value: "",
						},
						{
							Key:   "Realizations",
							Value: rlz_name,
						},
						{
							Key:   "Status",
							Value: "",
						},
						{
							Key:   "Timestamp",
							Value: "",
						},
					}

					table.Rows = append(table.Rows, cur_row)
				}

			}
		}

		fmt.Fprintf(tw, "Models:\n")
		tw.Flush()
		table.ColumnsColor = shcli.Blue
		fmt.Fprintf(tw, table.ColumnsToString())
		fmt.Fprintf(tw, table.RowsToString())
		tw.Flush()

		return nil

	})

}

func sortedModelKeys(e *portal.Experiment) []string {

	// build a list of model keys, sorted by time.
	keys := make([]string, 0, len(e.Models))
	for k := range e.Models {
		keys = append(keys, k)
	}

	sort.Slice(keys, func(i, j int) bool {
		// Use the strings in `keys` as the index into the models to read the compile time.
		a := e.Models[keys[i]].CompileTime.AsTime()
		b := e.Models[keys[j]].CompileTime.AsTime()

		if a.Equal(b) {
			return keys[i] > keys[j]
		}

		return a.After(b)
	})

	return keys
}

func showRev(exp, project, rev, encoding string, model bool) {

	var e portal.GetRevisionRequest_Encoding

	if encoding != "" {
		if encoding == "xir" {
			e = portal.GetRevisionRequest_XIRB64
		} else if encoding == "dot" {
			e = portal.GetRevisionRequest_DOT
		} else {
			// should not happen
			log.Fatalf("unknown encoding %s", encoding)
		}
	}

	clients.Workspace(endpoint(), token(), func(cli portal.WorkspaceClient) error {

		resp, err := cli.GetRevision(
			context.TODO(),
			&portal.GetRevisionRequest{
				Project:    project,
				Experiment: exp,
				Revision:   rev,
				Encoding:   e,
			},
		)
		if err != nil {
			handleError(err)
		}

		if jsonOut {
			fmt.Printf("%s\n", protojson.Format(resp))
			return nil
		}

		if resp.Encoding != "" {
			fmt.Printf("%s\n", resp.Encoding)
			return nil
		}

		m := resp.Model.Model // xir.Network

		fmt.Printf("Name: %s\n", m.Id)

		strs := []string{}
		for _, n := range m.Nodes {
			strs = append(strs, n.GetId())
		}
		fmt.Printf("Nodes: %s\n", strings.Join(strs, ", "))

		strs = []string{}
		for _, l := range m.Links {
			strs = append(strs, l.GetId())
		}
		fmt.Printf("Links: %s\n", strings.Join(strs, ", "))

		if model {
			fmt.Printf("\nModel:\n%s\n", resp.ModelFile)
		}

		return err
	})
}
